### Gymeet backend

Gymeet is an application for connecting people who would like to schedule and take part in team sports games.

This project is a monolithic POC for the backend component of the application.

**Why a monolith first?**

[Martin Fowler's answer](https://martinfowler.com/bliki/MonolithFirst.html)

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.