-- password = mypass
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(1000, 'aeon.fruit@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Aeon', 'Fruit',
null, null, null, '+216 79 852 458',
'aeon.fruit', '1995-10-09', false, 'Tunisia', 'Tunisia', 'Tunis');
-- password = hispass
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(2000, 'meob.ruin@gymeet.com', '$2a$10$nQvgVJOUbMCZnSNlBCp3buP3ty1DobA1i6VfpNf7XfUVczMEYYMRq', 'Meob', 'Ruin',
null, null, null, '+216 79 785 328',
'meob.ruin', '1976-11-16', false, 'Canada', 'Tunisia', 'Tunis');
-- password = thepass
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(3000, 'ron.rook@gymeet.com', '$2a$10$EstKrNGCwzwbf4S6NGUi7.R28mlsYfzyGl8phQyUHa.ZiRjIuO5Ta', 'Ron', 'Rook',
null, null, null, '+33 79 78 58 31 28',
'ron.rook', '1982-03-25', false, 'Tunisia', 'France', 'Toulouse');

INSERT INTO olympesk_users_roles(olympesk_user_id_ref, olympesk_role_id_ref) VALUES(1000, 1);
INSERT INTO olympesk_users_roles(olympesk_user_id_ref, olympesk_role_id_ref) VALUES(2000, 1);

-- aeon.fruit mate
-- password = mypass
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(1001, 'aeon.mate@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Aon', 'Mate',
null, null, null, null, 'aeon.mate', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(1001, 1000);
-- aeon.fruit blocker
-- password = mypass
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(1002, 'aeon.blocker@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'Aon', 'Blocker', null, null, null, null, 'aeon.blocker', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(1002, 1000);

-- mate
-- password = mypass
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(1, 'user1@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'User1', 'Name1',
null, null, null, null, 'user1', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(2, 'user2@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'User2', 'Name2',
null, null, null, null, 'user2', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(3, 'user3@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'User3', 'Name3',
null, null, null, null, 'user3', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(4, 'user4@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'User4', 'Name4',
null, null, null, null, 'user4', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(5, 'user5@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'User5', 'Name5',
null, null, null, null, 'user5', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
-- 1 - 2 mate -- get
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(1, 2);
-- 1 - 3 mate -- get
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(3, 1);
-- 3 - 4 mate -- severe connection
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(3, 4);
-- 2 - 4 mate -- severe connection
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(2, 4);

-- follower
-- password = mypass
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(6, 'user6@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'User6', 'Name6',
null, null, null, null, 'user6', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(7, 'user7@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'User7', 'Name7',
null, null, null, null, 'user7', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
-- 1 follower - 2 followed -- get
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(1, 2);
-- 1 - 3 reciprocated following -- get
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(1, 3);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(3, 1);
-- 4 follower - 1 followed -- get
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(4, 1);
-- 5 follower - 6 followed -- 5 un-follow
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(5, 6);
-- 5 - 7 reciprocated following -- 5 un-follow
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(5, 7);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(7, 5);

-- blocking
-- 1 blocking - 5 blocked -- get
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(1, 5);
-- 1 blocking - 6 blocked -- get
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(1, 6);

-- requester
-- 1 requester - 4 requested -- get
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(1, 4);
-- 1 requester - 7 requested -- get
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(1, 7);
-- 4 requester - 5 requested -- get
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(4, 5);
-- 5 requester - 6 requested -- 5 abort
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(5, 6);

-- IT
-- password = mypass
-- Active users
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(10, 'send.request@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Send',
'Request', null, null, null, null, 'send.request', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(11, 'abort.request@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Abort',
'Request', null, null, null, null, 'abort.request', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(12, 'accept.request@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Accept',
'Request', null, null, null, null, 'accept.request', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(13, 'decline.request@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Decline',
'Request', null, null, null, null, 'decline.request', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(14, 'follow@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Follow', 'Name',
null, null, null, null, 'follow', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(15, 'un-follow@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Un', 'Follow',
null, null, null, null, 'un-follow', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(16, 'block@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Block', 'Name',
null, null, null, null, 'block', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(17, 'un-block@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Un', 'Block',
null, null, null, null, 'un-block', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(18, 'delete.mate@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Delete', 'Mate',
null, null, null, null, 'delete.mate', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
-- Passive users
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(100, 'blocked@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Blocked', 'Name',
null, null, null, null, 'blocked', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(10, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(11, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(12, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(13, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(14, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(15, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(16, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(17, 100);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(18, 100);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(101, 'blocking@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'Blocking', 'Name',
null, null, null, null, 'blocking', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 10);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 11);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 12);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 13);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 14);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 15);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 16);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 17);
INSERT INTO olympesk_relation_blocking(blocking_olympesk_user_id_ref, blocked_olympesk_user_id_ref) VALUES(101, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(102, 'mate.follower.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'MateFollowerFollowed', 'Name', null, null, null, null, 'mate.follower.followed', '1995-10-09', false, 'Yunisia',
'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(10, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(11, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(12, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(13, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(14, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(15, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(16, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(17, 102);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(18, 102);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 102);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 102);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(102, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(103, 'mate.follower.not.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'MateFollowerNotFollowed', 'Name', null, null, null, null, 'mate.follower.not.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(10, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(11, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(12, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(13, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(14, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(15, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(16, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(17, 103);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(18, 103);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(103, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(104, 'mate.not.follower.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'MateNotFollowerFollowed', 'Name', null, null, null, null, 'mate.not.follower.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(10, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(11, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(12, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(13, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(14, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(15, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(16, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(17, 104);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(18, 104);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 104);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 104);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(105, 'mate.not.follower.not.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'MateNotFollowerNotFollowed', 'Name', null, null, null, null, 'mate.not.follower.not.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(10, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(11, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(12, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(13, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(14, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(15, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(16, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(17, 105);
INSERT INTO olympesk_relation_mate(first_olympesk_user_id_ref, second_olympesk_user_id_ref) VALUES(18, 105);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(106, 'not.mate.follower.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'NotMateFollowerFollowed', 'Name', null, null, null, null, 'not.mate.follower.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 106);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 106);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(106, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(107, 'not.mate.follower.not.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'NotMateFollowerNotFollowed', 'Name', null, null, null, null, 'not.mate.follower.not.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(107, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(108, 'not.mate.not.follower.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'NotMateNotFollowerFollowed', 'Name', null, null, null, null, 'not.mate.not.follower.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 108);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 108);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(109, 'not.mate.not.follower.not.followed@gymeet.com',
'$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'NotMateNotFollowerNotFollowed', 'Name',
null, null, null, null, 'not.mate.not.follower.not.followed', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(110, 'requester.follower.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'RequesterFollowerFollowed', 'Name', null, null, null, null, 'requester.follower.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 10);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 11);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 12);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 13);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 14);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 15);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 16);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 17);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(110, 18);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 110);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 110);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(110, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(111, 'requester.follower.not.followed@gymeet.com',
'$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'RequesterFollowerNotFollowed', 'Name',
null, null, null, null, 'requester.follower.not.followed', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 10);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 11);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 12);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 13);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 14);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 15);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 16);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 17);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(111, 18);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(111, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(112, 'requester.not.follower.followed@gymeet.com',
'$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'RequesterNotFollowerFollowed', 'Name',
null, null, null, null, 'requester.not.follower.followed', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 10);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 11);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 12);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 13);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 14);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 15);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 16);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 17);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(112, 18);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 112);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 112);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(113, 'requester.not.follower.not.followed@gymeet.com',
'$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'RequesterNotFollowerNotFollowed', 'Name',
null, null, null, null, 'requester.not.follower.not.followed', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 10);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 11);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 12);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 13);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 14);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 15);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 16);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 17);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(113, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(114, 'responder.follower.followed@gymeet.com', '$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze',
'ResponderFollowerFollowed', 'Name', null, null, null, null, 'responder.follower.followed', '1995-10-09', false,
'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(10, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(11, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(12, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(13, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(14, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(15, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(16, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(17, 114);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(18, 114);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 114);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 114);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(114, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(115, 'responder.follower.not.followed@gymeet.com',
'$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'ResponderFollowerNotFollowed', 'Name',
null, null, null, null, 'responder.follower.not.followed', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(10, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(11, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(12, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(13, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(14, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(15, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(16, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(17, 115);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(18, 115);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 10);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 11);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 12);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 13);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 14);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 15);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 16);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 17);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(115, 18);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(116, 'responder.not.follower.followed@gymeet.com',
'$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'ResponderNotFollowerFollowed', 'Name',
null, null, null, null, 'responder.not.follower.followed', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(10, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(11, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(12, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(13, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(14, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(15, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(16, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(17, 116);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(18, 116);
--
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(10, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(11, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(12, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(13, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(14, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(15, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(16, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(17, 116);
INSERT INTO olympesk_relation_follower(follower_olympesk_user_id_ref, followed_olympesk_user_id_ref) VALUES(18, 116);
--
INSERT INTO olympesk_user(id, email, password, first_name, surname, large_picture, profile_picture, thumbnail_picture,
phone_number, username, birth_date, female, home_country, country, city)
VALUES(117, 'responder.not.follower.not.followed@gymeet.com',
'$2a$10$Q/93V0MafOM3LwRPyBxdp..0BCsLVLq1wR2odrZvoys5OyPuBEVze', 'ResponderNotFollowerNotFollowed', 'Name',
null, null, null, null, 'responder.not.follower.not.followed', '1995-10-09', false, 'Yunisia', 'Yunisia', 'Yunis');
--
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(10, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(11, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(12, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(13, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(14, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(15, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(16, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(17, 117);
INSERT INTO olympesk_relation_requester(requester_olympesk_user_id_ref, requested_olympesk_user_id_ref) VALUES(18, 117);
-----------------------------------------------------------
-- Venue
-----------------------------------------------------------
INSERT INTO olympesk_venue(id, name, latitude, longitude, address, large_picture, medium_picture, thumbnail_picture,
description) VALUES(1, 'Gymnasium 13', 10.5, 36.5, 'address 1', null, null, null, null);
INSERT INTO olympesk_venue(id, name, latitude, longitude, address, large_picture, medium_picture, thumbnail_picture,
description) VALUES(2, 'Coliseum 13', 10.35, 36.35, 'address 2', null, null, null, null);
INSERT INTO olympesk_venue(id, name, latitude, longitude, address, large_picture, medium_picture, thumbnail_picture,
description) VALUES(3, 'Museum 13', 10.1, 36.1, 'address 3', null, null, null, null);
-----------------------------------------------------------
-- Activity
-----------------------------------------------------------
INSERT INTO olympesk_activity(id, name, icon, description) VALUES(1, 'Football', '1.jpg', null);
INSERT INTO olympesk_activity(id, name, icon, description) VALUES(2, 'Handball', '2.jpg', null);
INSERT INTO olympesk_activity(id, name, icon, description) VALUES(3, 'Car racing', '3.jpg', null);
--
INSERT INTO olympesk_users_activities(olympesk_user_id_ref, olympesk_activity_id_ref) VALUES(1000, 1);
INSERT INTO olympesk_users_activities(olympesk_user_id_ref, olympesk_activity_id_ref) VALUES(1000, 3);
INSERT INTO olympesk_users_activities(olympesk_user_id_ref, olympesk_activity_id_ref) VALUES(2000, 1);
INSERT INTO olympesk_users_activities(olympesk_user_id_ref, olympesk_activity_id_ref) VALUES(2000, 2);
--
INSERT INTO olympesk_activities_venues(olympesk_venue_id_ref, olympesk_activity_id_ref) VALUES(2, 3);
INSERT INTO olympesk_activities_venues(olympesk_venue_id_ref, olympesk_activity_id_ref) VALUES(3, 2);
INSERT INTO olympesk_activities_venues(olympesk_venue_id_ref, olympesk_activity_id_ref) VALUES(3, 3);

