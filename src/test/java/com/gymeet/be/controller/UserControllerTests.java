package com.gymeet.be.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.util.Calendar;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriUtils;

import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileDetailedInfo;
import com.gymeet.be.payload.ProfileField;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.ProfileRelationInfo;
import com.gymeet.be.payload.UserIdentityAvailability;
import com.gymeet.be.service.AuthService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class UserControllerTests {

	private TestRestTemplate restTemplate;
	private JwtAuthenticationResponse aeonFruitToken;

	@Autowired
	UserControllerTests(AuthService authService, TestRestTemplate restTemplate) {
		this.restTemplate = restTemplate;

		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("mypass");
		aeonFruitToken = authService.authenticateUser(loginPostRequest);
	}

	private String getAeonFruitToken() {
		return aeonFruitToken.getTokenType() + " " + aeonFruitToken.getAccessToken();
	}

	@Test
	void checkEmailAvailabilityAnonymousExist() {
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate.getForEntity(
				"/api/users/availability/email?em={email}", UserIdentityAvailability.class,
				UriUtils.encodeUriVariables("aeon.fruit@gymeet.com"));
		UserIdentityAvailability userIdentityAvailability = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(userIdentityAvailability);
		assertFalse(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkEmailAvailabilityAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate.exchange(
				"/api/users/availability/email?em={email}", HttpMethod.GET, entity, UserIdentityAvailability.class,
				UriUtils.encodeUriVariables("aeon.fruit@lombak.com"));
		assertEquals(FORBIDDEN, responseEntity.getStatusCode());
	}

	@Test
	void checkEmailAvailabilityAnonymousNoParam() {
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate
				.getForEntity("/api/users/availability/email", UserIdentityAvailability.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void checkPhoneNumberAvailabilityAnonymousExist() {
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate.getForEntity(
				"/api/users/availability/phone-number?pn={phoneNumber}", UserIdentityAvailability.class,
				UriUtils.encodeUriVariables("+216 79 852 458"));
		UserIdentityAvailability userIdentityAvailability = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(userIdentityAvailability);
		assertFalse(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkPhoneNumberAvailabilityAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate.exchange(
				"/api/users/availability/phone-number?pn={phoneNumber}", HttpMethod.GET, entity,
				UserIdentityAvailability.class, UriUtils.encodeUriVariables("123456789"));
		assertEquals(FORBIDDEN, responseEntity.getStatusCode());
	}

	@Test
	void checkPhoneNumberAvailabilityAnonymousNoParam() {
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate
				.getForEntity("/api/users/availability/phone-number", UserIdentityAvailability.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void checkUsernameAvailabilityAnonymousExist() {
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate.getForEntity(
				"/api/users/availability/username?un={username}", UserIdentityAvailability.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		UserIdentityAvailability userIdentityAvailability = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(userIdentityAvailability);
		assertFalse(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkUsernameAvailabilityAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate.exchange(
				"/api/users/availability/username?un={username}", HttpMethod.GET, entity,
				UserIdentityAvailability.class, UriUtils.encodeUriVariables("aeon.fruity"));
		assertEquals(FORBIDDEN, responseEntity.getStatusCode());
	}

	@Test
	void checkUsernameAvailabilityAnonymousNoParam() {
		ResponseEntity<UserIdentityAvailability> responseEntity = this.restTemplate
				.getForEntity("/api/users/availability/username", UserIdentityAvailability.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void getCurrentUserMinimalProfileAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.exchange("/api/users/me", HttpMethod.GET, entity,
				ProfileInfo.class);
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileInfo);
	}

	@Test
	void getCurrentUserMinimalProfileAnonymous() {
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.getForEntity("/api/users/me", ProfileInfo.class);
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
		assertVoidProfileInfo(profileInfo);
	}

	@Test
	void getUserMinimalProfileAuthenticatedExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.exchange("/api/users/{username}", HttpMethod.GET,
				entity, ProfileInfo.class, UriUtils.encodeUriVariables("meob.ruin"));
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileInfo);
		assertEquals("Meob", profileInfo.getFirstName());
		assertEquals("Ruin", profileInfo.getSurname());
		assertEquals("meob.ruin", profileInfo.getUsername());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getThumbnailPicture());
		ProfileRelationInfo relationInfo = profileInfo.getRelationInfo();
		assertNotNull(relationInfo);
		assertFalse(relationInfo.getMate());
		assertFalse(relationInfo.getFollower());
		assertFalse(relationInfo.getFollowed());
		assertFalse(relationInfo.getRequester());
		assertFalse(relationInfo.getResponder());
		assertFalse(relationInfo.getBlocked());
	}

	@Test
	void getUserMinimalProfileAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.exchange("/api/users/{username}", HttpMethod.GET,
				entity, ProfileInfo.class, UriUtils.encodeUriVariables("aeon.fruit"));
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileInfo);
		assertEquals("Aeon", profileInfo.getFirstName());
		assertEquals("Fruit", profileInfo.getSurname());
		assertEquals("aeon.fruit", profileInfo.getUsername());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getThumbnailPicture());
		assertNull(profileInfo.getRelationInfo());
	}

	@Test
	void getUserMinimalProfileAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.exchange("/api/users/{username}", HttpMethod.GET,
				entity, ProfileInfo.class, UriUtils.encodeUriVariables("aeon.fruity"));
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNotNull(profileInfo);
		assertVoidProfileInfo(profileInfo);
	}

	@Test
	void getUserMinimalProfileAnonymous() {
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.getForEntity("/api/users/{username}",
				ProfileInfo.class, UriUtils.encodeUriVariables("meob.ruin"));
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
		assertVoidProfileInfo(profileInfo);
	}

	@Test
	void getUserDetailedProfileAuthenticatedExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ProfileDetailedInfo> responseEntity = this.restTemplate.exchange("/api/users/{username}/about",
				HttpMethod.GET, entity, ProfileDetailedInfo.class, UriUtils.encodeUriVariables("meob.ruin"));
		ProfileDetailedInfo profileDetailedInfo = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileDetailedInfo);
		assertEquals("Meob", profileDetailedInfo.getFirstName());
		assertEquals("Ruin", profileDetailedInfo.getSurname());
		assertEquals("meob.ruin", profileDetailedInfo.getUsername());
		assertEquals("meob.ruin@gymeet.com", profileDetailedInfo.getEmail());
		assertEquals("+216 79 785 328", profileDetailedInfo.getPhoneNumber());
		assertEquals("Tunisia", profileDetailedInfo.getCountry());
		assertEquals("Tunis", profileDetailedInfo.getCity());
		assertEquals("Canada", profileDetailedInfo.getHomeCountry());
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.YEAR, 1976);
		birthDate.set(Calendar.MONTH, Calendar.NOVEMBER);
		birthDate.set(Calendar.DAY_OF_MONTH, 16);
		birthDate.set(Calendar.HOUR_OF_DAY, 0);
		birthDate.set(Calendar.MINUTE, 0);
		birthDate.set(Calendar.SECOND, 0);
		birthDate.set(Calendar.MILLISECOND, 0);
		assertEquals(birthDate.getTime(), profileDetailedInfo.getBirthDate());
		assertFalse(profileDetailedInfo.getFemale());
		assertNull(profileDetailedInfo.getProfilePicture());
		assertNull(profileDetailedInfo.getThumbnailPicture());
		ProfileRelationInfo relationInfo = profileDetailedInfo.getRelationInfo();
		assertNotNull(relationInfo);
		assertFalse(relationInfo.getMate());
		assertFalse(relationInfo.getFollower());
		assertFalse(relationInfo.getFollowed());
		assertFalse(relationInfo.getRequester());
		assertFalse(relationInfo.getResponder());
		assertFalse(relationInfo.getBlocked());
	}

	@Test
	void getUserDetailedProfileAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ProfileDetailedInfo> responseEntity = this.restTemplate.exchange("/api/users/{username}/about",
				HttpMethod.GET, entity, ProfileDetailedInfo.class, UriUtils.encodeUriVariables("aeon.fruit"));
		ProfileDetailedInfo profileDetailedInfo = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileDetailedInfo);
		assertEquals("Aeon", profileDetailedInfo.getFirstName());
		assertEquals("Fruit", profileDetailedInfo.getSurname());
		assertEquals("aeon.fruit", profileDetailedInfo.getUsername());
		assertEquals("aeon.fruit@gymeet.com", profileDetailedInfo.getEmail());
		assertEquals("+216 79 852 458", profileDetailedInfo.getPhoneNumber());
		assertEquals("Tunisia", profileDetailedInfo.getCountry());
		assertEquals("Tunis", profileDetailedInfo.getCity());
		assertEquals("Tunisia", profileDetailedInfo.getHomeCountry());
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.YEAR, 1995);
		birthDate.set(Calendar.MONTH, Calendar.OCTOBER);
		birthDate.set(Calendar.DAY_OF_MONTH, 9);
		birthDate.set(Calendar.HOUR_OF_DAY, 0);
		birthDate.set(Calendar.MINUTE, 0);
		birthDate.set(Calendar.SECOND, 0);
		birthDate.set(Calendar.MILLISECOND, 0);
		assertEquals(birthDate.getTime(), profileDetailedInfo.getBirthDate());
		assertFalse(profileDetailedInfo.getFemale());
		assertNull(profileDetailedInfo.getProfilePicture());
		assertNull(profileDetailedInfo.getThumbnailPicture());
		assertNull(profileDetailedInfo.getRelationInfo());
	}

	@Test
	void getUserDetailedProfileAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ProfileDetailedInfo> responseEntity = this.restTemplate.exchange("/api/users/{username}/about",
				HttpMethod.GET, entity, ProfileDetailedInfo.class, UriUtils.encodeUriVariables("aeon.fruity"));
		ProfileDetailedInfo profileDetailedInfo = responseEntity.getBody();
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNotNull(profileDetailedInfo);
		assertNull(profileDetailedInfo.getFirstName());
		assertNull(profileDetailedInfo.getSurname());
		assertNull(profileDetailedInfo.getUsername());
		assertNull(profileDetailedInfo.getEmail());
		assertNull(profileDetailedInfo.getPhoneNumber());
		assertNull(profileDetailedInfo.getCountry());
		assertNull(profileDetailedInfo.getCity());
		assertNull(profileDetailedInfo.getHomeCountry());
		assertNull(profileDetailedInfo.getBirthDate());
		assertNull(profileDetailedInfo.getFemale());
		assertNull(profileDetailedInfo.getProfilePicture());
		assertNull(profileDetailedInfo.getThumbnailPicture());
		assertNull(profileDetailedInfo.getRelationInfo());
	}

	@Test
	void getUserDetailedProfileAnonymous() {
		ResponseEntity<ProfileDetailedInfo> responseEntity = this.restTemplate.getForEntity(
				"/api/users/{username}/about", ProfileDetailedInfo.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateEmailUnavailableAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("aeon.fruit@gymeet.com");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		ResponseEntity<ProfileField> responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}",
				HttpMethod.PUT, entity, ProfileField.class, UriUtils.encodeUriVariables("email"));
		ProfileField profileField = responseEntity.getBody();
		assertEquals(CONFLICT, responseEntity.getStatusCode());
		assertNotNull(profileField);
		assertNull(profileField.getValue());
	}

	@Test
	void updateEmailBadFormatAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("bad-formatted email");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		ResponseEntity<ProfileField> responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}",
				HttpMethod.PUT, entity, ProfileField.class, UriUtils.encodeUriVariables("email"));

		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateEmailNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("email", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateEmailUnavailableAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("email", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updatePasswordNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("password", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updatePasswordNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("password", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateFirstNameNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("first_name", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateFirstNameNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("first_name", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateSurnameNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("surname", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateSurnameNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("surname", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updatePhoneNumberUnavailableAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("+216 79 785 328");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		ResponseEntity<ProfileField> responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}",
				HttpMethod.PUT, entity, ProfileField.class, UriUtils.encodeUriVariables("phone_number"));
		ProfileField profileField = responseEntity.getBody();
		assertEquals(CONFLICT, responseEntity.getStatusCode());
		assertNotNull(profileField);
		assertNull(profileField.getValue());
	}

	@Test
	void updatePhoneNumberNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("phone_number", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileField);
		assertNull(profileField.getValue());

		// revert
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("+216 79 852 458");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}", HttpMethod.PUT, entity,
				ProfileField.class, UriUtils.encodeUriVariables("phone_number"));
		assertEquals(OK, responseEntity.getStatusCode());
		profileField = responseEntity.getBody();
		assertNotNull(profileField);
		assertEquals("+216 79 852 458", profileField.getValue());
	}

	@Test
	void updatePhoneNumberNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("phone_number", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateUsernameBadFormatAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("bad-formatted@username/\\;^");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		ResponseEntity<ProfileField> responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}",
				HttpMethod.PUT, entity, ProfileField.class, UriUtils.encodeUriVariables("username"));

		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateUsernameNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("username", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateUsernameNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("username", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateHomeCountryNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("home_country", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileField);
		assertNull(profileField.getValue());

		// revert
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("Tunisia");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}", HttpMethod.PUT, entity,
				ProfileField.class, UriUtils.encodeUriVariables("home_country"));
		assertEquals(OK, responseEntity.getStatusCode());
		profileField = responseEntity.getBody();
		assertNotNull(profileField);
		assertEquals("Tunisia", profileField.getValue());
	}

	@Test
	void updateHomeCountryNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("home_country", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateCountryNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("country", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileField);
		assertNull(profileField.getValue());

		// revert
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("Tunisia");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}", HttpMethod.PUT, entity,
				ProfileField.class, UriUtils.encodeUriVariables("country"));
		assertEquals(OK, responseEntity.getStatusCode());
		profileField = responseEntity.getBody();
		assertNotNull(profileField);
		assertEquals("Tunisia", profileField.getValue());
	}

	@Test
	void updateCountryNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("country", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateCityNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("city", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(profileField);
		assertNull(profileField.getValue());

		// revert
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("Tunis");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}", HttpMethod.PUT, entity,
				ProfileField.class, UriUtils.encodeUriVariables("city"));
		assertEquals(OK, responseEntity.getStatusCode());
		profileField = responseEntity.getBody();
		assertNotNull(profileField);
		assertEquals("Tunis", profileField.getValue());
	}

	@Test
	void updateCityNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("city", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateBirthDateBadFormatAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("bad-formatted birth date");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		ResponseEntity<ProfileField> responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}",
				HttpMethod.PUT, entity, ProfileField.class, UriUtils.encodeUriVariables("birth_date"));

		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateBirthDateNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("birth_date", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateBirthDateNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("birth_date", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateGenderBadFormatAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ProfileField field = new ProfileField("bad-formatted gender");
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		ResponseEntity<ProfileField> responseEntity = this.restTemplate.exchange("/api/users/me/about?field={field}",
				HttpMethod.PUT, entity, ProfileField.class, UriUtils.encodeUriVariables("gender"));

		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateGenderNullAuthenticated() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("gender", true);
		ProfileField profileField = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(profileField);
	}

	@Test
	void updateGenderNullAnonymous() {
		ResponseEntity<ProfileField> responseEntity = updateFieldNull("gender", false);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getPictureAuthenticatedUsernameExistPictureExist() {
		updatePicture();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/users/{username}/picture?sz=0",
				HttpMethod.GET, entity, Resource.class, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(MediaType.IMAGE_JPEG, responseEntity.getHeaders().getContentType());
		assertEquals("attachment", responseEntity.getHeaders().getContentDisposition().getType());
		String filename = responseEntity.getHeaders().getContentDisposition().getFilename();
		assertNotNull(filename);
		assertTrue(filename.endsWith(".jpg"));

		deletePicture();
	}

	@Test
	void getPictureAuthenticatedUsernameExistPictureExistSizeNotExist() {
		updatePicture();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/users/{username}/picture?sz=4",
				HttpMethod.GET, entity, Resource.class, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());

		deletePicture();
	}

	@Test
	void getPictureAuthenticatedUsernameExistPictureNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/users/{username}/picture?sz=0",
				HttpMethod.GET, entity, Resource.class, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(NO_CONTENT, responseEntity.getStatusCode());
		assertNull(responseEntity.getBody());
	}

	@Test
	void getPictureAuthenticatedUsernameNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/users/{username}/picture?sz=0",
				HttpMethod.GET, entity, Resource.class, UriUtils.encodeUriVariables("aeon.fruity"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getPictureAnonymous() {
		ResponseEntity<ProfileDetailedInfo> responseEntity = this.restTemplate.getForEntity(
				"/api/users/{username}/picture?sz=0", ProfileDetailedInfo.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateDeletePictureAuthenticated() {
		updatePicture();
		deletePicture();
	}

	@Test
	void updatePictureUnknownFileFormat() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", new ClassPathResource("data.sql"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/picture", HttpMethod.PUT,
				entity, Void.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void updatePictureNull() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", null);
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/picture", HttpMethod.PUT,
				entity, Void.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void updatePictureAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/picture", HttpMethod.PUT,
				entity, Void.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void deletePictureAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/picture", HttpMethod.DELETE,
				entity, Void.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getUserActivitiesAuthenticatedExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Set<ActivityInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/activities", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ActivityInfo>>() {
				}, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getUserActivitiesAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/{username}/activities",
				HttpMethod.GET, entity, String.class, UriUtils.encodeUriVariables("aeon.fruity"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getUserActivitiesAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/{username}/activities",
				HttpMethod.GET, entity, String.class, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void addActivityAuthenticatedExistNotInList() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<ActivityInfo> responseEntity = this.restTemplate
				.exchange("/api/users/me/activities/{activityId}", HttpMethod.POST, entity, ActivityInfo.class, 2);
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());

		// revert
		ResponseEntity<Void> deleteResponseEntity = this.restTemplate.exchange("/api/users/me/activities/{activityId}",
				HttpMethod.DELETE, entity, Void.class, 2);
		assertEquals(NO_CONTENT, deleteResponseEntity.getStatusCode());
		assertNull(deleteResponseEntity.getBody());
	}

	@Test
	void addActivityAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<ActivityInfo> responseEntity = this.restTemplate
				.exchange("/api/users/me/activities/{activityId}", HttpMethod.POST, entity, ActivityInfo.class, 5);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void addActivityAuthenticatedExistInList() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<ActivityInfo> responseEntity = this.restTemplate
				.exchange("/api/users/me/activities/{activityId}", HttpMethod.POST, entity, ActivityInfo.class, 1);
		assertEquals(CONFLICT, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void addActivityAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/activities/{activityId}",
				HttpMethod.POST, entity, String.class, 1);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void removeActivityAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/activities/{activityId}",
				HttpMethod.DELETE, entity, Void.class, 5);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNull(responseEntity.getBody());
	}

	@Test
	void removeActivityAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/activities/{activityId}",
				HttpMethod.DELETE, entity, Void.class, 5);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	// TODO - Test UserController.findByName

	private ResponseEntity<ProfileField> updateFieldNull(String fieldName, boolean authorized) {
		HttpHeaders headers = new HttpHeaders();
		if (authorized) {
			headers.set("Authorization", getAeonFruitToken());
		}
		ProfileField field = new ProfileField(null);
		HttpEntity<ProfileField> entity = new HttpEntity<>(field, headers);
		return this.restTemplate.exchange("/api/users/me/about?field={field}", HttpMethod.PUT, entity,
				ProfileField.class, UriUtils.encodeUriVariables(fieldName));
	}

	private void updatePicture() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/picture", HttpMethod.PUT,
				entity, Void.class);
		assertEquals(NO_CONTENT, responseEntity.getStatusCode());
	}

	private void deletePicture() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/users/me/picture", HttpMethod.DELETE,
				entity, Void.class);
		assertEquals(NO_CONTENT, responseEntity.getStatusCode());
	}

	private void assertVoidProfileInfo(ProfileInfo profileInfo) {
		assertNotNull(profileInfo);
		assertNull(profileInfo.getFirstName());
		assertNull(profileInfo.getSurname());
		assertNull(profileInfo.getUsername());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getThumbnailPicture());
		assertNull(profileInfo.getRelationInfo());
	}

}
