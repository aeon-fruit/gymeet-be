package com.gymeet.be.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.SignUpPostRequest;
import com.gymeet.be.service.AuthService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class AuthControllerTests {

	private TestRestTemplate restTemplate;
	private JwtAuthenticationResponse aeonFruitToken;

	@Autowired
	AuthControllerTests(AuthService authService, TestRestTemplate restTemplate) {
		this.restTemplate = restTemplate;

		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("mypass");
		aeonFruitToken = authService.authenticateUser(loginPostRequest);
	}

	@Test
	void testRegisterUserAnonymousPhoneNumberNull() {
		SignUpPostRequest signUpPostRequest = new SignUpPostRequest();
		signUpPostRequest.setFirstName("New");
		signUpPostRequest.setSurname("User");
		signUpPostRequest.setEmail("new.user@gymeet.com");
		signUpPostRequest.setPassword("newpass");
		signUpPostRequest.setFemale(true);
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 18);
		birthDate.set(Calendar.MONTH, Calendar.APRIL);
		birthDate.set(Calendar.YEAR, 1995);
		signUpPostRequest.setBirthDate(birthDate.getTime());
		signUpPostRequest.setPhoneNumber(null);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.postForEntity("/api/auth/signup",
				signUpPostRequest, ProfileInfo.class);
		ProfileInfo profileInfo = responseEntity.getBody();
		List<String> location = responseEntity.getHeaders().get(HttpHeaders.LOCATION);
		assertEquals(CREATED, responseEntity.getStatusCode());
		assertNotNull(profileInfo);
		assertEquals(signUpPostRequest.getFirstName(), profileInfo.getFirstName());
		assertEquals(signUpPostRequest.getSurname(), profileInfo.getSurname());
		assertNotNull(profileInfo.getUsername());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getThumbnailPicture());
		assertNull(profileInfo.getRelationInfo());
		assertNotNull(location);
		assertFalse(location.isEmpty());
		assertNotNull(location.get(0));
		assertTrue(location.get(0).contains("/api/users/me"));
	}

	@Test
	void testRegisterUserAnonymousPhoneNumberNullEmailNull() {
		SignUpPostRequest signUpPostRequest = new SignUpPostRequest();
		signUpPostRequest.setFirstName("New");
		signUpPostRequest.setSurname("User2");
		signUpPostRequest.setEmail(null);
		signUpPostRequest.setPassword("newpass2");
		signUpPostRequest.setFemale(true);
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 18);
		birthDate.set(Calendar.MONTH, Calendar.APRIL);
		birthDate.set(Calendar.YEAR, 1995);
		signUpPostRequest.setBirthDate(birthDate.getTime());
		signUpPostRequest.setPhoneNumber(null);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.postForEntity("/api/auth/signup",
				signUpPostRequest, ProfileInfo.class);
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertVoidProfileInfo(profileInfo);
	}

	@Test
	void testRegisterUserAnonymousPhoneNumberNullEmailUnavailable() {
		SignUpPostRequest signUpPostRequest = new SignUpPostRequest();
		signUpPostRequest.setFirstName("New");
		signUpPostRequest.setSurname("User3");
		signUpPostRequest.setEmail("aeon.fruit@gymeet.com");
		signUpPostRequest.setPassword("newpass3");
		signUpPostRequest.setFemale(true);
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 18);
		birthDate.set(Calendar.MONTH, Calendar.APRIL);
		birthDate.set(Calendar.YEAR, 1995);
		signUpPostRequest.setBirthDate(birthDate.getTime());
		signUpPostRequest.setPhoneNumber(null);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.postForEntity("/api/auth/signup",
				signUpPostRequest, ProfileInfo.class);
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(CONFLICT, responseEntity.getStatusCode());
		assertVoidProfileInfo(profileInfo);
	}

	@Test
	void testRegisterUserAnonymousPhoneNumberUnavailable() {
		SignUpPostRequest signUpPostRequest = new SignUpPostRequest();
		signUpPostRequest.setFirstName("New");
		signUpPostRequest.setSurname("User4");
		signUpPostRequest.setEmail("new.user4@gymeet.com");
		signUpPostRequest.setPassword("newpass4");
		signUpPostRequest.setFemale(true);
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 18);
		birthDate.set(Calendar.MONTH, Calendar.APRIL);
		birthDate.set(Calendar.YEAR, 1995);
		signUpPostRequest.setBirthDate(birthDate.getTime());
		signUpPostRequest.setPhoneNumber("+33 79 78 58 31 28");
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.postForEntity("/api/auth/signup",
				signUpPostRequest, ProfileInfo.class);
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(CONFLICT, responseEntity.getStatusCode());
		assertVoidProfileInfo(profileInfo);
	}

	@Test
	void testRegisterUserAuthenticatedPhoneNumberNotNull() {
		SignUpPostRequest signUpPostRequest = new SignUpPostRequest();
		signUpPostRequest.setFirstName("Test2");
		signUpPostRequest.setSurname("Fruit");
		signUpPostRequest.setEmail("test2.fruit@gymeet.com");
		signUpPostRequest.setPassword("test2pass");
		signUpPostRequest.setFemale(false);
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 18);
		birthDate.set(Calendar.MONTH, Calendar.APRIL);
		birthDate.set(Calendar.YEAR, 1995);
		signUpPostRequest.setBirthDate(birthDate.getTime());
		signUpPostRequest.setPhoneNumber("+2 123456789");
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", aeonFruitToken.getTokenType() + " " + aeonFruitToken.getAccessToken());
		HttpEntity<SignUpPostRequest> entity = new HttpEntity<>(signUpPostRequest, headers);
		ResponseEntity<ProfileInfo> responseEntity = this.restTemplate.exchange("/api/auth/signup", HttpMethod.POST,
				entity, ProfileInfo.class);
		ProfileInfo profileInfo = responseEntity.getBody();
		assertEquals(FORBIDDEN, responseEntity.getStatusCode());
		assertVoidProfileInfo(profileInfo);
	}

	@Test
	void testAuthenticateUserAnonymousGood() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("mypass");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNotNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAnonymousGoodWithoutRole() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("ron.rook@gymeet.com");
		loginPostRequest.setPassword("hispass");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAnonymousLoginBad() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruity@gymeet.com");
		loginPostRequest.setPassword("mypass");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAnonymousLoginNull() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin(null);
		loginPostRequest.setPassword("mypass");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAnonymousLoginBlank() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("");
		loginPostRequest.setPassword("mypass");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAnonymousPasswordBad() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("badpass");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAnonymousPasswordNull() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword(null);
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAnonymousPasswordBlank() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("");
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.postForEntity("/api/auth/signin",
				loginPostRequest, JwtAuthenticationResponse.class);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAutheticatedGood() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("mypass");
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", aeonFruitToken.getTokenType() + " " + aeonFruitToken.getAccessToken());
		HttpEntity<LoginPostRequest> entity = new HttpEntity<>(loginPostRequest, headers);
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.exchange("/api/auth/signin",
				HttpMethod.POST, entity, JwtAuthenticationResponse.class);
		assertEquals(FORBIDDEN, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	@Test
	void testAuthenticateUserAuthenticatedLoginBlank() {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("");
		loginPostRequest.setPassword("mypass");
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", aeonFruitToken.getTokenType() + " " + aeonFruitToken.getAccessToken());
		HttpEntity<LoginPostRequest> entity = new HttpEntity<>(loginPostRequest, headers);
		ResponseEntity<JwtAuthenticationResponse> responseEntity = this.restTemplate.exchange("/api/auth/signin",
				HttpMethod.POST, entity, JwtAuthenticationResponse.class);
		assertEquals(FORBIDDEN, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertNull(responseEntity.getBody().getAccessToken());
	}

	private void assertVoidProfileInfo(ProfileInfo profileInfo) {
		assertNotNull(profileInfo);
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getThumbnailPicture());
		assertNull(profileInfo.getRelationInfo());
		assertNull(profileInfo.getFirstName());
		assertNull(profileInfo.getSurname());
		assertNull(profileInfo.getUsername());
	}

}
