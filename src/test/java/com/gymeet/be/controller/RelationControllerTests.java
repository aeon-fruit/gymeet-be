package com.gymeet.be.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriUtils;

import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.service.AuthService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RelationControllerTests {

	private TestRestTemplate restTemplate;
	private JwtAuthenticationResponse aeonFruitToken;

	@Autowired
	RelationControllerTests(AuthService authService, TestRestTemplate restTemplate) {
		this.restTemplate = restTemplate;

		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("mypass");
		aeonFruitToken = authService.authenticateUser(loginPostRequest);
	}

	private String getAeonFruitToken() {
		return aeonFruitToken.getTokenType() + " " + aeonFruitToken.getAccessToken();
	}

	@Test
	void getCurrentUserMateListAnonymous() {
		ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/api/users/me/relations?rt=mate",
				String.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getCurrentUserMateListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange("/api/users/me/relations?rt=mate",
				HttpMethod.GET, entity, new ParameterizedTypeReference<Set<ProfileInfo>>() {
				});
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getCurrentUserFollowerListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations?rt=follower", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				});
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getCurrentUserFollowedListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations?rt=followed", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				});
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getCurrentUserRequesterListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations?rt=requester", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				});
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getCurrentUserResponderListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations?rt=responder", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				});
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getCurrentUserBlockingListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations?rt=blocking", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				});
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getCurrentUserUnknownListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/relations?rt=nothing",
				HttpMethod.GET, entity, String.class);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getCurrentUserNoListAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange("/api/users/me/relations",
				HttpMethod.GET, entity, new ParameterizedTypeReference<Set<ProfileInfo>>() {
				});
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void getMateListAnonymous() {
		ResponseEntity<String> responseEntity = this.restTemplate.getForEntity(
				"/api/users/{username}/relations?rt=mate", String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getMateListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/{username}/relations?rt=mate",
				HttpMethod.GET, entity, String.class, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getFollowerListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=follower", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getFollowedListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=followed", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getRequesterListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=requester", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getResponderListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=responder", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getBlockingListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=blocking", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getUnknownListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/{username}/relations?rt=nothing",
				HttpMethod.GET, entity, String.class, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getNoListAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/{username}/relations",
				HttpMethod.GET, entity, String.class, UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void getMateListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=mate", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				}, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getFollowerListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=follower", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				}, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getFollowedListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=followed", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ProfileInfo>>() {
				}, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void getRequesterListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=requester", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getResponderListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=responder", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getBlockingListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/{username}/relations?rt=blocking", HttpMethod.GET, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getUnknownListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/{username}/relations?rt=nothing",
				HttpMethod.GET, entity, String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void getNullListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/{username}/relations?rt",
				HttpMethod.GET, entity, String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void getNoListAuthenticatedNotSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ProfileInfo>> responseEntity = this.restTemplate.exchange("/api/users/{username}/relations",
				HttpMethod.GET, entity, new ParameterizedTypeReference<Set<ProfileInfo>>() {
				}, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void addRequestAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void addUnknownAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=random", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void addNullAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/relations/{username}?rt",
				HttpMethod.POST, entity, String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void addNothingAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/relations/{username}",
				HttpMethod.POST, entity, String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void addMateAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/relations/{username}?rt=mate",
				HttpMethod.POST, entity, String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void addRequestAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void addRequestAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void addRequestAuthenticatedBlocking() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("aeon.blocker"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void addRequestAuthenticatedMate() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("aeon.mate"));
		assertEquals(CONFLICT, responseEntity.getStatusCode());
	}

	@Test
	void addResponseAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=responder", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void addFollowerAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=follower", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void addBlockingAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=blocking", HttpMethod.POST, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeRequestAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void removeUnknownAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=random", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeNullAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/relations/{username}?rt",
				HttpMethod.DELETE, entity, String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeNothingAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/relations/{username}",
				HttpMethod.DELETE, entity, String.class, UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void removeFollowedAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=followed", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("meob.ruin"));
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void removeRequestAuthenticatedSelf() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("aeon.fruit"));
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void removeRequestAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeRequestAuthenticatedBlocking() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=requester", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("aeon.blocker"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeMateAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/users/me/relations/{username}?rt=mate",
				HttpMethod.DELETE, entity, String.class, UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeFollowerAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=follower", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeResponderAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=responder", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeBlockingAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				"/api/users/me/relations/{username}?rt=blocking", HttpMethod.DELETE, entity, String.class,
				UriUtils.encodeUriVariables("lombok.com"));
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

}
