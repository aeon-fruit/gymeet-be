package com.gymeet.be.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;

import com.gymeet.be.payload.ActivityDetailedInfo;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ActivityPostRequest;
import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.service.AuthService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class ActivityControllerTests {

	private TestRestTemplate restTemplate;
	private JwtAuthenticationResponse aeonFruitToken;

	@Autowired
	ActivityControllerTests(AuthService authService, TestRestTemplate restTemplate) {
		this.restTemplate = restTemplate;

		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("mypass");
		aeonFruitToken = authService.authenticateUser(loginPostRequest);
	}

	private String getAeonFruitToken() {
		return aeonFruitToken.getTokenType() + " " + aeonFruitToken.getAccessToken();
	}

	@Test
	void findAllActivitiesAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ActivityInfo>> responseEntity = this.restTemplate.exchange("/api/activities/",
				HttpMethod.GET, entity, new ParameterizedTypeReference<Set<ActivityInfo>>() {
				});
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void findAllActivitiesAnonymous() {
		ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/api/activities/", String.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void findActivityByPartialNameAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<ActivityInfo>> responseEntity = this.restTemplate.exchange(
				"/api/activities/search?q={query}", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<ActivityInfo>>() {
				}, "ball");
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void findActivityByPartialNameAnonymous() {
		ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/api/activities/search", String.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void findActivityByIdFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ActivityDetailedInfo> responseEntity = this.restTemplate.exchange("/api/activities/{id}",
				HttpMethod.GET, entity, ActivityDetailedInfo.class, 2L);
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void findActivityByIdNotFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<ActivityDetailedInfo> responseEntity = this.restTemplate.exchange("/api/activities/{id}",
				HttpMethod.GET, entity, ActivityDetailedInfo.class, 1234567890L);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void findActivityByIdAnonymous() {
		ResponseEntity<ActivityDetailedInfo> responseEntity = this.restTemplate.getForEntity("/api/activities/{id}",
				ActivityDetailedInfo.class, 2);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void addActivityAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("Football", "");
		parameters.add("data", activityPostRequest);
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);

		ResponseEntity<ActivityDetailedInfo> responseEntity = this.restTemplate.exchange("/api/activities/",
				HttpMethod.POST, entity, ActivityDetailedInfo.class);
		assertEquals(CONFLICT, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void addActivityAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("Football", "");
		HttpEntity<ActivityPostRequest> entity = new HttpEntity<>(activityPostRequest, headers);
		ResponseEntity<ActivityDetailedInfo> responseEntity = this.restTemplate.exchange("/api/activities/",
				HttpMethod.POST, entity, ActivityDetailedInfo.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateActivityDetailsUnavailableAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("Handball", "");
		HttpEntity<ActivityPostRequest> entity = new HttpEntity<>(activityPostRequest, headers);
		ResponseEntity<ActivityDetailedInfo> responseEntity = this.restTemplate.exchange("/api/activities/{id}",
				HttpMethod.PUT, entity, ActivityDetailedInfo.class, 1L);
		assertEquals(CONFLICT, responseEntity.getStatusCode());
	}

	@Test
	void updateActivityDetailsUnavailableAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("Handball", "");
		HttpEntity<ActivityPostRequest> entity = new HttpEntity<>(activityPostRequest, headers);
		ResponseEntity<ActivityDetailedInfo> responseEntity = this.restTemplate.exchange("/api/activities/{id}",
				HttpMethod.PUT, entity, ActivityDetailedInfo.class, 1L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void removeActivityByIdNotFoundAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/activities/{id}", HttpMethod.DELETE,
				entity, Void.class, 5L);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeActivityByIdNotFoundAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/activities/{id}", HttpMethod.DELETE,
				entity, Void.class, 5L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getIconAuthenticatedFoundIconExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/activities/{id}/icon",
				HttpMethod.GET, entity, Resource.class, 1L);
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(MediaType.IMAGE_JPEG, responseEntity.getHeaders().getContentType());
		assertEquals("attachment", responseEntity.getHeaders().getContentDisposition().getType());
		String filename = responseEntity.getHeaders().getContentDisposition().getFilename();
		assertNotNull(filename);
		assertTrue(filename.endsWith(".jpg"));
	}

	@Test
	void getIconAuthenticatedNotFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/activities/{id}/icon",
				HttpMethod.GET, entity, Resource.class, 1234567890L);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void getIconAnonymous() {
		ResponseEntity<Resource> responseEntity = this.restTemplate.getForEntity("/api/activities/{id}/icon",
				Resource.class, 1L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateActivityIconAuthenticatedUnknownFormat() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", new ClassPathResource("data.sql"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/activities/{id}/icon",
				HttpMethod.PUT, entity, Resource.class, 2L);
		assertEquals(BAD_REQUEST, responseEntity.getStatusCode());
	}

	@Test
	void updateActivityIconAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/activities/{id}/icon",
				HttpMethod.PUT, entity, Resource.class, 1L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void findVenuesByActivityIdFoundAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Set<VenueDetailedInfo>> responseEntity = this.restTemplate.exchange(
				"/api/activities/{id}/venues", HttpMethod.GET, entity,
				new ParameterizedTypeReference<Set<VenueDetailedInfo>>() {
				}, 1L);
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void findVenuesByActivityIdNotFoundAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/activities/{id}/venues",
				HttpMethod.GET, entity, String.class, 123457890L);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void findVenuesByActivityIdAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/activities/{id}/venues",
				HttpMethod.GET, entity, String.class, 1L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

}
