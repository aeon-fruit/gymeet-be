package com.gymeet.be.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;

import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileDetailedInfo;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.payload.VenueInfo;
import com.gymeet.be.payload.VenuePostRequest;
import com.gymeet.be.service.AuthService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class VenueControllerTests {

	private TestRestTemplate restTemplate;
	private JwtAuthenticationResponse aeonFruitToken;

	@Autowired
	VenueControllerTests(AuthService authService, TestRestTemplate restTemplate) {
		this.restTemplate = restTemplate;

		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin("aeon.fruit@gymeet.com");
		loginPostRequest.setPassword("mypass");
		aeonFruitToken = authService.authenticateUser(loginPostRequest);
	}

	private String getAeonFruitToken() {
		return aeonFruitToken.getTokenType() + " " + aeonFruitToken.getAccessToken();
	}

	@Test
	void findVenueByPartialNameAuthenticatedFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Set<VenueInfo>> responseEntity = this.restTemplate.exchange("/api/venues/search?q={query}",
				HttpMethod.GET, entity, new ParameterizedTypeReference<Set<VenueInfo>>() {
				}, "eum 13");
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void findVenueByPartialNameAuthenticatedNotFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = this.restTemplate.exchange("/api/venues/search?q={query}",
				HttpMethod.GET, entity, String.class, "not found");
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void findVenueByPartialNameAnonymous() {
		ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/api/venues/search?q=nothing",
				String.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void findVenueByIdFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.exchange("/api/venues/{id}",
				HttpMethod.GET, entity, VenueDetailedInfo.class, 2L);
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
	}

	@Test
	void findVenueByIdNotFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.exchange("/api/venues/{id}",
				HttpMethod.GET, entity, VenueDetailedInfo.class, 1234567890L);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void findVenueByIdAnonymous() {
		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.getForEntity("/api/venues/{id}",
				VenueDetailedInfo.class, 2);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void addVenueAuthenticatedNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		VenuePostRequest venuePostRequest = new VenuePostRequest("Doffy Coliseum", 0., 0., "Grand Line", "",
				new HashSet<>());
		parameters.add("data", venuePostRequest);
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);

		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.exchange("/api/venues/", HttpMethod.POST,
				entity, VenueDetailedInfo.class);
		assertEquals(CREATED, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		URI location = responseEntity.getHeaders().getLocation();
		assertNotNull(location);
		assertTrue(location.toString().contains("/api/venues/" + responseEntity.getBody().getId()));
	}

	@Test
	void addVenueAuthenticatedExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		VenuePostRequest venuePostRequest = new VenuePostRequest("Gymnasium 13", 0., 0., "Grand Line", "",
				new HashSet<>());
		parameters.add("data", venuePostRequest);
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);

		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.exchange("/api/venues/", HttpMethod.POST,
				entity, VenueDetailedInfo.class);
		assertEquals(CONFLICT, responseEntity.getStatusCode());
	}

	@Test
	void addVenueAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		VenuePostRequest venuePostRequest = new VenuePostRequest("Gymnasium 13", 0., 0., "Grand Line", "",
				new HashSet<>());
		HttpEntity<VenuePostRequest> entity = new HttpEntity<>(venuePostRequest, headers);
		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.exchange("/api/venues", HttpMethod.POST,
				entity, VenueDetailedInfo.class);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateVenueDetailsUnavailableAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		VenuePostRequest venuePostRequest = new VenuePostRequest("Museum 13", 0., 0., "Grand Line", "",
				new HashSet<>());
		HttpEntity<VenuePostRequest> entity = new HttpEntity<>(venuePostRequest, headers);
		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.exchange("/api/venues/{id}",
				HttpMethod.PUT, entity, VenueDetailedInfo.class, 1L);
		assertEquals(CONFLICT, responseEntity.getStatusCode());
	}

	@Test
	void updateVenueDetailsUnavailableAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		VenuePostRequest venuePostRequest = new VenuePostRequest("Gymnasium 13", 0., 0., "Grand Line", "",
				new HashSet<>());
		HttpEntity<VenuePostRequest> entity = new HttpEntity<>(venuePostRequest, headers);
		ResponseEntity<VenueDetailedInfo> responseEntity = this.restTemplate.exchange("/api/venues/{id}",
				HttpMethod.PUT, entity, VenueDetailedInfo.class, 1L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void removeVenueByIdNotFoundAuthenticated() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/venues/{id}", HttpMethod.DELETE, entity,
				Void.class, 5L);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void removeVenueByIdNotFoundAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<>(null, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/venues/{id}", HttpMethod.DELETE, entity,
				Void.class, 5L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void getPictureAuthenticatedFoundPictureExist() {
		updatePicture();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/venues/{id}/picture?sz=0",
				HttpMethod.GET, entity, Resource.class, 1L);
		assertEquals(OK, responseEntity.getStatusCode());
		assertNotNull(responseEntity.getBody());
		assertEquals(MediaType.IMAGE_JPEG, responseEntity.getHeaders().getContentType());
		assertEquals("attachment", responseEntity.getHeaders().getContentDisposition().getType());
		String filename = responseEntity.getHeaders().getContentDisposition().getFilename();
		assertNotNull(filename);
		assertTrue(filename.endsWith(".jpg"));
	}

	@Test
	void getPictureAuthenticatedFoundPictureNotExist() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/venues/{id}/picture?sz=0",
				HttpMethod.GET, entity, Resource.class, 2L);
		assertEquals(NO_CONTENT, responseEntity.getStatusCode());
		assertNull(responseEntity.getBody());
	}

	@Test
	void getPictureAuthenticatedNotFound() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		HttpEntity<String> entity = new HttpEntity<>(headers);
		ResponseEntity<Resource> responseEntity = this.restTemplate.exchange("/api/venues/{id}/picture?sz=0",
				HttpMethod.GET, entity, Resource.class, 1234567890L);
		assertEquals(NOT_FOUND, responseEntity.getStatusCode());
	}

	@Test
	void getPictureAnonymous() {
		ResponseEntity<ProfileDetailedInfo> responseEntity = this.restTemplate
				.getForEntity("/api/venues/{id}/picture?sz=0", ProfileDetailedInfo.class, 1L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	@Test
	void updateVenuePictureAuthenticated() {
		updatePicture();
	}

	@Test
	void updateVenuePictureAnonymous() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/venues/{id}/picture", HttpMethod.PUT,
				entity, Void.class, 1L);
		assertEquals(UNAUTHORIZED, responseEntity.getStatusCode());
	}

	private void updatePicture() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", getAeonFruitToken());
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
		parameters.add("file", new ClassPathResource("original.jpg"));
		HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
		ResponseEntity<Void> responseEntity = this.restTemplate.exchange("/api/venues/{id}/picture", HttpMethod.PUT,
				entity, Void.class, 1L);
		assertEquals(NO_CONTENT, responseEntity.getStatusCode());
	}

}
