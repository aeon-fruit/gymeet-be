package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.RelationFollower;
import com.gymeet.be.model.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RelationFollowerRepositoryTests {

	@Autowired
	private RelationFollowerRepository relationFollowerRepository;

	@Autowired
	private UserRepository userRepository;

	@Test
	void findByFollowerExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);

		List<RelationFollower> list = relationFollowerRepository.findByFollower(user1);
		assertEquals(2, list.size());
	}

	@Test
	void findByFollowerNotExist() {
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		List<RelationFollower> list = relationFollowerRepository.findByFollower(user2);
		assertTrue(list.isEmpty());
	}

	@Test
	void findByFollowedExist() {
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		List<RelationFollower> list = relationFollowerRepository.findByFollowed(user2);
		assertEquals(1, list.size());
	}

	@Test
	void findByFollowedNotExist() {
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		List<RelationFollower> list = relationFollowerRepository.findByFollowed(user4);
		assertTrue(list.isEmpty());
	}

	@Test
	void findByFollowerAndFollowedExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);

		RelationFollower relation = relationFollowerRepository.findByFollowerAndFollowed(user1, user2);
		assertNotNull(relation);

		relation = relationFollowerRepository.findByFollowerAndFollowed(user2, user1);
		assertNull(relation);

		relation = relationFollowerRepository.findByFollowerAndFollowed(user1, user3);
		assertNotNull(relation);

		relation = relationFollowerRepository.findByFollowerAndFollowed(user3, user1);
		assertNotNull(relation);
	}

	@Test
	void findByFollowerAndFollowedNotExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		RelationFollower relation = relationFollowerRepository.findByFollowerAndFollowed(user3, user4);
		assertNull(relation);

		relation = relationFollowerRepository.findByFollowerAndFollowed(user4, user3);
		assertNull(relation);
	}

	@Test
	void existsByFollowerAndFollowedExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		Boolean exists = relationFollowerRepository.existsByFollowerAndFollowed(user1, user2);
		assertTrue(exists);

		exists = relationFollowerRepository.existsByFollowerAndFollowed(user2, user1);
		assertFalse(exists);
	}

	@Test
	void existsByFollowerAndFollowedNotExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		Boolean exists = relationFollowerRepository.existsByFollowerAndFollowed(user3, user4);
		assertFalse(exists);

		exists = relationFollowerRepository.existsByFollowerAndFollowed(user4, user3);
		assertFalse(exists);
	}

	@Test
	void severeConnectionReciprocatedExist() {
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);
		User user7 = userRepository.findByEmail("user7@gymeet.com").orElse(null);
		assertNotNull(user7);

		Boolean exists = relationFollowerRepository.existsByFollowerAndFollowed(user5, user7);
		assertTrue(exists);
		exists = relationFollowerRepository.existsByFollowerAndFollowed(user7, user5);
		assertTrue(exists);

		relationFollowerRepository.severeConnection(user7.getId(), user5.getId());

		exists = relationFollowerRepository.existsByFollowerAndFollowed(user5, user7);
		assertFalse(exists);
		exists = relationFollowerRepository.existsByFollowerAndFollowed(user7, user5);
		assertFalse(exists);
	}

	@Test
	void severeConnectionNotReciprocatedExist() {
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);
		User user6 = userRepository.findByEmail("user6@gymeet.com").orElse(null);
		assertNotNull(user6);

		Boolean exists = relationFollowerRepository.existsByFollowerAndFollowed(user5, user6);
		assertTrue(exists);
		exists = relationFollowerRepository.existsByFollowerAndFollowed(user6, user5);
		assertFalse(exists);

		relationFollowerRepository.severeConnection(user5.getId(), user6.getId());

		exists = relationFollowerRepository.existsByFollowerAndFollowed(user5, user6);
		assertFalse(exists);
		exists = relationFollowerRepository.existsByFollowerAndFollowed(user6, user5);
		assertFalse(exists);
	}

	@Test
	void severeConnectionNotExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);

		Boolean exists = relationFollowerRepository.existsByFollowerAndFollowed(user1, user5);
		assertFalse(exists);
		exists = relationFollowerRepository.existsByFollowerAndFollowed(user5, user1);
		assertFalse(exists);

		relationFollowerRepository.severeConnection(user1.getId(), user5.getId());

		exists = relationFollowerRepository.existsByFollowerAndFollowed(user1, user5);
		assertFalse(exists);
		exists = relationFollowerRepository.existsByFollowerAndFollowed(user5, user1);
		assertFalse(exists);

		List<RelationFollower> list = relationFollowerRepository.findByFollower(user1);
		assertEquals(2, list.size());
	}

}
