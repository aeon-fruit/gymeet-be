package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.RelationBlocking;
import com.gymeet.be.model.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RelationBlockingRepositoryTests {

	@Autowired
	private RelationBlockingRepository relationBlockingRepository;

	@Autowired
	private UserRepository userRepository;

	@Test
	void findByBlockingExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);

		List<RelationBlocking> list = relationBlockingRepository.findByBlocking(user1);
		assertEquals(2, list.size());
	}

	@Test
	void findByBlockingNotExist() {
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		List<RelationBlocking> list = relationBlockingRepository.findByBlocking(user2);
		assertTrue(list.isEmpty());
	}

	@Test
	void findByBlockingAndBlockedExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);

		RelationBlocking relation = relationBlockingRepository.findByBlockingAndBlocked(user1, user5);
		assertNotNull(relation);

		relation = relationBlockingRepository.findByBlockingAndBlocked(user5, user1);
		assertNull(relation);
	}

	@Test
	void findByBlockingAndBlockedNotExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		RelationBlocking relation = relationBlockingRepository.findByBlockingAndBlocked(user3, user4);
		assertNull(relation);

		relation = relationBlockingRepository.findByBlockingAndBlocked(user4, user3);
		assertNull(relation);
	}

	@Test
	void existsByBlockingAndBlockedExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);

		Boolean exists = relationBlockingRepository.existsByBlockingAndBlocked(user1, user5);
		assertTrue(exists);

		exists = relationBlockingRepository.existsByBlockingAndBlocked(user5, user1);
		assertFalse(exists);
	}

	@Test
	void existsByBlockingAndBlockedNotExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		Boolean exists = relationBlockingRepository.existsByBlockingAndBlocked(user3, user4);
		assertFalse(exists);

		exists = relationBlockingRepository.existsByBlockingAndBlocked(user4, user3);
		assertFalse(exists);
	}

}
