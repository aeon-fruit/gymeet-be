package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.Role;
import com.gymeet.be.util.RoleName;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RoleRepositoryTests {

	@Autowired
	private RoleRepository roleRepository;

	@Test
	void roleRepositoryFindByNameUser() {
		Role role = roleRepository.findByName(RoleName.ROLE_USER).orElse(null);
		assertNotNull(role);
		assertEquals(RoleName.ROLE_USER, role.getName());
	}

	@Test
	void roleRepositoryFindByNameAdmin() {
		Role role = roleRepository.findByName(RoleName.ROLE_ADMIN).orElse(null);
		assertNotNull(role);
		assertEquals(RoleName.ROLE_ADMIN, role.getName());
	}

}
