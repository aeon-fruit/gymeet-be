package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class UserRepositoryTests {

	@Autowired
	private UserRepository userRepository;

	@Test
	void findByEmailExist() {
		User user = userRepository.findByEmail("aeon.fruit@gymeet.com").orElse(null);
		assertNotNull(user);
		assertEquals("aeon.fruit", user.getUsername());
	}

	@Test
	void findByEmailNotExist() {
		User user = userRepository.findByEmail("new.fruit@gymeet.com").orElse(null);
		assertNull(user);
	}

	@Test
	void findByPhoneNumberExist() {
		User user = userRepository.findByPhoneNumber("+216 79 852 458").orElse(null);
		assertNotNull(user);
		assertEquals("aeon.fruit", user.getUsername());
	}

	@Test
	void findByPhoneNumberNotExist() {
		User user = userRepository.findByPhoneNumber("+31 79 85 28 41 58").orElse(null);
		assertNull(user);
	}

	@Test
	void findByEmailOrPhoneNumberEmailExist() {
		User user = userRepository.findByEmailOrPhoneNumber("aeon.fruit@gymeet.com", "aeon.fruit@gymeet.com")
				.orElse(null);
		assertNotNull(user);
		assertEquals("aeon.fruit", user.getUsername());
	}

	@Test
	void findByEmailOrPhoneNumberPhoneNumberExist() {
		User user = userRepository.findByEmailOrPhoneNumber("+216 79 852 458", "+216 79 852 458").orElse(null);
		assertNotNull(user);
		assertEquals("aeon.fruit", user.getUsername());
	}

	@Test
	void findByEmailOrPhoneNumberNotExist() {
		User user = userRepository.findByEmailOrPhoneNumber("new.fruit@gymeet.com", "+31 79 85 28 41 58").orElse(null);
		assertNull(user);
	}

	@Test
	void findByUsernameExist() {
		User user = userRepository.findByUsername("aeon.fruit").orElse(null);
		assertNotNull(user);
		assertEquals("aeon.fruit", user.getUsername());
	}

	@Test
	void findByUsernameNotExist() {
		User user = userRepository.findByEmail("new.fruit").orElse(null);
		assertNull(user);
	}

	@Test
	void findByCityAndCountryExistOne() {
		List<User> users = userRepository.findByCityAndCountry("Toulouse", "France");
		assertEquals(1, users.size());
	}

	@Test
	void findByCityAndCountryExistMultiple() {
		List<User> users = userRepository.findByCityAndCountry("Tunis", "Tunisia");
		assertEquals(2, users.size());
	}

	@Test
	void findByCityAndCountryCityNotExist() {
		List<User> users = userRepository.findByCityAndCountry("Gabes", "Tunisia");
		assertEquals(0, users.size());
	}

	@Test
	void findByCityAndCountryCountryNotExist() {
		List<User> users = userRepository.findByCityAndCountry("Tunis", "Canada");
		assertEquals(0, users.size());
	}

	@Test
	void findByCityAndCountryNotExist() {
		List<User> users = userRepository.findByCityAndCountry("Montreal", "Canada");
		assertEquals(0, users.size());
	}

	@Test
	void findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCaseExact() {
		List<User> users = userRepository.findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase("Aeon",
				"Fruit");
		assertEquals(1, users.size());
	}

	@Test
	void findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCaseExactIgnoreCase() {
		List<User> users = userRepository.findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase("aEoN",
				"FrUiT");
		assertEquals(1, users.size());
	}

	@Test
	void findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCasePartial() {
		List<User> users = userRepository.findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase("eo", "rui");
		assertEquals(2, users.size());
	}

	@Test
	void findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCaseNameNotExistSurnameExist() {
		List<User> users = userRepository.findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase("Bocca",
				"Fruit");
		assertEquals(1, users.size());
	}

	@Test
	void findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCaseNameExistSurnameNotExist() {
		List<User> users = userRepository.findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase("Aeon",
				"Lupo");
		assertEquals(1, users.size());
	}

	@Test
	void findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCaseNotExist() {
		List<User> users = userRepository.findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase("Bocca",
				"Lupo");
		assertEquals(0, users.size());
	}

	@Test
	void findByHomeCountryExist() {
		List<User> users = userRepository.findByHomeCountry("Tunisia");
		assertEquals(2, users.size());
	}

	@Test
	void findByHomeCountryNotExist() {
		List<User> users = userRepository.findByHomeCountry("France");
		assertEquals(0, users.size());
	}

	@Test
	void existsByEmailExist() {
		Boolean exists = userRepository.existsByEmail("aeon.fruit@gymeet.com");
		assertTrue(exists);
	}

	@Test
	void existsByEmailNotExist() {
		Boolean exists = userRepository.existsByEmail("aeon.fruit@hello.com");
		assertFalse(exists);
	}

	@Test
	void existsByPhoneNumberExist() {
		Boolean exists = userRepository.existsByPhoneNumberNotNullAndPhoneNumber("+216 79 785 328");
		assertTrue(exists);
	}

	@Test
	void existsByPhoneNumberNotExist() {
		Boolean exists = userRepository.existsByPhoneNumberNotNullAndPhoneNumber("79 785 328");
		assertFalse(exists);
	}

	@Test
	void existsByEmailOrPhoneNumberExist() {
		Boolean exists = userRepository.existsByEmailOrPhoneNumberNotNullAndPhoneNumber("aeon.fruit@gymeet.com",
				"+216 79 785 328");
		assertTrue(exists);
	}

	@Test
	void existsByEmailOrPhoneNumberEmailExistPhoneNumberNotExist() {
		Boolean exists = userRepository.existsByEmailOrPhoneNumberNotNullAndPhoneNumber("aeon.fruit@gymeet.com",
				"79 785 328");
		assertTrue(exists);
	}

	@Test
	void existsByEmailOrPhoneNumberEmailNotExistPhoneNumberExist() {
		Boolean exists = userRepository.existsByEmailOrPhoneNumberNotNullAndPhoneNumber("aeon.fruit@hello.com",
				"+216 79 785 328");
		assertTrue(exists);
	}

	@Test
	void existsByEmailOrPhoneNumberNotExist() {
		Boolean exists = userRepository.existsByEmailOrPhoneNumberNotNullAndPhoneNumber("aeon.fruit@hello.com",
				"79 785 328");
		assertFalse(exists);
	}

	@Test
	void existsByUsernameExist() {
		Boolean exists = userRepository.existsByUsername("aeon.fruit");
		assertTrue(exists);
	}

	@Test
	void existsByUsernameNotExist() {
		Boolean exists = userRepository.existsByUsername("nemo.outis");
		assertFalse(exists);
	}

}
