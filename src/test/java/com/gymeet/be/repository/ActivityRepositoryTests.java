package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.Activity;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class ActivityRepositoryTests {

	@Autowired
	private ActivityRepository activityRepository;

	@Test
	void findByNameContainingIgnoreCaseFoundExact() {
		List<Activity> activities = activityRepository.findByNameContainingIgnoreCase("Car racing");
		assertNotNull(activities);
		assertEquals(1, activities.size());
	}

	@Test
	void findByNameContainingIgnoreCaseFoundPartial() {
		List<Activity> activities = activityRepository.findByNameContainingIgnoreCase("ball");
		assertNotNull(activities);
		assertEquals(2, activities.size());
	}

	@Test
	void findByNameContainingIgnoreCaseNotFound() {
		List<Activity> activities = activityRepository.findByNameContainingIgnoreCase("nothing");
		assertNotNull(activities);
		assertTrue(activities.isEmpty());
	}

	@Test
	void findByNameIgnoreCaseFound() {
		Activity activity = activityRepository.findByNameIgnoreCase("Car racing").orElse(null);
		assertNotNull(activity);
		assertEquals("Car racing", activity.getName());
	}

	@Test
	void findByNameIgnoreCaseNotFound() {
		Activity activity = activityRepository.findByNameIgnoreCase("nothing").orElse(null);
		assertNull(activity);
	}

}
