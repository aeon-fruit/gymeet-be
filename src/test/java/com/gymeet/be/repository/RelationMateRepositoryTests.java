package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.RelationMate;
import com.gymeet.be.model.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RelationMateRepositoryTests {

	@Autowired
	private RelationMateRepository relationMateRepository;

	@Autowired
	private UserRepository userRepository;

	@Test
	void findByFirstExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);

		List<RelationMate> list = relationMateRepository.findByFirst(user3);
		assertEquals(2, list.size());
	}

	@Test
	void findByFirstNotExist() {
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);

		List<RelationMate> list = relationMateRepository.findByFirst(user5);
		assertTrue(list.isEmpty());
	}

	@Test
	void findBySecondExist() {
		User user4 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user4);

		List<RelationMate> list = relationMateRepository.findBySecond(user4);
		assertEquals(1, list.size());
	}

	@Test
	void findBySecondNotExist() {
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);

		List<RelationMate> list = relationMateRepository.findBySecond(user5);
		assertTrue(list.isEmpty());
	}

	@Test
	void findByFirstAndSecondOrSecondAndFirstExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		RelationMate relation = relationMateRepository.findByFirstAndSecondOrSecondAndFirst(user1, user2, user1, user2);
		assertNotNull(relation);

		relation = relationMateRepository.findByFirstAndSecondOrSecondAndFirst(user2, user1, user2, user1);
		assertNotNull(relation);
	}

	@Test
	void findByFirstAndSecondOrSecondAndFirstNotExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		RelationMate relation = relationMateRepository.findByFirstAndSecondOrSecondAndFirst(user1, user4, user1, user4);
		assertNull(relation);

		relation = relationMateRepository.findByFirstAndSecondOrSecondAndFirst(user4, user1, user4, user1);
		assertNull(relation);
	}

	@Test
	void existsByFirstAndSecondOrSecondAndFirstExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		Boolean exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user1, user2, user1, user2);
		assertTrue(exists);

		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user2, user1, user2, user1);
		assertTrue(exists);
	}

	@Test
	void existsByFirstAndSecondOrSecondAndFirstNotExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		Boolean exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user1, user4, user1, user4);
		assertFalse(exists);

		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user4, user1, user4, user1);
		assertFalse(exists);
	}

	@Test
	void severeConnectionFirstExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		Boolean exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user3, user4, user3, user4);
		assertTrue(exists);

		relationMateRepository.severeConnection(user3.getId(), user4.getId());

		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user3, user4, user3, user4);
		assertFalse(exists);
		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user4, user3, user4, user3);
		assertFalse(exists);
	}

	@Test
	void severeConnectionSecondExist() {
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		Boolean exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user2, user4, user2, user4);
		assertTrue(exists);

		relationMateRepository.severeConnection(user4.getId(), user2.getId());

		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user2, user4, user2, user4);
		assertFalse(exists);
		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user4, user2, user4, user2);
		assertFalse(exists);
	}

	@Test
	void severeConnectionFirstNotExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);

		Boolean exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user1, user5, user1, user5);
		assertFalse(exists);

		relationMateRepository.severeConnection(user1.getId(), user5.getId());

		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user1, user5, user1, user5);
		assertFalse(exists);
		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user5, user1, user5, user1);
		assertFalse(exists);

		List<RelationMate> list = relationMateRepository.findByFirst(user1);
		assertEquals(1, list.size());
	}

	@Test
	void severeConnectionSecondNotExist() {
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);

		Boolean exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user5, user1, user5, user1);
		assertFalse(exists);

		relationMateRepository.severeConnection(user5.getId(), user1.getId());

		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user1, user5, user1, user5);
		assertFalse(exists);
		exists = relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user5, user1, user5, user1);
		assertFalse(exists);

		List<RelationMate> list = relationMateRepository.findBySecond(user1);
		assertEquals(1, list.size());
	}

}
