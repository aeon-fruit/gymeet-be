package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.Venue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class VenueRepositoryTests {

	@Autowired
	private VenueRepository venueRepository;

	@Test
	void findByNameContainingIgnoreCaseFoundExact() {
		List<Venue> venues = venueRepository.findByNameContainingIgnoreCase("Gymnasium 13");
		assertNotNull(venues);
		assertEquals(1, venues.size());
	}

	@Test
	void findByNameContainingIgnoreCaseFoundPartial() {
		List<Venue> venues = venueRepository.findByNameContainingIgnoreCase("eum 13");
		assertNotNull(venues);
		assertEquals(2, venues.size());
	}

	@Test
	void findByNameContainingIgnoreCaseNotFound() {
		List<Venue> venues = venueRepository.findByNameContainingIgnoreCase("nothing");
		assertNotNull(venues);
		assertTrue(venues.isEmpty());
	}

	@Test
	void findByNameIgnoreCaseFound() {
		Venue venue = venueRepository.findByNameIgnoreCase("Gymnasium 13").orElse(null);
		assertNotNull(venue);
		assertEquals("Gymnasium 13", venue.getName());
	}

	@Test
	void findByNameIgnoreCaseNotFound() {
		Venue venue = venueRepository.findByNameIgnoreCase("nothing").orElse(null);
		assertNull(venue);
	}

}
