package com.gymeet.be.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.model.RelationRequester;
import com.gymeet.be.model.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RelationRequesterRepositoryTests {

	@Autowired
	private RelationRequesterRepository relationRequesterRepository;

	@Autowired
	private UserRepository userRepository;

	@Test
	void findByRequesterExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);

		List<RelationRequester> list = relationRequesterRepository.findByRequester(user1);
		assertEquals(2, list.size());
	}

	@Test
	void findByRequesterNotExist() {
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		List<RelationRequester> list = relationRequesterRepository.findByRequester(user2);
		assertTrue(list.isEmpty());
	}

	@Test
	void findByRequestedExist() {
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		List<RelationRequester> list = relationRequesterRepository.findByRequested(user4);
		assertEquals(1, list.size());
	}

	@Test
	void findByRequestedNotExist() {
		User user2 = userRepository.findByEmail("user2@gymeet.com").orElse(null);
		assertNotNull(user2);

		List<RelationRequester> list = relationRequesterRepository.findByRequested(user2);
		assertTrue(list.isEmpty());
	}

	@Test
	void findByRequesterAndRequestedExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);
		User user7 = userRepository.findByEmail("user7@gymeet.com").orElse(null);
		assertNotNull(user7);

		RelationRequester relation = relationRequesterRepository.findByRequesterAndRequested(user1, user4);
		assertNotNull(relation);

		relation = relationRequesterRepository.findByRequesterAndRequested(user4, user1);
		assertNull(relation);

		relation = relationRequesterRepository.findByRequesterAndRequested(user1, user7);
		assertNotNull(relation);

		relation = relationRequesterRepository.findByRequesterAndRequested(user7, user1);
		assertNull(relation);
	}

	@Test
	void findByRequesterAndRequestedNotExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		RelationRequester relation = relationRequesterRepository.findByRequesterAndRequested(user3, user4);
		assertNull(relation);

		relation = relationRequesterRepository.findByRequesterAndRequested(user4, user3);
		assertNull(relation);
	}

	@Test
	void existsByRequesterAndRequestedExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		Boolean exists = relationRequesterRepository.existsByRequesterAndRequested(user1, user4);
		assertTrue(exists);

		exists = relationRequesterRepository.existsByRequesterAndRequested(user4, user1);
		assertFalse(exists);
	}

	@Test
	void existsByRequesterAndRequestedNotExist() {
		User user3 = userRepository.findByEmail("user3@gymeet.com").orElse(null);
		assertNotNull(user3);
		User user4 = userRepository.findByEmail("user4@gymeet.com").orElse(null);
		assertNotNull(user4);

		Boolean exists = relationRequesterRepository.existsByRequesterAndRequested(user3, user4);
		assertFalse(exists);

		exists = relationRequesterRepository.existsByRequesterAndRequested(user4, user3);
		assertFalse(exists);
	}

	@Test
	void severeConnectionExist() {
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);
		User user6 = userRepository.findByEmail("user6@gymeet.com").orElse(null);
		assertNotNull(user6);

		Boolean exists = relationRequesterRepository.existsByRequesterAndRequested(user5, user6);
		assertTrue(exists);
		exists = relationRequesterRepository.existsByRequesterAndRequested(user6, user5);
		assertFalse(exists);

		relationRequesterRepository.severeConnection(user5.getId(), user6.getId());

		exists = relationRequesterRepository.existsByRequesterAndRequested(user5, user6);
		assertFalse(exists);
		exists = relationRequesterRepository.existsByRequesterAndRequested(user6, user5);
		assertFalse(exists);
	}

	@Test
	void severeConnectionNotExist() {
		User user1 = userRepository.findByEmail("user1@gymeet.com").orElse(null);
		assertNotNull(user1);
		User user5 = userRepository.findByEmail("user5@gymeet.com").orElse(null);
		assertNotNull(user5);

		Boolean exists = relationRequesterRepository.existsByRequesterAndRequested(user1, user5);
		assertFalse(exists);
		exists = relationRequesterRepository.existsByRequesterAndRequested(user5, user1);
		assertFalse(exists);

		relationRequesterRepository.severeConnection(user1.getId(), user5.getId());

		exists = relationRequesterRepository.existsByRequesterAndRequested(user1, user5);
		assertFalse(exists);
		exists = relationRequesterRepository.existsByRequesterAndRequested(user5, user1);
		assertFalse(exists);

		List<RelationRequester> list = relationRequesterRepository.findByRequester(user1);
		assertEquals(2, list.size());
	}

}
