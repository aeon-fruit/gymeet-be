package com.gymeet.be.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.ResourceUtils;

import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.payload.VenueInfo;
import com.gymeet.be.payload.VenuePostRequest;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class VenueServiceTests {

	@Autowired
	private VenueService venueService;

	@Test
	void findVenuesByPartialNameFoundExactName() throws ResourceNotFoundException, ModelMappingException {
		Set<VenueInfo> venueInfoSet = venueService.findVenuesByPartialName("Gymnasium 13");
		assertEquals(1, venueInfoSet.size());
	}

	@Test
	void findVenuesByPartialNameFoundMultiplePartialName() throws ResourceNotFoundException, ModelMappingException {
		Set<VenueInfo> venueInfoSet = venueService.findVenuesByPartialName("eum 13");
		assertEquals(2, venueInfoSet.size());
	}

	@Test
	void findVenuesByPartialNameNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			venueService.findVenuesByPartialName("Not Found");
		});
	}

	@Test
	void findVenueByIdFound() throws ResourceNotFoundException, ModelMappingException {
		VenueDetailedInfo venueDetailedInfo = venueService.findVenueById(1L);
		assertNotNull(venueDetailedInfo);
	}

	@Test
	void findVenueByIdNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			venueService.findVenueById(1234567890L);
		});
	}

	@Test
	void getPictureFoundPictureExistSize0()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		getPictureFoundPictureExist(0);
	}

	@Test
	void getPictureFoundPictureExistSize1()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		getPictureFoundPictureExist(1);
	}

	@Test
	void getPictureFoundPictureExistSize2()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		getPictureFoundPictureExist(2);
	}

	@Test
	void getPictureFoundPictureExistSize3()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		updatePicture("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			venueService.getPicture(1L, 3);
		});

		deletePicture();
	}

	@Test
	void getPictureFoundPictureNotExistSize0() throws ResourceNotFoundException {
		Resource picture = venueService.getPicture(2L, 0);
		assertNull(picture);
	}

	@Test
	void getPictureNotFoundSize0() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			venueService.getPicture(1234567890L, 0);
		});
	}

	@Test
	void addVenueGood() throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			ResourceConflictException, IOException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.jpg")));
		addVenueGoodWithPictureParam(picture);
	}

	@Test
	void addVenuePictureNull() throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			ResourceConflictException, IOException {
		addVenueGoodWithPictureParam(null);
	}

	@Test
	void addVenueNameExist() {
		VenuePostRequest venuePostRequest = new VenuePostRequest("Gymnasium 13", 0., 0., "Grand Line", "",
				new HashSet<>());
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			venueService.addVenue(venuePostRequest, null);
		});
	}

	@Test
	void updateVenueDetailsFoundNameNotExist() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setName("Wano no Kuni");
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundNameExist() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setName("Gymnasium 13");
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			venueService.updateVenueDetails(venueId, venuePostRequest);
		});

		// revert
		venueService.removeVenueById(venueId);
	}

	@Test
	void updateVenueDetailsFoundNameBlank() throws ResourceNotFoundException, IOException, UnknownFileTypeException,
			ResourceConflictException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setName("");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			venueService.updateVenueDetails(venueId, venuePostRequest);
		});

		// revert
		venueService.removeVenueById(venueId);
	}

	@Test
	void updateVenueDetailsFoundNameNull() throws ResourceNotFoundException, IOException, UnknownFileTypeException,
			ResourceConflictException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setName(null);
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			venueService.updateVenueDetails(venueId, venuePostRequest);
		});

		// revert
		venueService.removeVenueById(venueId);
	}

	@Test
	void updateVenueDetailsFoundLatitudeNotNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setLatitude(1.);
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundLatitudeNull() throws ResourceNotFoundException, IOException, UnknownFileTypeException,
			ResourceConflictException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setLatitude(null);
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			venueService.updateVenueDetails(venueId, venuePostRequest);
		});

		// revert
		venueService.removeVenueById(venueId);
	}

	@Test
	void updateVenueDetailsFoundLongitudeNotNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setLongitude(1.);
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundLongitudeNull() throws ResourceNotFoundException, IOException, UnknownFileTypeException,
			ResourceConflictException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setLongitude(null);
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			venueService.updateVenueDetails(venueId, venuePostRequest);
		});

		// revert
		venueService.removeVenueById(venueId);
	}

	@Test
	void updateVenueDetailsFoundAddressNotNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setAddress("Shin Sekai");
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundAddressBlank() throws ResourceNotFoundException, IOException, UnknownFileTypeException,
			ResourceConflictException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setAddress("");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			venueService.updateVenueDetails(venueId, venuePostRequest);
		});

		// revert
		venueService.removeVenueById(venueId);
	}

	@Test
	void updateVenueDetailsFoundAddressNull() throws ResourceNotFoundException, IOException, UnknownFileTypeException,
			ResourceConflictException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setAddress(null);
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			venueService.updateVenueDetails(venueId, venuePostRequest);
		});

		// revert
		venueService.removeVenueById(venueId);
	}

	@Test
	void updateVenueDetailsFoundDescriptionNotNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setDescription("A new description");
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundDescriptionBlank() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setDescription("   ");
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundDescriptionNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setDescription(null);
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundActivitiesNotEmpty() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		activityIdSet = new HashSet<>();
		activityIdSet.add(2L);
		activityIdSet.add(3L);
		venuePostRequest.setActivities(activityIdSet);
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, activityIdSet, picture);

		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	@Test
	void updateVenueDetailsFoundActivitiesEmpty() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setActivities(new HashSet<>());
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, new HashSet<>(), picture);

		compareResultAndRevert(venueId, venuePostRequest, new HashSet<>(), picture);
	}

	@Test
	void updateVenueDetailsFoundActivitiesNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venuePostRequest.setActivities(null);
		VenueDetailedInfo info = venueService.updateVenueDetails(venueId, venuePostRequest);
		compareResultWithoutId(info, venuePostRequest, new HashSet<>(), picture);

		compareResultAndRevert(venueId, venuePostRequest, new HashSet<>(), picture);
	}

	@Test
	void updateVenueDetailsNotFound() {
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			venueService.updateVenueDetails(1234567890L, venuePostRequest);
		});
	}

	@Test
	void updatePicturePJPEG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdatePicture("name.pjpg", "original.pjpg", "image/pjpeg", "classpath:original.pjpg");
	}

	@Test
	void updatePictureJPEG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdatePicture("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");
	}

	@Test
	void updatePicturePNG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdatePicture("name.png", "original.png", "image/png", "classpath:original.png");
	}

	@Test
	void updatePictureBMP()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdatePicture("name.bmp", "original.bmp", "image/bmp", "classpath:original.bmp");
	}

	@Test
	void updatePictureXPNG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdatePicture("name.png", "original.png", "image/x-png", "classpath:original.png");
	}

	@Test
	void updatePictureNull()
			throws UnknownFileTypeException, IOException, ResourceNotFoundException, ModelMappingException {
		deletePicture();
	}

	@Test
	void updatePictureNotImage() throws IOException, ResourceNotFoundException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("data.sql", "data.sql", null,
				new FileInputStream(ResourceUtils.getFile("classpath:data.sql")));
		assertNotNull(picture);
		Assertions.assertThrows(UnknownFileTypeException.class, () -> venueService.updateVenuePicture(1L, picture));
		VenueDetailedInfo venueDetailedInfo = venueService.findVenueById(1L);
		assertNotNull(venueDetailedInfo);
		assertNull(venueDetailedInfo.getLargePicture());
		assertNull(venueDetailedInfo.getMediumPicture());
		assertNull(venueDetailedInfo.getThumbnailPicture());
	}

	@Test
	void updateVenuePictureNotFound() throws IOException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Assertions.assertThrows(ResourceNotFoundException.class,
				() -> venueService.updateVenuePicture(1234567890L, picture));
	}

	@Test
	void removeVenueByIdFound() throws ResourceNotFoundException, IOException, UnknownFileTypeException,
			ResourceConflictException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);

		Long venueId = addVenue(venuePostRequest, picture);

		venueService.removeVenueById(venueId);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> venueService.findVenueById(venueId));
	}

	@Test
	void removeVenueByIdNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> venueService.removeVenueById(1234567890L));
	}

	private Long addVenue(VenuePostRequest venuePostRequest, MockMultipartFile picture) throws UnknownFileTypeException,
			ResourceNotFoundException, ModelMappingException, ResourceConflictException, IOException {
		VenueDetailedInfo info = venueService.addVenue(venuePostRequest, picture);
		compareResultWithoutId(info, venuePostRequest, venuePostRequest.getActivities(), picture);

		Set<VenueInfo> venueInfoSet = venueService.findVenuesByPartialName("Dressrosa");
		assertEquals(1, venueInfoSet.size());

		return venueInfoSet.toArray(new VenueInfo[0])[0].getId();
	}

	private void addVenueGoodWithPictureParam(MockMultipartFile picture) throws ResourceNotFoundException,
			UnknownFileTypeException, ModelMappingException, ResourceConflictException, IOException {
		Set<Long> activityIdSet = new HashSet<>();
		activityIdSet.add(1L);
		activityIdSet.add(2L);
		VenuePostRequest venuePostRequest = new VenuePostRequest("Dressrosa", 0., 0., "Grand Line", "", activityIdSet);
		Long venueId = addVenue(venuePostRequest, picture);
		compareResultAndRevert(venueId, venuePostRequest, activityIdSet, picture);
	}

	private void compareResult(Long venueId, VenuePostRequest venuePostRequest, Set<Long> activityIdSet,
			MockMultipartFile picture) throws ResourceNotFoundException, ModelMappingException {
		VenueDetailedInfo venueInfo = venueService.findVenueById(venueId);
		compareResultWithoutId(venueInfo, venuePostRequest, activityIdSet, picture);
	}

	private void compareResultWithoutId(VenueDetailedInfo venueInfo, VenuePostRequest venuePostRequest,
			Set<Long> activityIdSet, MockMultipartFile picture) {
		assertEquals(venuePostRequest.getName(), venueInfo.getName());
		assertEquals(venuePostRequest.getLatitude(), venueInfo.getLatitude());
		assertEquals(venuePostRequest.getLongitude(), venueInfo.getLongitude());
		assertEquals(venuePostRequest.getAddress(), venueInfo.getAddress());
		assertEquals(venuePostRequest.getDescription(), venueInfo.getDescription());
		if (picture != null) {
			assertNotNull(venueInfo.getLargePicture());
			assertNotNull(venueInfo.getMediumPicture());
			assertNotNull(venueInfo.getThumbnailPicture());
			assertTrue(venueInfo.getLargePicture().endsWith(".jpg"));
			assertTrue(venueInfo.getMediumPicture().endsWith(".jpg"));
			assertTrue(venueInfo.getThumbnailPicture().endsWith(".jpg"));
		} else {
			assertNull(venueInfo.getLargePicture());
			assertNull(venueInfo.getMediumPicture());
			assertNull(venueInfo.getThumbnailPicture());
		}
		Set<ActivityInfo> activityInfoSet = venueInfo.getActivityInfoSet();
		assertNotNull(activityInfoSet);
		assertEquals(activityIdSet.size(), activityInfoSet.size());
		Set<Long> resultIdSet = new HashSet<>();
		activityInfoSet.forEach(activityInfo -> resultIdSet.add(activityInfo.getId()));
		assertEquals(activityIdSet.size(), resultIdSet.size());
		assertTrue(activityIdSet.containsAll(resultIdSet));
		assertTrue(resultIdSet.containsAll(activityIdSet));
	}

	private void compareResultAndRevert(Long venueId, VenuePostRequest venuePostRequest, Set<Long> activityIdSet,
			MockMultipartFile picture) throws ResourceNotFoundException, ModelMappingException, IOException {
		compareResult(venueId, venuePostRequest, activityIdSet, picture);
		// revert
		venueService.removeVenueById(venueId);
	}

	private void reversibleUpdatePicture(String name, String originalFilename, String contentType,
			String resourceLocation)
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		updatePicture(name, originalFilename, contentType, resourceLocation);
		deletePicture();
	}

	private void updatePicture(String name, String originalFilename, String contentType, String resourceLocation)
			throws UnknownFileTypeException, IOException, ResourceNotFoundException, ModelMappingException {
		MockMultipartFile picture = new MockMultipartFile(name, originalFilename, contentType,
				new FileInputStream(ResourceUtils.getFile(resourceLocation)));
		assertNotNull(picture);
		venueService.updateVenuePicture(1L, picture);
		VenueDetailedInfo venueDetailedInfo = venueService.findVenueById(1L);
		assertNotNull(venueDetailedInfo);
		assertNotNull(venueDetailedInfo.getLargePicture());
		assertNotNull(venueDetailedInfo.getMediumPicture());
		assertNotNull(venueDetailedInfo.getThumbnailPicture());
		assertTrue(venueDetailedInfo.getLargePicture().endsWith(".jpg"));
		assertTrue(venueDetailedInfo.getMediumPicture().endsWith(".jpg"));
		assertTrue(venueDetailedInfo.getThumbnailPicture().endsWith(".jpg"));
	}

	private void deletePicture()
			throws UnknownFileTypeException, IOException, ResourceNotFoundException, ModelMappingException {
		venueService.updateVenuePicture(1L, null);
		VenueDetailedInfo venueDetailedInfo = venueService.findVenueById(1L);
		assertNotNull(venueDetailedInfo);
		assertNull(venueDetailedInfo.getLargePicture());
		assertNull(venueDetailedInfo.getMediumPicture());
		assertNull(venueDetailedInfo.getThumbnailPicture());
	}

	private void getPictureFoundPictureExist(int size)
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		updatePicture("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");

		Resource picture = venueService.getPicture(1L, size);
		assertNotNull(picture);
		assertNotNull(picture.getFilename());
		assertTrue(picture.getFilename().endsWith(".jpg"));

		deletePicture();
	}

}
