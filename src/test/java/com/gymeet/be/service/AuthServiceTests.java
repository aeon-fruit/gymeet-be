package com.gymeet.be.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.exception.AppException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.SignUpPostRequest;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class AuthServiceTests {

	@Autowired
	private AuthService authService;

	private JwtAuthenticationResponse authenticate(String login, String password) {
		LoginPostRequest loginPostRequest = new LoginPostRequest();
		loginPostRequest.setLogin(login);
		loginPostRequest.setPassword(password);
		return authService.authenticateUser(loginPostRequest);
	}

	@Test
	void authenticateUserExistEmail() {
		JwtAuthenticationResponse jwtAuthenticationResponse = authenticate("aeon.fruit@gymeet.com", "mypass");
		assertNotNull(jwtAuthenticationResponse);
	}

	@Test
	void authenticateUserExistPhoneNumber() {
		JwtAuthenticationResponse jwtAuthenticationResponse = authenticate("+216 79 785 328", "thepass");
		assertNotNull(jwtAuthenticationResponse);
	}

	@Test
	void authenticateUserExistEmailBadPassword() {
		Assertions.assertThrows(BadCredentialsException.class, () -> {
			authenticate("aeon.fruit@gymeet.com", "thepass");
		});
	}

	@Test
	void registerUserPhoneNumberNull()
			throws ResourceConflictException, ResourceBadRequestException, ModelMappingException, AppException {
		ProfileInfo profileInfo = register("Good1", "Fella1", "good.fella1@gymeet.com", null, "goodpass", new Date(),
				false);
		assertNotNull(profileInfo);
		assertEquals("Good1.Fella1", profileInfo.getUsername());
		assertEquals("Good1", profileInfo.getFirstName());
		assertEquals("Fella1", profileInfo.getSurname());
		assertNull(profileInfo.getThumbnailPicture());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getRelationInfo());
	}

	@Test
	void registerUserPhoneNumberBlank()
			throws ResourceConflictException, ResourceBadRequestException, ModelMappingException, AppException {
		ProfileInfo profileInfo = register("Good2", "Fella2", "good.fella2@gymeet.com", "", "goodpass", new Date(),
				false);
		assertNotNull(profileInfo);
		assertEquals("Good2.Fella2", profileInfo.getUsername());
		assertEquals("Good2", profileInfo.getFirstName());
		assertEquals("Fella2", profileInfo.getSurname());
		assertNull(profileInfo.getThumbnailPicture());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getRelationInfo());
	}

	@Test
	void registerUserEmailExist() {
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			register("Good3", "Fella3", "aeon.fruit@gymeet.com", null, "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserPhoneNumberExist() {
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			register("Good4", "Fella4", "good.fruit4@gymeet.com", "+216 79 785 328", "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserFirstNameNull() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register(null, "Fella5", "good.fella5@gymeet.com", null, "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserFirstNameBlank() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register("", "Fella6", "good.fella6@gymeet.com", null, "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserSurnameNull() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register("Good5", null, "good.fruit7@gymeet.com", "+216 79 785 302", "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserSurnameBlank() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register("Good6", "", "good.fruit8@gymeet.com", "+216 79 785 303", "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserEmailNull() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register("Good7", "Fella7", null, "+216 79 785 304", "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserEmailBlank() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register("Good8", "Fella8", "", "+216 79 785 305", "goodpass", new Date(), false);
		});
	}

	@Test
	void registerUserPasswordNull() {
		Assertions.assertThrows(ResourceBadRequestException.class, () -> {
			register("Good9", "Fella9", "good.fruit9@gymeet.com", "+216 79 785 306", null, new Date(), false);
		});
	}

	@Test
	void registerUserPasswordBlank() {
		Assertions.assertThrows(ResourceBadRequestException.class, () -> {
			register("Good10", "Fella10", "good.fruit10@gymeet.com", "+216 79 785 307", "", new Date(), false);
		});
	}

	@Test
	void registerUserBirthDateNull() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register("Good11", "Fella11", "good.fella11@gymeet.com", "", "goodpass", null, false);
		});
	}

	@Test
	void registerUserFemaleNull() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			register("Good12", "Fella12", "good.fella12@gymeet.com", "", "goodpass", new Date(), null);
		});
	}

	private ProfileInfo register(String firstName, String surname, String email, String phoneNumber, String password,
			Date birthDate, Boolean female)
			throws ResourceConflictException, ResourceBadRequestException, ModelMappingException, AppException {
		SignUpPostRequest signUpPostRequest = new SignUpPostRequest();
		signUpPostRequest.setFirstName(firstName);
		signUpPostRequest.setSurname(surname);
		signUpPostRequest.setEmail(email);
		signUpPostRequest.setPhoneNumber(phoneNumber);
		signUpPostRequest.setPassword(password);
		signUpPostRequest.setBirthDate(birthDate);
		signUpPostRequest.setFemale(female);
		return authService.registerUser(signUpPostRequest);
	}

}
