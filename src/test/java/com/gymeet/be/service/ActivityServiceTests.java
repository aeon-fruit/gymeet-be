package com.gymeet.be.service;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.ResourceUtils;

import com.gymeet.be.config.FileStorageProperties;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ActivityDetailedInfo;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ActivityPostRequest;
import com.gymeet.be.payload.VenueDetailedInfo;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class ActivityServiceTests {

	@Autowired
	private ActivityService activityService;

	@Autowired
	private FileStorageProperties fileStorageProperties;

	@BeforeEach
	void copyIcons() throws IOException {
		Path location = Paths.get(fileStorageProperties.getActivity().getIcon().getDir() + "/1.jpg").toAbsolutePath()
				.normalize();
		Files.copy(ResourceUtils.getFile("classpath:original.jpg").toPath(), location, REPLACE_EXISTING);
		location = Paths.get(fileStorageProperties.getActivity().getIcon().getDir() + "/2.jpg").toAbsolutePath()
				.normalize();
		Files.copy(ResourceUtils.getFile("classpath:original.jpg").toPath(), location, REPLACE_EXISTING);
		location = Paths.get(fileStorageProperties.getActivity().getIcon().getDir() + "/3.jpg").toAbsolutePath()
				.normalize();
		Files.copy(ResourceUtils.getFile("classpath:original.jpg").toPath(), location, REPLACE_EXISTING);
	}

	@Test
	void findAllActivities() throws ModelMappingException {
		Set<ActivityInfo> activities = activityService.findAllActivities();
		assertEquals(3, activities.size());
	}

	@Test
	void findActivitiesByPartialNameFoundExactName() throws ResourceNotFoundException, ModelMappingException {
		Set<ActivityInfo> activityInfoSet = activityService.findActivitiesByPartialName("Car racing");
		assertEquals(1, activityInfoSet.size());
	}

	@Test
	void findActivitiesByPartialNameFoundMultiplePartialName() throws ResourceNotFoundException, ModelMappingException {
		Set<ActivityInfo> activityInfoSet = activityService.findActivitiesByPartialName("ball");
		assertEquals(2, activityInfoSet.size());
	}

	@Test
	void findActivitiesByPartialNameNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			activityService.findActivitiesByPartialName("Not Found");
		});
	}

	@Test
	void findActivityByIdFound() throws ResourceNotFoundException, ModelMappingException {
		ActivityDetailedInfo activityDetailedInfo = activityService.findActivityById(1L);
		assertNotNull(activityDetailedInfo);
	}

	@Test
	void findActivityByIdNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			activityService.findActivityById(1234567890L);
		});
	}

	@Test
	void addActivityGood() throws ModelMappingException, UnknownFileTypeException, ResourceNotFoundException,
			ResourceConflictException, IOException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.jpg")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");
		Long activityId = addActivity(activityPostRequest, icon);
		compareResultAndRevert(activityId, activityPostRequest, icon);
	}

	@Test
	void addActivityIconNull() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			activityService.addActivity(new ActivityPostRequest("NewSport", "A sport"), null);
		});
	}

	@Test
	void addActivityNameExist() {
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			activityService.addActivity(new ActivityPostRequest("Football", ""), null);
		});
	}

	@Test
	void updateActivityDetailsFoundNameNotExist() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityPostRequest.setName("Another Sport");
		ActivityDetailedInfo info = activityService.updateActivityDetails(activityId, activityPostRequest);
		assertNotNull(info);
		assertEquals(activityId, info.getId());
		assertEquals("Another Sport", info.getName());
		assertEquals("A sport", info.getDescription());
		assertNotNull(info.getIcon());

		compareResultAndRevert(activityId, activityPostRequest, icon);
	}

	@Test
	void updateActivityDetailsFoundNameExist() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityPostRequest.setName("Football");
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			activityService.updateActivityDetails(activityId, activityPostRequest);
		});

		// revert
		activityService.removeActivityById(activityId);
	}

	@Test
	void updateActivityDetailsFoundNameBlank() throws ResourceNotFoundException, ModelMappingException,
			UnknownFileTypeException, ResourceConflictException, IOException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityPostRequest.setName("    ");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			activityService.updateActivityDetails(activityId, activityPostRequest);
		});

		// revert
		activityService.removeActivityById(activityId);
	}

	@Test
	void updateActivityDetailsFoundNameNull() throws ResourceNotFoundException, ModelMappingException,
			UnknownFileTypeException, ResourceConflictException, IOException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityPostRequest.setName(null);
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			activityService.updateActivityDetails(activityId, activityPostRequest);
		});

		// revert
		activityService.removeActivityById(activityId);
	}

	@Test
	void updateActivityDetailsFoundDescriptionNotNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityPostRequest.setDescription("A new description");
		ActivityDetailedInfo info = activityService.updateActivityDetails(activityId, activityPostRequest);
		assertNotNull(info);
		assertEquals(activityId, info.getId());
		assertEquals("NewSport", info.getName());
		assertEquals("A new description", info.getDescription());
		assertNotNull(info.getIcon());

		compareResultAndRevert(activityId, activityPostRequest, icon);
	}

	@Test
	void updateActivityDetailsFoundDescriptionBlank() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityPostRequest.setDescription("   ");
		ActivityDetailedInfo info = activityService.updateActivityDetails(activityId, activityPostRequest);
		assertNotNull(info);
		assertEquals(activityId, info.getId());
		assertEquals("NewSport", info.getName());
		assertEquals("   ", info.getDescription());
		assertNotNull(info.getIcon());

		compareResultAndRevert(activityId, activityPostRequest, icon);
	}

	@Test
	void updateActivityDetailsFoundDescriptionNull() throws ModelMappingException, ResourceNotFoundException,
			ResourceConflictException, IOException, UnknownFileTypeException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityPostRequest.setDescription(null);
		ActivityDetailedInfo info = activityService.updateActivityDetails(activityId, activityPostRequest);
		assertNotNull(info);
		assertEquals(activityId, info.getId());
		assertEquals("NewSport", info.getName());
		assertNull(info.getDescription());
		assertNotNull(info.getIcon());

		compareResultAndRevert(activityId, activityPostRequest, icon);
	}

	@Test
	void updateActivityDetailsNotFound() {
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			activityService.updateActivityDetails(1234567890L, activityPostRequest);
		});
	}

	@Test
	void removeActivityByIdFound() throws ResourceNotFoundException, ModelMappingException, UnknownFileTypeException,
			ResourceConflictException, IOException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		ActivityPostRequest activityPostRequest = new ActivityPostRequest("NewSport", "A sport");

		Long activityId = addActivity(activityPostRequest, icon);

		activityService.removeActivityById(activityId);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			activityService.findActivityById(activityId);
		});
	}

	@Test
	void removeActivityByIdNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> activityService.removeActivityById(1234567890L));
	}

	@Test
	void getIconFound() throws ResourceNotFoundException, UnknownFileTypeException, IOException, ModelMappingException {
		updateIcon("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");
		Resource icon = activityService.getIcon(1L);
		assertNotNull(icon);
		assertNotNull(icon.getFilename());
		assertTrue(icon.getFilename().endsWith(".jpg"));
	}

	@Test
	void getIconNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			activityService.getIcon(1234567890L);
		});
	}

	@Test
	void updateIconPJPEG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdateIcon("name.pjpg", "original.pjpg", "image/pjpeg", "classpath:original.pjpg");
	}

	@Test
	void updateIconJPEG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdateIcon("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");
	}

	@Test
	void updateIconPNG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdateIcon("name.png", "original.png", "image/png", "classpath:original.png");
	}

	@Test
	void updateIconBMP()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdateIcon("name.bmp", "original.bmp", "image/bmp", "classpath:original.bmp");
	}

	@Test
	void updateIconXPNG()
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		reversibleUpdateIcon("name.png", "original.png", "image/x-png", "classpath:original.png");
	}

	@Test
	void updateIconNull() {
		Assertions.assertThrows(ConstraintViolationException.class, () -> activityService.updateActivityIcon(1L, null));
	}

	@Test
	void updateIconNotImage() throws IOException {
		MockMultipartFile icon = new MockMultipartFile("data.sql", "data.sql", null,
				new FileInputStream(ResourceUtils.getFile("classpath:data.sql")));
		assertNotNull(icon);
		Assertions.assertThrows(UnknownFileTypeException.class, () -> activityService.updateActivityIcon(3L, icon));
	}

	@Test
	void updateActivityIconNotFound() throws IOException {
		MockMultipartFile icon = new MockMultipartFile("name.jpg", "name.jpg", "image/jpeg",
				new FileInputStream(ResourceUtils.getFile("classpath:original.png")));
		Assertions.assertThrows(ResourceNotFoundException.class,
				() -> activityService.updateActivityIcon(1234567890L, icon));
	}

	@Test
	void findVenuesByActivityIdFoundEmpty() throws ResourceNotFoundException, ModelMappingException {
		Set<VenueDetailedInfo> venueDetailedInfoSet = activityService.findVenuesByActivityId(1L);
		assertNotNull(venueDetailedInfoSet);
		assertTrue(venueDetailedInfoSet.isEmpty());
	}

	@Test
	void findVenuesByActivityIdFoundSingle() throws ResourceNotFoundException, ModelMappingException {
		Set<VenueDetailedInfo> venueDetailedInfoSet = activityService.findVenuesByActivityId(2L);
		assertNotNull(venueDetailedInfoSet);
		assertEquals(1, venueDetailedInfoSet.size());
	}

	@Test
	void findVenuesByActivityIdFoundMultiple() throws ResourceNotFoundException, ModelMappingException {
		Set<VenueDetailedInfo> venueDetailedInfoSet = activityService.findVenuesByActivityId(3L);
		assertNotNull(venueDetailedInfoSet);
		assertEquals(2, venueDetailedInfoSet.size());
	}

	@Test
	void findVenuesByActivityIdNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			activityService.findVenuesByActivityId(1234567890L);
		});
	}

	private Long addActivity(ActivityPostRequest activityPostRequest, MockMultipartFile icon)
			throws ModelMappingException, UnknownFileTypeException, ResourceConflictException, IOException,
			ResourceNotFoundException {
		ActivityDetailedInfo info = activityService.addActivity(activityPostRequest, icon);
		assertNotNull(info);
		assertNotNull(info.getId());
		assertEquals("NewSport", info.getName());
		assertEquals("A sport", info.getDescription());
		assertNotNull(info.getIcon());

		Set<ActivityInfo> activityInfoSet = activityService.findActivitiesByPartialName("NewSport");
		assertEquals(1, activityInfoSet.size());

		return activityInfoSet.toArray(new ActivityInfo[0])[0].getId();
	}

	private void reversibleUpdateIcon(String name, String originalFilename, String contentType, String resourceLocation)
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, IOException {
		updateIcon(name, originalFilename, contentType, resourceLocation);
	}

	private void compareResult(Long activityId, ActivityPostRequest activityPostRequest, MockMultipartFile icon)
			throws ResourceNotFoundException, ModelMappingException {
		ActivityDetailedInfo activityDetailedInfo = activityService.findActivityById(activityId);
		assertEquals(activityPostRequest.getName(), activityDetailedInfo.getName());
		assertEquals(activityPostRequest.getDescription(), activityDetailedInfo.getDescription());
		if (icon != null) {
			assertNotNull(activityDetailedInfo.getIcon());
		} else {
			assertNull(activityDetailedInfo.getIcon());
		}
	}

	private void compareResultAndRevert(Long activityId, ActivityPostRequest activityPostRequest,
			MockMultipartFile icon) throws ResourceNotFoundException, ModelMappingException, IOException {
		compareResult(activityId, activityPostRequest, icon);
		// revert
		activityService.removeActivityById(activityId);
	}

	private void updateIcon(String name, String originalFilename, String contentType, String resourceLocation)
			throws UnknownFileTypeException, IOException, ResourceNotFoundException, ModelMappingException {
		MockMultipartFile icon = new MockMultipartFile(name, originalFilename, contentType,
				new FileInputStream(ResourceUtils.getFile(resourceLocation)));
		assertNotNull(icon);
		activityService.updateActivityIcon(1L, icon);
		ActivityDetailedInfo activityDetailedInfo = activityService.findActivityById(1L);
		assertNotNull(activityDetailedInfo);
		assertNotNull(activityDetailedInfo.getId());
		assertEquals("Football", activityDetailedInfo.getName());
		assertNull(activityDetailedInfo.getDescription());
		assertNotNull(activityDetailedInfo.getIcon());
		assertTrue(activityDetailedInfo.getIcon().endsWith(".jpg"));
	}

}
