package com.gymeet.be.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Set;

import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.ResourceUtils;

import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ProfileDetailedInfo;
import com.gymeet.be.payload.ProfileField;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.UserIdentityAvailability;
import com.gymeet.be.security.UserDetailsImpl;
import com.gymeet.be.security.UserDetailsServiceImpl;
import com.gymeet.be.util.ProfileFieldName;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class UserServiceTests {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserService userService;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Test
	void checkEmailAvailabilityExist() {
		UserIdentityAvailability userIdentityAvailability = userService.checkEmailAvailability("aeon.fruit@gymeet.com");
		assertFalse(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkEmailAvailabilityNotExist() {
		UserIdentityAvailability userIdentityAvailability = userService.checkEmailAvailability("aeon.fruit@lombak.com");
		assertTrue(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkPhoneNumberAvailabilityExist() {
		UserIdentityAvailability userIdentityAvailability = userService.checkPhoneNumberAvailability("+216 79 852 458");
		assertFalse(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkPhoneNumberAvailabilityNotExist() {
		UserIdentityAvailability userIdentityAvailability = userService.checkPhoneNumberAvailability("123456789");
		assertTrue(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkUsernameAvailabilityExist() {
		UserIdentityAvailability userIdentityAvailability = userService.checkUsernameAvailability("aeon.fruit");
		assertFalse(userIdentityAvailability.getAvailable());
	}

	@Test
	void checkUsernameAvailabilityNotExist() {
		UserIdentityAvailability userIdentityAvailability = userService.checkUsernameAvailability("lombak.com");
		assertTrue(userIdentityAvailability.getAvailable());
	}

	@Test
	void getCurrentUserMinimalProfile() throws ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		ProfileInfo profileInfo = userService.getCurrentUserMinimalProfile(userDetails);
		assertEquals("aeon.fruit", profileInfo.getUsername());
		assertEquals("Aeon", profileInfo.getFirstName());
		assertEquals("Fruit", profileInfo.getSurname());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getThumbnailPicture());
	}

	@Test
	void getUserMinimalProfileExist()
			throws BadRequestException, ResourceNotFoundException, ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		ProfileInfo profileInfo = userService.getUserMinimalProfile(userDetails, "aeon.fruit");
		assertEquals("aeon.fruit", profileInfo.getUsername());
		assertEquals("Aeon", profileInfo.getFirstName());
		assertEquals("Fruit", profileInfo.getSurname());
		assertNull(profileInfo.getProfilePicture());
		assertNull(profileInfo.getThumbnailPicture());
	}

	@Test
	void getUserMinimalProfileUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			userService.getUserMinimalProfile(userDetails, "user1");
		});
	}

	@Test
	void getUserMinimalProfileNotExist() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService
					.loadUserByUsername("aeon.fruit@gymeet.com");
			userService.getUserMinimalProfile(userDetails, "lombak.com");
		});
	}

	@Test
	void getUserDetailedProfileExist()
			throws BadRequestException, ResourceNotFoundException, ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "aeon.fruit");
		assertEquals("aeon.fruit", profileDetailedInfo.getUsername());
		assertEquals("Aeon", profileDetailedInfo.getFirstName());
		assertEquals("Fruit", profileDetailedInfo.getSurname());
		assertEquals("aeon.fruit@gymeet.com", profileDetailedInfo.getEmail());
		assertEquals("+216 79 852 458", profileDetailedInfo.getPhoneNumber());
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 9);
		birthDate.set(Calendar.MONTH, Calendar.OCTOBER);
		birthDate.set(Calendar.YEAR, 1995);
		birthDate.set(Calendar.HOUR_OF_DAY, 0);
		birthDate.set(Calendar.MINUTE, 0);
		birthDate.set(Calendar.SECOND, 0);
		birthDate.set(Calendar.MILLISECOND, 0);
		assertEquals(birthDate.getTime().getTime(), profileDetailedInfo.getBirthDate().getTime());
		assertEquals(false, profileDetailedInfo.getFemale());
		assertEquals("Tunisia", profileDetailedInfo.getHomeCountry());
		assertEquals("Tunisia", profileDetailedInfo.getCountry());
		assertEquals("Tunis", profileDetailedInfo.getCity());
		assertNull(profileDetailedInfo.getProfilePicture());
		assertNull(profileDetailedInfo.getThumbnailPicture());
	}

	@Test
	void getUserDetailedProfileUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			userService.getUserDetailedProfile(userDetails, "user1");
		});
	}

	@Test
	void getUserDetailedProfileNotExist() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService
					.loadUserByUsername("aeon.fruit@gymeet.com");
			userService.getUserDetailedProfile(userDetails, "lombak.com");
		});
	}

	@Test
	void getPictureUsernameExistPictureExistSize0() throws ResourceNotFoundException, UnknownFileTypeException,
			ModelMappingException, BadRequestException, IOException, ResourceBadRequestException {
		getPictureUsernameExistPictureExist(0);
	}

	@Test
	void getPictureUsernameExistPictureExistSize1() throws ResourceNotFoundException, UnknownFileTypeException,
			ModelMappingException, BadRequestException, IOException, ResourceBadRequestException {
		getPictureUsernameExistPictureExist(1);
	}

	@Test
	void getPictureUsernameExistPictureExistSize2() throws ResourceNotFoundException, UnknownFileTypeException,
			ModelMappingException, BadRequestException, IOException, ResourceBadRequestException {
		getPictureUsernameExistPictureExist(2);
	}

	@Test
	void getPictureUsernameExistPictureExistSize3() throws ResourceNotFoundException, UnknownFileTypeException,
			ModelMappingException, BadRequestException, IOException, ResourceBadRequestException {
		updatePicture("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService
					.loadUserByUsername("aeon.fruit@gymeet.com");
			userService.getPicture(userDetails, "ron.rook", 3);
		});

		deletePicture();
	}

	@Test
	void getPictureUsernameExistPictureNotExistSize0() throws ResourceNotFoundException, BadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Resource picture = userService.getPicture(userDetails, "ron.rook", 0);
		assertNull(picture);
	}

	@Test
	void getPictureUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			userService.getPicture(userDetails, "user1", 0);
		});
	}

	@Test
	void getPictureUsernameNotExist() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService
					.loadUserByUsername("aeon.fruit@gymeet.com");
			userService.getPicture(userDetails, "lombak.com", 0);
		});
	}

	@Test
	void updateEmailAvailable()
			throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("ron.rook@kmail.com"),
				ProfileFieldName.EMAIL.name());
		userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@kmail.com");
		assertEquals("ron.rook@kmail.com", profileField.getValue());
		assertEquals("ron.rook@kmail.com", userDetails.getLogin());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("ron.rook@gymeet.com"),
				ProfileFieldName.EMAIL.name());
		assertEquals("ron.rook@gymeet.com", profileField.getValue());
	}

	@Test
	void updateEmailUnavailable() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("aeon.fruit@gymeet.com"),
					ProfileFieldName.EMAIL.name());
		});
	}

	@Test
	void updateEmailNull() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(null), ProfileFieldName.EMAIL.name());
		});
	}

	@Test
	void updateEmailBlank() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("   "), ProfileFieldName.EMAIL.name());
		});
	}

	@Test
	void updatePasswordGood() throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("lepass"),
				ProfileFieldName.PASSWORD.name());
		userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		assertNull(profileField.getValue());
		assertTrue(passwordEncoder.matches("lepass", userDetails.getPassword()));
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("thepass"),
				ProfileFieldName.PASSWORD.name());
		assertNull(profileField.getValue());
	}

	@Test
	void updatePasswordNull() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceBadRequestException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(null), ProfileFieldName.PASSWORD.name());
		});
	}

	@Test
	void updatePasswordBlank() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceBadRequestException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("   "), ProfileFieldName.PASSWORD.name());
		});
	}

	@Test
	void updateFirstNameGood()
			throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("Rony"),
				ProfileFieldName.FIRST_NAME.name());
		userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		assertEquals("Rony", profileField.getValue());
		assertEquals("Rony", userDetails.getFirstName());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Ron"),
				ProfileFieldName.FIRST_NAME.name());
		assertEquals("Ron", profileField.getValue());
	}

	@Test
	void updateFirstNameNull() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(null), ProfileFieldName.FIRST_NAME.name());
		});
	}

	@Test
	void updateFirstNameBlank() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("   "), ProfileFieldName.FIRST_NAME.name());
		});
	}

	@Test
	void updateSurnameGood() throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("Rogue"),
				ProfileFieldName.SURNAME.name());
		userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		assertEquals("Rogue", profileField.getValue());
		assertEquals("Rogue", userDetails.getSurname());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Rook"),
				ProfileFieldName.SURNAME.name());
		assertEquals("Rook", profileField.getValue());
	}

	@Test
	void updateSurnameNull() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(null), ProfileFieldName.SURNAME.name());
		});
	}

	@Test
	void updateSurnameBlank() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("   "), ProfileFieldName.SURNAME.name());
		});
	}

	@Test
	void updatePhoneNumberAvailable() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("123456789"),
				ProfileFieldName.PHONE_NUMBER.name());
		userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("123456789");
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertEquals("123456789", profileField.getValue());
		assertEquals("123456789", profileDetailedInfo.getPhoneNumber());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("+33 79 78 58 31 28"),
				ProfileFieldName.PHONE_NUMBER.name());
		assertEquals("+33 79 78 58 31 28", profileField.getValue());
	}

	@Test
	void updatePhoneNumberUnavailable() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("+216 79 785 328"),
					ProfileFieldName.PHONE_NUMBER.name());
		});
	}

	@Test
	void updatePhoneNumberNull() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField(null),
				ProfileFieldName.PHONE_NUMBER.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getPhoneNumber());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("+33 79 78 58 31 28"),
				ProfileFieldName.PHONE_NUMBER.name());
		assertEquals("+33 79 78 58 31 28", profileField.getValue());
	}

	@Test
	void updatePhoneNumberBlank() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("    "),
				ProfileFieldName.PHONE_NUMBER.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getPhoneNumber());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("+33 79 78 58 31 28"),
				ProfileFieldName.PHONE_NUMBER.name());
		assertEquals("+33 79 78 58 31 28", profileField.getValue());
	}

	@Test
	void updateUsernameAvailable()
			throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("rony.rookie"),
				ProfileFieldName.USERNAME.name());
		userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		assertEquals("rony.rookie", profileField.getValue());
		assertEquals("rony.rookie", userDetails.getUsername());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("ron.rook"),
				ProfileFieldName.USERNAME.name());
		assertEquals("ron.rook", profileField.getValue());
	}

	@Test
	void updateUsernameUnavailable() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("aeon.fruit"),
					ProfileFieldName.USERNAME.name());
		});
	}

	@Test
	void updateUsernameNull() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(null), ProfileFieldName.USERNAME.name());
		});
	}

	@Test
	void updateUsernameBlank() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("   "), ProfileFieldName.USERNAME.name());
		});
	}

	@Test
	void updateHomeCountryGood() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("United Kingdom"),
				ProfileFieldName.HOME_COUNTRY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertEquals("United Kingdom", profileField.getValue());
		assertEquals("United Kingdom", profileDetailedInfo.getHomeCountry());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Tunisia"),
				ProfileFieldName.HOME_COUNTRY.name());
		assertEquals("Tunisia", profileField.getValue());
	}

	@Test
	void updateHomeCountryNull() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField(null),
				ProfileFieldName.HOME_COUNTRY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getHomeCountry());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Tunisia"),
				ProfileFieldName.HOME_COUNTRY.name());
		assertEquals("Tunisia", profileField.getValue());
	}

	@Test
	void updateHomeCountryBlank() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("   "),
				ProfileFieldName.HOME_COUNTRY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getHomeCountry());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Tunisia"),
				ProfileFieldName.HOME_COUNTRY.name());
		assertEquals("Tunisia", profileField.getValue());
	}

	@Test
	void updateCountryGood() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("United Kingdom"),
				ProfileFieldName.COUNTRY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertEquals("United Kingdom", profileField.getValue());
		assertEquals("United Kingdom", profileDetailedInfo.getCountry());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("France"),
				ProfileFieldName.COUNTRY.name());
		assertEquals("France", profileField.getValue());
	}

	@Test
	void updateCountryNull() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField(null),
				ProfileFieldName.COUNTRY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getCountry());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("France"),
				ProfileFieldName.COUNTRY.name());
		assertEquals("France", profileField.getValue());
	}

	@Test
	void updateCountryBlank() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("   "),
				ProfileFieldName.COUNTRY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getCountry());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("France"),
				ProfileFieldName.COUNTRY.name());
		assertEquals("France", profileField.getValue());
	}

	@Test
	void updateCityGood() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("London"),
				ProfileFieldName.CITY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertEquals("London", profileField.getValue());
		assertEquals("London", profileDetailedInfo.getCity());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Toulouse"),
				ProfileFieldName.CITY.name());
		assertEquals("Toulouse", profileField.getValue());
	}

	@Test
	void updateCityNull() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField(null),
				ProfileFieldName.CITY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getCity());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Toulouse"),
				ProfileFieldName.CITY.name());
		assertEquals("Toulouse", profileField.getValue());
	}

	@Test
	void updateCityBlank() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField("   "),
				ProfileFieldName.CITY.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileField.getValue());
		assertNull(profileDetailedInfo.getCity());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField("Toulouse"),
				ProfileFieldName.CITY.name());
		assertEquals("Toulouse", profileField.getValue());
	}

	@Test
	void updateBirthDateGood() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 29);
		birthDate.set(Calendar.MONTH, Calendar.FEBRUARY);
		birthDate.set(Calendar.YEAR, 2004);
		birthDate.set(Calendar.HOUR_OF_DAY, 0);
		birthDate.set(Calendar.MINUTE, 0);
		birthDate.set(Calendar.SECOND, 0);
		birthDate.set(Calendar.MILLISECOND, 0);

		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField(birthDate.getTime()),
				ProfileFieldName.BIRTH_DATE.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertEquals(birthDate.getTime(), profileField.getValue());
		assertEquals(birthDate.getTime().getTime(), profileDetailedInfo.getBirthDate().getTime());
		// revert
		birthDate.set(Calendar.DAY_OF_MONTH, 9);
		birthDate.set(Calendar.MONTH, Calendar.OCTOBER);
		birthDate.set(Calendar.YEAR, 1995);
		profileField = userService.updateProfileField(userDetails, new ProfileField(birthDate.getTime()),
				ProfileFieldName.BIRTH_DATE.name());
		assertEquals(birthDate.getTime(), profileField.getValue());
	}

	@Test
	void updateBirthDateNull() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(null), ProfileFieldName.BIRTH_DATE.name());
		});
	}

	@Test
	void updateBirthDateFuture() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Calendar birthDate = Calendar.getInstance();
		birthDate.set(Calendar.DAY_OF_MONTH, 29);
		birthDate.set(Calendar.MONTH, Calendar.FEBRUARY);
		birthDate.set(Calendar.YEAR, 2024);
		birthDate.set(Calendar.HOUR_OF_DAY, 0);
		birthDate.set(Calendar.MINUTE, 0);
		birthDate.set(Calendar.SECOND, 0);
		birthDate.set(Calendar.MILLISECOND, 0);
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(birthDate.getTime()),
					ProfileFieldName.BIRTH_DATE.name());
		});
	}

	@Test
	void updateFemaleGood() throws ResourceNotFoundException, ResourceConflictException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		ProfileField profileField = userService.updateProfileField(userDetails, new ProfileField(true),
				ProfileFieldName.GENDER.name());
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertEquals(true, profileField.getValue());
		assertEquals(true, profileDetailedInfo.getFemale());
		// revert
		profileField = userService.updateProfileField(userDetails, new ProfileField(false),
				ProfileFieldName.GENDER.name());
		assertEquals(false, profileField.getValue());
	}

	@Test
	void updateFemaleNull() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ConstraintViolationException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField(null), ProfileFieldName.GENDER.name());
		});
	}

	@Test
	void updateFemaleNullProfileField() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceBadRequestException.class, () -> {
			userService.updateProfileField(userDetails, null, ProfileFieldName.GENDER.name());
		});
	}

	@Test
	void updateUnknownFieldName() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("random value"), "random field name");
		});
	}

	@Test
	void updateNullFieldName() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			userService.updateProfileField(userDetails, new ProfileField("random value"), null);
		});
	}

	@Test
	void updatePicturePJPEG() throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			BadRequestException, IOException, ResourceBadRequestException {
		reversibleUpdatePicture("name.pjpg", "original.pjpg", "image/pjpeg", "classpath:original.pjpg");
	}

	@Test
	void updatePictureJPEG() throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			BadRequestException, IOException, ResourceBadRequestException {
		reversibleUpdatePicture("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");
	}

	@Test
	void updatePicturePNG() throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			BadRequestException, IOException, ResourceBadRequestException {
		reversibleUpdatePicture("name.png", "original.png", "image/png", "classpath:original.png");
	}

	@Test
	void updatePictureBMP() throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			BadRequestException, IOException, ResourceBadRequestException {
		reversibleUpdatePicture("name.bmp", "original.bmp", "image/bmp", "classpath:original.bmp");
	}

	@Test
	void updatePictureXPNG() throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			BadRequestException, IOException, ResourceBadRequestException {
		reversibleUpdatePicture("name.png", "original.png", "image/x-png", "classpath:original.png");
	}

	@Test
	void updatePictureNull() throws UnknownFileTypeException, IOException, ResourceNotFoundException,
			BadRequestException, ModelMappingException, ResourceBadRequestException {
		deletePicture();
	}

	@Test
	void updatePictureNotImage() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		Assertions.assertThrows(UnknownFileTypeException.class, () -> {
			MockMultipartFile picture = new MockMultipartFile("data.sql", "data.sql", null,
					new FileInputStream(ResourceUtils.getFile("classpath:data.sql")));
			assertNotNull(picture);
			userService.updatePicture(userDetails, picture);
		});
	}

	@Test
	void getUserActivitiesSelf() throws BadRequestException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ActivityInfo> activityInfoSet = userService.getUserActivities(userDetails, "aeon.fruit");
		assertEquals(2, activityInfoSet.size());
	}

	@Test
	void getUserActivitiesUsernameFound() throws BadRequestException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ActivityInfo> activityInfoSet = userService.getUserActivities(userDetails, "meob.ruin");
		assertEquals(2, activityInfoSet.size());
	}

	@Test
	void getUserActivitiesUsernameBlocking() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService
					.loadUserByUsername("aeon.fruit@gymeet.com");
			userService.getUserActivities(userDetails, "lombak.com");
		});
	}

	@Test
	void getUserActivitiesUsernameNotFound() {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService
					.loadUserByUsername("aeon.fruit@gymeet.com");
			userService.getUserActivities(userDetails, "lombak.com");
		});
	}

	@Test
	void addRemoveActivityFound()
			throws ModelMappingException, ResourceNotFoundException, ResourceConflictException, BadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		ActivityInfo activityInfo = userService.addActivity(userDetails, 2L);
		assertNotNull(activityInfo);
		assertEquals(2L, activityInfo.getId().longValue());
		assertEquals("Handball", activityInfo.getName());

		Set<ActivityInfo> activityInfoSet = userService.getUserActivities(userDetails, "aeon.fruit");
		assertEquals(3, activityInfoSet.size());

		userService.removeActivity(userDetails, 2L);

		activityInfoSet = userService.getUserActivities(userDetails, "aeon.fruit");
		assertEquals(2, activityInfoSet.size());
	}

	@Test
	void addActivityNotFound() throws ModelMappingException, ResourceNotFoundException, BadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			userService.addActivity(userDetails, 1234567890L);
		});

		Set<ActivityInfo> activityInfoSet = userService.getUserActivities(userDetails, "aeon.fruit");
		assertEquals(2, activityInfoSet.size());
	}

	@Test
	void addActivityExistInList() throws ModelMappingException, ResourceNotFoundException, BadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceConflictException.class, () -> {
			userService.addActivity(userDetails, 1L);
		});

		Set<ActivityInfo> activityInfoSet = userService.getUserActivities(userDetails, "aeon.fruit");
		assertEquals(2, activityInfoSet.size());
	}

	@Test
	void removeActivityNotFound() throws ResourceNotFoundException, BadRequestException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class,
				() -> userService.removeActivity(userDetails, 1234567890L));

		Set<ActivityInfo> activityInfoSet = userService.getUserActivities(userDetails, "aeon.fruit");
		assertEquals(2, activityInfoSet.size());
	}

	@Test
	void removeActivityNotInList() throws ResourceNotFoundException, BadRequestException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> userService.removeActivity(userDetails, 2L));

		Set<ActivityInfo> activityInfoSet = userService.getUserActivities(userDetails, "aeon.fruit");
		assertEquals(2, activityInfoSet.size());
	}

	// TODO - Test UserService.findByName

	private void getPictureUsernameExistPictureExist(int size)
			throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException, BadRequestException,
			IOException, ResourceBadRequestException {
		updatePicture("name.jpg", "original.jpg", "image/jpeg", "classpath:original.jpg");

		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Resource picture = userService.getPicture(userDetails, "ron.rook", size);
		assertNotNull(picture);
		assertNotNull(picture.getFilename());
		assertTrue(picture.getFilename().endsWith(".jpg"));

		deletePicture();
	}

	private void reversibleUpdatePicture(String name, String originalFilename, String contentType,
			String resourceLocation) throws ResourceNotFoundException, UnknownFileTypeException, ModelMappingException,
			BadRequestException, IOException, ResourceBadRequestException {
		updatePicture(name, originalFilename, contentType, resourceLocation);
		deletePicture();
	}

	private void updatePicture(String name, String originalFilename, String contentType, String resourceLocation)
			throws UnknownFileTypeException, IOException, ResourceNotFoundException, BadRequestException,
			ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		MockMultipartFile picture = null;
		try {
			picture = new MockMultipartFile(name, originalFilename, contentType,
					new FileInputStream(ResourceUtils.getFile(resourceLocation)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertNotNull(picture);
		userService.updatePicture(userDetails, picture);
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNotNull(profileDetailedInfo.getProfilePicture());
		assertNotNull(profileDetailedInfo.getThumbnailPicture());
		assertTrue(profileDetailedInfo.getProfilePicture().endsWith(".jpg"));
		assertTrue(profileDetailedInfo.getThumbnailPicture().endsWith(".jpg"));
	}

	private void deletePicture() throws UnknownFileTypeException, IOException, ResourceNotFoundException,
			BadRequestException, ModelMappingException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("ron.rook@gymeet.com");
		userService.updatePicture(userDetails, null);
		ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(userDetails, "ron.rook");
		assertNull(profileDetailedInfo.getProfilePicture());
		assertNull(profileDetailedInfo.getThumbnailPicture());
	}

}
