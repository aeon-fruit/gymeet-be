package com.gymeet.be.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.ResourceUnauthorizedException;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.ProfileRelationInfo;
import com.gymeet.be.payload.RelationProperty;
import com.gymeet.be.security.UserDetailsImpl;
import com.gymeet.be.security.UserDetailsServiceImpl;
import com.gymeet.be.util.RelationType;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class RelationServiceTests {

	@Autowired
	private RelationService relationService;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Test
	void getCurrentUserMateListNotEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		Set<ProfileInfo> list = getCurrentUserMateList(userDetails);
		assertEquals(2, list.size());
	}

	@Test
	void getCurrentUserMateListEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Set<ProfileInfo> list = getCurrentUserMateList(userDetails);
		assertTrue(list.isEmpty());
	}

	@Test
	void getMateListUsernameExistNotEmpty() throws BadRequestException, ModelMappingException,
			ResourceNotFoundException, ResourceUnauthorizedException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ProfileInfo> list = getMateList(userDetails, "user1");
		assertEquals(2, list.size());
	}

	@Test
	void getMateListUsernameExistEmpty() throws BadRequestException, ModelMappingException, ResourceNotFoundException,
			ResourceUnauthorizedException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ProfileInfo> list = getMateList(userDetails, "user5");
		assertTrue(list.isEmpty());
	}

	@Test
	void getMateListUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			getMateList(userDetails, "lombak.com");
		});
	}

	@Test
	void getMateListUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			getMateList(userDetails, "aeon.fruit");
		});
	}

	@Test
	void getMateListUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			getMateList(userDetails, "user1");
		});
	}

	@Test
	void getCurrentUserFollowerListNotEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		Set<ProfileInfo> list = getCurrentUserFollowerList(userDetails);
		assertEquals(1, list.size());
	}

	@Test
	void getCurrentUserFollowerListEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user4@gymeet.com");
		Set<ProfileInfo> list = getCurrentUserFollowerList(userDetails);
		assertTrue(list.isEmpty());
	}

	@Test
	void getFollowerListUsernameExistNotEmpty() throws BadRequestException, ModelMappingException,
			ResourceNotFoundException, ResourceUnauthorizedException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ProfileInfo> list = getFollowerList(userDetails, "user2");
		assertEquals(1, list.size());
	}

	@Test
	void getFollowerListUsernameExistEmpty() throws BadRequestException, ModelMappingException,
			ResourceNotFoundException, ResourceUnauthorizedException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ProfileInfo> list = getFollowerList(userDetails, "user4");
		assertTrue(list.isEmpty());
	}

	@Test
	void getFollowerListUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			getFollowerList(userDetails, "lombak.com");
		});
	}

	@Test
	void getFollowerListUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			getFollowerList(userDetails, "aeon.fruit");
		});
	}

	@Test
	void getFollowerListUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			getFollowerList(userDetails, "user1");
		});
	}

	@Test
	void getCurrentUserFollowedListEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		Set<ProfileInfo> list = getCurrentUserFollowedList(userDetails);
		assertEquals(2, list.size());
	}

	@Test
	void getCurrentUserFollowedListNotEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		Set<ProfileInfo> list = getCurrentUserFollowedList(userDetails);
		assertTrue(list.isEmpty());
	}

	@Test
	void getFollowedListUsernameExistNotEmpty() throws BadRequestException, ModelMappingException,
			ResourceNotFoundException, ResourceUnauthorizedException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ProfileInfo> list = getFollowedList(userDetails, "user1");
		assertEquals(2, list.size());
	}

	@Test
	void getFollowedListUsernameExistEmpty() throws BadRequestException, ModelMappingException,
			ResourceNotFoundException, ResourceUnauthorizedException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Set<ProfileInfo> list = getFollowedList(userDetails, "user2");
		assertTrue(list.isEmpty());
	}

	@Test
	void getFollowedListUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			getFollowedList(userDetails, "lombak.com");
		});
	}

	@Test
	void getFollowedListUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			getFollowedList(userDetails, "aeon.fruit");
		});
	}

	@Test
	void getFollowedListUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			getFollowedList(userDetails, "user1");
		});
	}

	@Test
	void getRequestPendingListNotEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		Set<ProfileInfo> list = getRequestPendingList(userDetails);
		assertEquals(2, list.size());
	}

	@Test
	void getRequestPendingListEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		Set<ProfileInfo> list = getRequestPendingList(userDetails);
		assertTrue(list.isEmpty());
	}

	@Test
	void getResponsePendingListNotEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user4@gymeet.com");
		Set<ProfileInfo> list = getResponsePendingList(userDetails);
		assertEquals(1, list.size());
	}

	@Test
	void getResponsePendingListEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		Set<ProfileInfo> list = getResponsePendingList(userDetails);
		assertTrue(list.isEmpty());
	}

	@Test
	void getBlockedListNotEmpty()
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		Set<ProfileInfo> list = getBlockedList(userDetails);
		assertEquals(2, list.size());
	}

	@Test
	void getBlockedListEmpty() throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		Set<ProfileInfo> list = getBlockedList(userDetails);
		assertTrue(list.isEmpty());
	}

	@Test
	void getNullList() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.getCurrentUserRelationList(userDetails, null);
		});
	}

	@Test
	void getWrongList() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.getCurrentUserRelationList(userDetails, "random list");
		});
	}

	@Test
	void isMateUsernameExistExist() throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		ProfileRelationInfo info = relationServiceIsMate(userDetails, "user1");
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isMateUsernameExistNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		ProfileRelationInfo info = relationServiceIsMate(userDetails, "user5");
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isMateUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsMate(userDetails, "lombak.com");
		});
	}

	@Test
	void isMateUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceIsMate(userDetails, "aeon.fruit");
		});
	}

	@Test
	void isMateUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsMate(userDetails, "user1");
		});
	}

	@Test
	void isFollowerUsernameExistReciprocatedExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollower(userDetails, "user3");
		assertTrue(info.getFollowed());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowerUsernameExistNotReciprocatedExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollower(userDetails, "user2");
		assertTrue(info.getFollowed());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowerUsernameExistNotReciprocatedNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollower(userDetails, "user1");
		assertFalse(info.getFollowed());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowerUsernameExistNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user3@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollower(userDetails, "user4");
		assertFalse(info.getFollowed());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowerUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsFollower(userDetails, "lombak.com");
		});
	}

	@Test
	void isFollowerUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceIsFollower(userDetails, "aeon.fruit");
		});
	}

	@Test
	void isFollowerUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsFollower(userDetails, "user1");
		});
	}

	@Test
	void isFollowedUsernameExistReciprocatedExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user3@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollowed(userDetails, "user1");
		assertTrue(info.getFollower());
		assertFalse(info.getMate());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowedUsernameExistNotReciprocatedExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user2@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollowed(userDetails, "user1");
		assertTrue(info.getFollower());
		assertFalse(info.getMate());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowedUsernameExistNotReciprocatedNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollowed(userDetails, "user2");
		assertFalse(info.getFollower());
		assertFalse(info.getMate());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowedUsernameExistNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user3@gymeet.com");
		ProfileRelationInfo info = relationServiceIsFollowed(userDetails, "user4");
		assertFalse(info.getFollower());
		assertFalse(info.getMate());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void isFollowedUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsFollowed(userDetails, "lombak.com");
		});
	}

	@Test
	void isFollowedUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceIsFollowed(userDetails, "aeon.fruit");
		});
	}

	@Test
	void isFollowedUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsFollowed(userDetails, "user1");
		});
	}

	@Test
	void hasRequestPendingUsernameExistExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		ProfileRelationInfo info = relationServiceHasRequestPending(userDetails, "user4");
		assertTrue(info.getResponder());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getBlocked());
	}

	@Test
	void hasRequestPendingUsernameExistNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user3@gymeet.com");
		ProfileRelationInfo info = relationServiceHasRequestPending(userDetails, "user4");
		assertFalse(info.getResponder());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getBlocked());
	}

	@Test
	void hasRequestPendingUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceHasRequestPending(userDetails, "lombak.com");
		});
	}

	@Test
	void hasRequestPendingUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceHasRequestPending(userDetails, "aeon.fruit");
		});
	}

	@Test
	void hasRequestPendingUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceHasRequestPending(userDetails, "user1");
		});
	}

	@Test
	void hasResponsePendingUsernameExistExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		ProfileRelationInfo info = relationServiceHasResponsePending(userDetails, "user4");
		assertTrue(info.getRequester());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void hasResponsePendingUsernameExistNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user4@gymeet.com");
		ProfileRelationInfo info = relationServiceHasResponsePending(userDetails, "user3");
		assertFalse(info.getRequester());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	@Test
	void hasResponsePendingUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceHasResponsePending(userDetails, "lombak.com");
		});
	}

	@Test
	void hasResponsePendingUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceHasResponsePending(userDetails, "aeon.fruit");
		});
	}

	@Test
	void hasResponsePendingUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceHasResponsePending(userDetails, "user1");
		});
	}

	@Test
	void isBlockingUsernameExistExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user1@gymeet.com");
		ProfileRelationInfo info = relationServiceIsBlocking(userDetails, "user5");
		assertTrue(info.getBlocked());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
	}

	@Test
	void isBlockingUsernameExistNotExist()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user3@gymeet.com");
		ProfileRelationInfo info = relationServiceIsBlocking(userDetails, "user4");
		assertFalse(info.getBlocked());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
	}

	@Test
	void isBlockingUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsBlocking(userDetails, "lombak.com");
		});
	}

	@Test
	void isBlockingUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceIsBlocking(userDetails, "aeon.fruit");
		});
	}

	@Test
	void isBlockingUsernameBlocking() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceIsBlocking(userDetails, "user1");
		});
	}

	@Test
	void isEmptyRelationList() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceBadRequestException.class, () -> {
			relationService.getRelationInfo(userDetails, "user2");
		});
	}

	@Test
	void isNullRelationList() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceBadRequestException.class, () -> {
			relationService.getRelationInfo(userDetails, "user2", (RelationType[]) null);
		});
	}

	@Test
	void isNullContentRelationList() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.getRelationInfo(userDetails, "user2", null, null);
		});
	}

	@Test
	void isBlockedUsernameExistExist() throws BadRequestException, ResourceNotFoundException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		RelationProperty exist = relationService.isBlocked(userDetails, "user1");
		assertTrue(exist.getHaveRelation());
	}

	@Test
	void isBlockedUsernameExistNotExist() throws BadRequestException, ResourceNotFoundException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user3@gymeet.com");
		RelationProperty exist = relationService.isBlocked(userDetails, "user4");
		assertFalse(exist.getHaveRelation());
	}

	@Test
	void isBlockedUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.isBlocked(userDetails, "lombak.com");
		});
	}

	@Test
	void isBlockedUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationService.isBlocked(userDetails, "aeon.fruit");
		});
	}

	@Test
	void isBlockedUsernameBlocking() throws BadRequestException, ResourceNotFoundException {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("user5@gymeet.com");
		RelationProperty exist = relationService.isBlocked(userDetails, "user1");
		assertTrue(exist.getHaveRelation());
	}

	@Test
	void sendRequestUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceSendRequest(userDetails, "aeon.fruit");
		});
	}

	@Test
	void sendRequestUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceSendRequest(userDetails, "lombok.com");
		});
	}

	@Test
	void sendRequestUsernameBlocked() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceConflictException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		ProfileRelationInfo info = relationServiceSendRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameBlocking()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void sendRequestUsernameMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameNotMateFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceSendRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameNotMateFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceSendRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameNotMateNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceSendRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameNotMateNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceSendRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameRequesterFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void sendRequestUsernameRequesterFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void sendRequestUsernameRequesterNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void sendRequestUsernameRequesterNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void sendRequestUsernameResponderFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameResponderFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameResponderNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void sendRequestUsernameResponderNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "send.request";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceSendRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceAbortRequest(userDetails, "aeon.fruit");
		});
	}

	@Test
	void abortRequestUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(userDetails, "lombok.com");
		});
	}

	@Test
	void abortRequestUsernameBlocked()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void abortRequestUsernameBlocking()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void abortRequestUsernameMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameNotMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameNotMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameNotMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameNotMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameRequesterFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void abortRequestUsernameRequesterFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void abortRequestUsernameRequesterNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void abortRequestUsernameRequesterNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAbortRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void abortRequestUsernameResponderFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceAbortRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameResponderFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceAbortRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameResponderNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceAbortRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void abortRequestUsernameResponderNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "abort.request";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceAbortRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceAcceptRequest(userDetails, "aeon.fruit");
		});
	}

	@Test
	void acceptRequestUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(userDetails, "lombok.com");
		});
	}

	@Test
	void acceptRequestUsernameBlocked()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void acceptRequestUsernameBlocking()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void acceptRequestUsernameMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameNotMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameNotMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameNotMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameNotMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameRequesterFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceAcceptRequest(first, secondUsername);
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameRequesterFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceAcceptRequest(first, secondUsername);
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameRequesterNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceAcceptRequest(first, secondUsername);
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameRequesterNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceAcceptRequest(first, secondUsername);
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameResponderFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameResponderFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameResponderNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void acceptRequestUsernameResponderNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "accept.request";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceAcceptRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceAcceptRequest(userDetails, "aeon.fruit");
		});
	}

	@Test
	void declineRequestUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(userDetails, "lombok.com");
		});
	}

	@Test
	void declineRequestUsernameBlocked()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void declineRequestUsernameBlocking()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void declineRequestUsernameMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameNotMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameNotMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameNotMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameNotMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameRequesterFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceDeclineRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameRequesterFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceDeclineRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameRequesterNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceDeclineRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameRequesterNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceDeclineRequest(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameResponderFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameResponderFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameResponderNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void declineRequestUsernameResponderNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "decline.request";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeclineRequest(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceFollow(userDetails, "aeon.fruit");
		});
	}

	@Test
	void followUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceFollow(userDetails, "lombok.com");
		});
	}

	@Test
	void followUsernameBlocked() throws BadRequestException, ResourceNotFoundException, ResourceConflictException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameBlocking() throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void followUsernameMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameMateFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameMateNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameNotMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameNotMateFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameNotMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameNotMateNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameRequesterFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void followUsernameRequesterFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void followUsernameRequesterNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void followUsernameRequesterNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void followUsernameResponderFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameResponderFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameResponderNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceFollow(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void followUsernameResponderNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "follow";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceUnFollow(userDetails, "aeon.fruit");
		});
	}

	@Test
	void unFollowUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(userDetails, "lombok.com");
		});
	}

	@Test
	void unFollowUsernameBlocked() throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void unFollowUsernameBlocking() throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void unFollowUsernameMateFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameMateNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameNotMateFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameNotMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameNotMateNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameNotMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameRequesterFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unFollowUsernameRequesterFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unFollowUsernameRequesterNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unFollowUsernameRequesterNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unFollowUsernameResponderFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameResponderFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameResponderNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceUnFollow(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unFollowUsernameResponderNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-follow";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnFollow(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void blockUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceBlock(userDetails, "aeon.fruit");
		});
	}

	@Test
	void blockUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceBlock(userDetails, "lombok.com");
		});
	}

	@Test
	void blockUsernameBlocked() throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		Assertions.assertThrows(ResourceConflictException.class, () -> {
			relationServiceBlock(first, secondUsername);
		});

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameBlocking() throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceBlock(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void blockUsernameMateFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameMateFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameMateNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameMateNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameNotMateFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameNotMateFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameNotMateNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameNotMateNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameRequesterFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameRequesterFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameRequesterNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameRequesterNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameResponderFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameResponderFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameResponderNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void blockUsernameResponderNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceConflictException, ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "block";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void unBlockUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceUnBlock(userDetails, "aeon.fruit");
		});
	}

	@Test
	void unBlockUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(userDetails, "lombok.com");
		});
	}

	@Test
	void unBlockUsernameBlocked() throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		ProfileRelationInfo info = relationServiceUnBlock(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameBlocking() throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void unBlockUsernameMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameNotMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameNotMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameNotMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameNotMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameRequesterFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unBlockUsernameRequesterFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unBlockUsernameRequesterNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unBlockUsernameRequesterNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void unBlockUsernameResponderFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameResponderFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameResponderNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void unBlockUsernameResponderNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "un-block";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceUnBlock(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameSelf() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(BadRequestException.class, () -> {
			relationServiceDeleteMate(userDetails, "aeon.fruit");
		});
	}

	@Test
	void deleteMateUsernameNotExist() {
		UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername("aeon.fruit@gymeet.com");
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(userDetails, "lombok.com");
		});
	}

	@Test
	void deleteMateUsernameBlocked()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(first, secondUsername, second, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		isBlocking(first, secondUsername, second, firstUsername);
	}

	@Test
	void deleteMateUsernameBlocking()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "blocking";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isBlocking(second, firstUsername, first, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		isBlocking(second, firstUsername, first, secondUsername);
	}

	@Test
	void deleteMateUsernameMateFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceDeleteMate(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameMateFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceDeleteMate(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameMateNotFollowerFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceDeleteMate(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameMateNotFollowerNotFollowed() throws BadRequestException, ResourceNotFoundException,
			ResourceUnauthorizedException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		ProfileRelationInfo info = relationServiceDeleteMate(first, secondUsername);
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameNotMateFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "not.mate.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		isNotMateFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameNotMateFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "not.mate.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		isNotMateNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameNotMateNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "not.mate.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		isNotMateFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameNotMateNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "not.mate.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		isNotMateNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameRequesterFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "requester.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void deleteMateUsernameRequesterFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "requester.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void deleteMateUsernameRequesterNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "requester.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void deleteMateUsernameRequesterNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "requester.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(second, secondUsername, first, firstUsername);
	}

	@Test
	void deleteMateUsernameResponderFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "responder.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameResponderFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "responder.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingNotFollowerFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameResponderNotFollowerFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "responder.not.follower.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void deleteMateUsernameResponderNotFollowerNotFollowed()
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		String firstUsername = "delete.mate";
		String secondUsername = "responder.not.follower.not.followed";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");
		UserDetailsImpl second = (UserDetailsImpl) userDetailsService
				.loadUserByUsername(secondUsername + "@gymeet.com");

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationServiceDeleteMate(first, secondUsername);
		});

		hasRequestPendingNotFollowerNotFollowed(first, firstUsername, second, secondUsername);
	}

	@Test
	void addRelationNullType() {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.addRelation(first, secondUsername, null);
		});
	}

	@Test
	void addRelationWrongType() {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.addRelation(first, secondUsername, "random type");
		});
	}

	@Test
	void addRelationMateType() {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");

		Assertions.assertThrows(ResourceUnauthorizedException.class, () -> {
			relationService.addRelation(first, secondUsername, RelationType.MATE.name());
		});
	}

	@Test
	void addRelationFollowedType() {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");

		Assertions.assertThrows(ResourceUnauthorizedException.class, () -> {
			relationService.addRelation(first, secondUsername, RelationType.FOLLOWED.name());
		});
	}

	@Test
	void removeRelationNullType() {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.removeRelation(first, secondUsername, null);
		});
	}

	@Test
	void removeRelationWrongType() {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.removeRelation(first, secondUsername, "random type");
		});
	}

	@Test
	void removeRelationFollowedType() {
		String firstUsername = "block";
		String secondUsername = "blocked";
		UserDetailsImpl first = (UserDetailsImpl) userDetailsService.loadUserByUsername(firstUsername + "@gymeet.com");

		Assertions.assertThrows(ResourceUnauthorizedException.class, () -> {
			relationService.removeRelation(first, secondUsername, RelationType.FOLLOWED.name());
		});
	}

	private Set<ProfileInfo> getCurrentUserMateList(UserDetailsImpl userDetails)
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		return relationService.getCurrentUserRelationList(userDetails, RelationType.MATE.name());
	}

	private Set<ProfileInfo> getCurrentUserFollowerList(UserDetailsImpl userDetails)
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		return relationService.getCurrentUserRelationList(userDetails, RelationType.FOLLOWER.name());
	}

	private Set<ProfileInfo> getCurrentUserFollowedList(UserDetailsImpl userDetails)
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		return relationService.getCurrentUserRelationList(userDetails, RelationType.FOLLOWED.name());
	}

	private Set<ProfileInfo> getRequestPendingList(UserDetailsImpl userDetails)
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		return relationService.getCurrentUserRelationList(userDetails, RelationType.REQUESTER.name());
	}

	private Set<ProfileInfo> getResponsePendingList(UserDetailsImpl userDetails)
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		return relationService.getCurrentUserRelationList(userDetails, RelationType.RESPONDER.name());
	}

	private Set<ProfileInfo> getBlockedList(UserDetailsImpl userDetails)
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		return relationService.getCurrentUserRelationList(userDetails, RelationType.BLOCKING.name());
	}

	private Set<ProfileInfo> getMateList(UserDetailsImpl userDetails, String username) throws BadRequestException,
			ModelMappingException, ResourceNotFoundException, ResourceUnauthorizedException {
		return relationService.getRelationList(userDetails, username, RelationType.MATE.name());
	}

	private Set<ProfileInfo> getFollowerList(UserDetailsImpl userDetails, String username) throws BadRequestException,
			ModelMappingException, ResourceNotFoundException, ResourceUnauthorizedException {
		return relationService.getRelationList(userDetails, username, RelationType.FOLLOWER.name());
	}

	private Set<ProfileInfo> getFollowedList(UserDetailsImpl userDetails, String username) throws BadRequestException,
			ModelMappingException, ResourceNotFoundException, ResourceUnauthorizedException {
		return relationService.getRelationList(userDetails, username, RelationType.FOLLOWED.name());
	}

	private ProfileRelationInfo relationServiceIsMate(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		return relationService.getRelationInfo(userDetails, username, RelationType.MATE);
	}

	private ProfileRelationInfo relationServiceIsFollower(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		return relationService.getRelationInfo(userDetails, username, RelationType.FOLLOWED);
	}

	private ProfileRelationInfo relationServiceIsFollowed(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		return relationService.getRelationInfo(userDetails, username, RelationType.FOLLOWER);
	}

	private ProfileRelationInfo relationServiceHasRequestPending(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		return relationService.getRelationInfo(userDetails, username, RelationType.RESPONDER);
	}

	private ProfileRelationInfo relationServiceHasResponsePending(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		return relationService.getRelationInfo(userDetails, username, RelationType.REQUESTER);
	}

	private ProfileRelationInfo relationServiceIsBlocking(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		return relationService.getRelationInfo(userDetails, username, RelationType.BLOCKING);
	}

	private ProfileRelationInfo relationServiceSendRequest(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceConflictException, ResourceBadRequestException {
		return relationService.addRelation(userDetails, username, RelationType.REQUESTER.name());
	}

	private ProfileRelationInfo relationServiceAbortRequest(UserDetailsImpl userDetails, String username)
			throws ResourceUnauthorizedException, BadRequestException, ResourceNotFoundException,
			ResourceBadRequestException {
		return relationService.removeRelation(userDetails, username, RelationType.REQUESTER.name());
	}

	private ProfileRelationInfo relationServiceAcceptRequest(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceConflictException, ResourceBadRequestException {
		return relationService.addRelation(userDetails, username, RelationType.RESPONDER.name());
	}

	private ProfileRelationInfo relationServiceDeclineRequest(UserDetailsImpl userDetails, String username)
			throws ResourceUnauthorizedException, BadRequestException, ResourceNotFoundException,
			ResourceBadRequestException {
		return relationService.removeRelation(userDetails, username, RelationType.RESPONDER.name());
	}

	private ProfileRelationInfo relationServiceFollow(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceConflictException, ResourceBadRequestException {
		return relationService.addRelation(userDetails, username, RelationType.FOLLOWER.name());
	}

	private ProfileRelationInfo relationServiceUnFollow(UserDetailsImpl userDetails, String username)
			throws ResourceUnauthorizedException, BadRequestException, ResourceNotFoundException,
			ResourceBadRequestException {
		return relationService.removeRelation(userDetails, username, RelationType.FOLLOWER.name());
	}

	private ProfileRelationInfo relationServiceBlock(UserDetailsImpl userDetails, String username)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceConflictException, ResourceBadRequestException {
		return relationService.addRelation(userDetails, username, RelationType.BLOCKING.name());
	}

	private ProfileRelationInfo relationServiceUnBlock(UserDetailsImpl userDetails, String username)
			throws ResourceUnauthorizedException, BadRequestException, ResourceNotFoundException,
			ResourceBadRequestException {
		return relationService.removeRelation(userDetails, username, RelationType.BLOCKING.name());
	}

	private ProfileRelationInfo relationServiceDeleteMate(UserDetailsImpl userDetails, String username)
			throws ResourceUnauthorizedException, BadRequestException, ResourceNotFoundException,
			ResourceBadRequestException {
		return relationService.removeRelation(userDetails, username, RelationType.MATE.name());
	}

	private void isBlocking(UserDetailsImpl first, String secondUsername, UserDetailsImpl second, String firstUsername)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertTrue(info.getBlocked());

		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			relationService.getRelationInfo(second, firstUsername, RelationType.values());
		});
	}

	private void isMateFollowerFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void isMateNotFollowerFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void isMateFollowerNotFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertTrue(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void isMateNotFollowerNotFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertTrue(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void isNotMateFollowerFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void isNotMateNotFollowerFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void isNotMateFollowerNotFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void isNotMateNotFollowerNotFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void hasRequestPendingFollowerFollowed(UserDetailsImpl first, String firstUsername, UserDetailsImpl second,
			String secondUsername) throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertTrue(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void hasRequestPendingNotFollowerFollowed(UserDetailsImpl first, String firstUsername,
			UserDetailsImpl second, String secondUsername)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void hasRequestPendingFollowerNotFollowed(UserDetailsImpl first, String firstUsername,
			UserDetailsImpl second, String secondUsername)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertTrue(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertTrue(info.getFollower());
		assertFalse(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

	private void hasRequestPendingNotFollowerNotFollowed(UserDetailsImpl first, String firstUsername,
			UserDetailsImpl second, String secondUsername)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo info = relationService.getRelationInfo(first, secondUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertFalse(info.getRequester());
		assertTrue(info.getResponder());
		assertFalse(info.getBlocked());

		info = relationService.getRelationInfo(second, firstUsername, RelationType.values());
		assertFalse(info.getMate());
		assertFalse(info.getFollower());
		assertFalse(info.getFollowed());
		assertTrue(info.getRequester());
		assertFalse(info.getResponder());
		assertFalse(info.getBlocked());
	}

}
