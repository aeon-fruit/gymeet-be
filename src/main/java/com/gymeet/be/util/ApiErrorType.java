package com.gymeet.be.util;

import lombok.Getter;

@Getter
public enum ApiErrorType {

	ACCESS_DENIED(4031, "Access denied"), IO_ERROR(5001, "I/O error"),
	UNKNOWN_FILE_TYPE(4001, "Unknown file type for file '%s'"), RESOURCE_NOT_FOUND(4041, "%s not found with %s: '%s'"),
	RESOURCE_BAD_REQUEST(4002, "'%s' is a bad value for %s of %s"),
	RESOURCE_CONFLICT(4091, "%s with %s: '%s' already exists"),
	RESOURCE_UNAUTHORIZED(4032, "Cannot access %s with %s: '%s'"), USER_ROLE_NOT_SET(5002, "User Role is not set"),
	MODEL_MAPPING_ERROR(5003, "Model mapping error from %s to %s"), CANNOT_PROBE_SELF(4003, "Cannot probe self: %s");

	private final int code;
	private final String message;

	ApiErrorType(int code, String message) {
		this.code = code;
		this.message = message;
	}

}
