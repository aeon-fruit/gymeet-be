package com.gymeet.be.util;

public enum FileType {
	UNKNOWN, IMAGE_PJPEG, IMAGE_JPEG, IMAGE_PNG, IMAGE_GIF, IMAGE_BMP, IMAGE_X_PNG, IMAGE_X_ICON
}
