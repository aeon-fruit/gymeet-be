package com.gymeet.be.util;

public enum GameLevelName {
	BEGINNER, AMATEUR, INTERMEDIATE, SEASONED, PROFESSIONAL
}
