package com.gymeet.be.util;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.modelmapper.ConfigurationException;
import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;

import com.gymeet.be.exception.ModelMappingException;

public class DtoUtils {

	private static ModelMapper modelMapper = new ModelMapper();

	public static <T, D> T dtoToEntity(@NotNull D dto, @NotNull Class<? extends T> klass) throws ModelMappingException {
		try {
			return modelMapper.map(dto, klass);
		} catch (IllegalArgumentException | ConfigurationException | MappingException e) {
			throw new ModelMappingException(e, dto.getClass(), klass);
		}
	}

	public static <T, D> T dtoToEntity(@NotNull D dto, @NotNull T entity) throws ModelMappingException {
		try {
			modelMapper.map(dto, entity);
			return entity;
		} catch (IllegalArgumentException | ConfigurationException | MappingException e) {
			throw new ModelMappingException(e, dto.getClass(), entity.getClass());
		}
	}

	public static <T, D> List<T> dtoToEntity(@NotNull List<D> dtoList, @NotNull Class<? extends T> klass)
			throws ModelMappingException {
		final List<T> entityList = new ArrayList<>();
		T entity;
		for (D dto : dtoList) {
			entity = dtoToEntity(dto, klass);
			entityList.add(entity);
		}
		return entityList;
	}

	public static <T, D> D entityToDto(@NotNull T entity, @NotNull Class<? extends D> klass)
			throws ModelMappingException {
		try {
			return modelMapper.map(entity, klass);
		} catch (IllegalArgumentException | ConfigurationException | MappingException e) {
			throw new ModelMappingException(e, entity.getClass(), klass);
		}
	}

	public static <T, D> D entityToDto(@NotNull T entity, @NotNull D dto) throws ModelMappingException {
		try {
			modelMapper.map(entity, dto);
			return dto;
		} catch (IllegalArgumentException | ConfigurationException | MappingException e) {
			throw new ModelMappingException(e, entity.getClass(), dto.getClass());
		}
	}

	public static <T, D> List<D> entityToDto(@NotNull List<T> entityList, @NotNull Class<? extends D> klass)
			throws ModelMappingException {
		final List<D> dtoList = new ArrayList<>();
		D dto;
		for (T entity : entityList) {
			dto = entityToDto(entity, klass);
			dtoList.add(dto);
		}
		return dtoList;
	}

}