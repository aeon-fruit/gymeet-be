package com.gymeet.be.util;

import static java.awt.Color.WHITE;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;

import javax.imageio.ImageIO;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.gymeet.be.exception.UnknownFileTypeException;

public class ImageStorageUtils {

	public static String storePicture(@NotNull MultipartFile file, ImageStorageConfig... imageStorageConfigList)
			throws UnknownFileTypeException, IOException {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());

//        if (filename.contains("..")) {
//            throw new FileStorageException("Sorry! Filename contains invalid path sequence: " + filename);
//        }

		// detect file type
		FileType fileType = isImage(file.getContentType());
		if (fileType == FileType.UNKNOWN) {
			throw new UnknownFileTypeException(filename);
		}

		// generate filename
		String checksum;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(file.getBytes());
			byte[] digest = md.digest();
			checksum = DatatypeConverter.printHexBinary(digest).toLowerCase();
		} catch (Exception ignored) {
			checksum = "";
		}
		String stdFilename = checksum + System.nanoTime() + ".jpg";

		// convert to jpg
		BufferedImage image = ImageIO.read(file.getInputStream());
		for (ImageStorageConfig imageStorageConfig : imageStorageConfigList) {
			saveJPG(image, imageStorageConfig.getLocation().resolve(stdFilename), imageStorageConfig.getWidth(),
					imageStorageConfig.getHeight());
		}

		return stdFilename;
	}

	public static void deletePicture(String filename, Path location) throws IOException {
		if (filename != null) {
			Path picture = location.resolve(filename);
			Files.deleteIfExists(picture);
		}
	}

	private static FileType isImage(String contentType) {
		if (contentType == null) {
			return FileType.UNKNOWN;
		}
		if (contentType.equals("image/pjpeg")) {
			return FileType.IMAGE_PJPEG;
		}
		if (contentType.equals("image/jpeg")) {
			return FileType.IMAGE_JPEG;
		}
		if (contentType.equals("image/png")) {
			return FileType.IMAGE_PNG;
		}
//        if (contentType.equals("image/gif")) {
//            return FileType.IMAGE_GIF;
//        }
		if (contentType.equals("image/bmp")) {
			return FileType.IMAGE_BMP;
		}
		if (contentType.equals("image/x-png")) {
			return FileType.IMAGE_X_PNG;
		}
//        if (contentType.equals("image/x-icon")) {
//            return FileType.IMAGE_X_ICON;
//        }
		return FileType.UNKNOWN;
	}

	private static void saveJPG(@NotNull BufferedImage image, @NotNull Path location, int width, int height)
			throws IOException {
		int imageWidth = image.getWidth();
		int imageHeight = image.getHeight();
		double widthRatio = 1.;
		double heightRatio = 1.;
		double ratio;
		if (width > 0) {
			widthRatio = (imageWidth * 1.) / width;
		}
		if (height > 0) {
			heightRatio = (imageHeight * 1.) / height;
		}
		if (widthRatio < heightRatio) {
			ratio = heightRatio;
		} else {
			ratio = widthRatio;
		}
		width = (int) (imageWidth / ratio);
		height = (int) (imageHeight / ratio);

		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = result.createGraphics();
		g2d.drawImage(image, 0, 0, width, height, WHITE, null);
		g2d.dispose();
		ImageIO.write(result, "jpg", location.toFile());
	}

}
