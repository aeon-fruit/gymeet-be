package com.gymeet.be.util;

public enum RelationType {
	MATE, FOLLOWER, FOLLOWED, REQUESTER, RESPONDER, BLOCKING
}
