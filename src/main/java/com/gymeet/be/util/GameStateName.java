package com.gymeet.be.util;

public enum GameStateName {
	PLANNED, FIXED, HAPPENED, CANCELLED
}
