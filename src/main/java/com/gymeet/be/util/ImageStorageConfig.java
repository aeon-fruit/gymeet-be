package com.gymeet.be.util;

import java.nio.file.Path;

import lombok.Data;

@Data
public class ImageStorageConfig {

	private final Path location;
	private final int width;
	private final int height;

}
