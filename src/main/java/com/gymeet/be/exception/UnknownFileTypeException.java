package com.gymeet.be.exception;

import static com.gymeet.be.util.ApiErrorType.UNKNOWN_FILE_TYPE;

import lombok.Getter;

@Getter
public class UnknownFileTypeException extends GymeetException {

	private static final long serialVersionUID = 2608124258915644750L;

	private String filename;

	public UnknownFileTypeException(String filename) {
		super(UNKNOWN_FILE_TYPE, filename);
		this.filename = filename;
	}

	public UnknownFileTypeException(Throwable cause, String filename) {
		super(cause, UNKNOWN_FILE_TYPE, filename);
		this.filename = filename;
	}

}
