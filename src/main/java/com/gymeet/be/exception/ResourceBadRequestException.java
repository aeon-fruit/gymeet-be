package com.gymeet.be.exception;

import static com.gymeet.be.util.ApiErrorType.RESOURCE_BAD_REQUEST;

import lombok.Getter;

@Getter
public class ResourceBadRequestException extends ResourceException {

	private static final long serialVersionUID = 4727545869645751434L;

	public ResourceBadRequestException(String resourceName, String fieldName, Object fieldValue) {
		super(RESOURCE_BAD_REQUEST, resourceName, fieldName, fieldValue);
	}

	public ResourceBadRequestException(Throwable cause, String resourceName, String fieldName, Object fieldValue) {
		super(cause, RESOURCE_BAD_REQUEST, resourceName, fieldName, fieldValue);
	}

}
