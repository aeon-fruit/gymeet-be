package com.gymeet.be.exception;

import static com.gymeet.be.util.ApiErrorType.MODEL_MAPPING_ERROR;

import lombok.Getter;

@Getter
public class ModelMappingException extends GymeetException {

	private static final long serialVersionUID = -2994475425300224085L;

	public ModelMappingException(Throwable cause, Class<?> source, Class<?> target) {
		super(cause, MODEL_MAPPING_ERROR, source.getName(), target.getName());
	}

}
