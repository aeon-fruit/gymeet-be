package com.gymeet.be.exception;

import com.gymeet.be.util.ApiErrorType;

import lombok.Getter;

@Getter
public abstract class ResourceException extends GymeetException {

	private static final long serialVersionUID = -8912338409711567924L;

	private String resourceName;
	private String fieldName;
	private Object fieldValue;

	public ResourceException(ApiErrorType apiErrorType, String resourceName, String fieldName, Object fieldValue) {
		super(apiErrorType, resourceName, fieldName, fieldValue);
		this.resourceName = resourceName;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
	}

	public ResourceException(Throwable cause, ApiErrorType apiErrorType, String resourceName, String fieldName,
			Object fieldValue) {
		super(cause, apiErrorType, resourceName, fieldName, fieldValue);
		this.resourceName = resourceName;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
	}

}
