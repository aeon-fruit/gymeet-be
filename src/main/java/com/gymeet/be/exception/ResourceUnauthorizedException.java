package com.gymeet.be.exception;

import static com.gymeet.be.util.ApiErrorType.RESOURCE_UNAUTHORIZED;

import lombok.Getter;

@Getter
public class ResourceUnauthorizedException extends ResourceException {

	private static final long serialVersionUID = 2882387510388352019L;

	public ResourceUnauthorizedException(String resourceName, String fieldName, Object fieldValue) {
		super(RESOURCE_UNAUTHORIZED, resourceName, fieldName, fieldValue);
	}

	public ResourceUnauthorizedException(Throwable cause, String resourceName, String fieldName, Object fieldValue) {
		super(cause, RESOURCE_UNAUTHORIZED, resourceName, fieldName, fieldValue);
	}

}
