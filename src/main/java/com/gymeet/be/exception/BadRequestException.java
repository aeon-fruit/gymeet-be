package com.gymeet.be.exception;

import com.gymeet.be.util.ApiErrorType;

public class BadRequestException extends GymeetException {

	private static final long serialVersionUID = -1654860453478536982L;

	public BadRequestException(ApiErrorType apiErrorType, Object... args) {
		super(apiErrorType, args);
	}

	public BadRequestException(ApiErrorType apiErrorType) {
		super(apiErrorType);
	}

	public BadRequestException(Throwable cause, ApiErrorType apiErrorType, Object... args) {
		super(cause, apiErrorType, args);
	}

	public BadRequestException(Throwable cause, ApiErrorType apiErrorType) {
		super(cause, apiErrorType);
	}

}
