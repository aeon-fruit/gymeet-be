package com.gymeet.be.exception;

import static com.gymeet.be.util.ApiErrorType.RESOURCE_CONFLICT;

import lombok.Getter;

@Getter
public class ResourceConflictException extends ResourceException {

	private static final long serialVersionUID = 4181185100860478425L;

	public ResourceConflictException(String resourceName, String fieldName, Object fieldValue) {
		super(RESOURCE_CONFLICT, resourceName, fieldName, fieldValue);
	}

	public ResourceConflictException(Throwable cause, String resourceName, String fieldName, Object fieldValue) {
		super(cause, RESOURCE_CONFLICT, resourceName, fieldName, fieldValue);
	}

}
