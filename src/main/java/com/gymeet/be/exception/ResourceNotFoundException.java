package com.gymeet.be.exception;

import static com.gymeet.be.util.ApiErrorType.RESOURCE_NOT_FOUND;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends ResourceException {

	private static final long serialVersionUID = -1007347098862071878L;

	public ResourceNotFoundException(String resourceName, String fieldName, Object fieldValue) {
		super(RESOURCE_NOT_FOUND, resourceName, fieldName, fieldValue);
	}

	public ResourceNotFoundException(Throwable cause, String resourceName, String fieldName, Object fieldValue) {
		super(cause, RESOURCE_NOT_FOUND, resourceName, fieldName, fieldValue);
	}

}
