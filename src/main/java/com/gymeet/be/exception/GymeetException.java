package com.gymeet.be.exception;

import com.gymeet.be.util.ApiErrorType;

import lombok.Getter;

@Getter
public abstract class GymeetException extends Exception {

	private static final long serialVersionUID = 4591387323918045350L;

	private Integer code;

	public GymeetException(ApiErrorType apiErrorType, Object... args) {
		super(String.format(apiErrorType.getMessage(), args));
		this.code = apiErrorType.getCode();
	}

	public GymeetException(ApiErrorType apiErrorType) {
		super(apiErrorType.getMessage());
		this.code = apiErrorType.getCode();
	}

	public GymeetException(Throwable cause, ApiErrorType apiErrorType, Object... args) {
		super(String.format(apiErrorType.getMessage(), args), cause);
		this.code = apiErrorType.getCode();
	}

	public GymeetException(Throwable cause, ApiErrorType apiErrorType) {
		super(apiErrorType.getMessage(), cause);
		this.code = apiErrorType.getCode();
	}

}
