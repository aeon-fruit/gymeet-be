package com.gymeet.be.exception;

public class FileStorageException extends RuntimeException {

	private static final long serialVersionUID = -8654323085229663093L;

	public FileStorageException(String message) {
		super(message);
	}

	public FileStorageException(String message, Throwable cause) {
		super(message, cause);
	}

}
