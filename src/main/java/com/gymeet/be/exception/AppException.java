package com.gymeet.be.exception;

import com.gymeet.be.util.ApiErrorType;

public class AppException extends GymeetException {

	private static final long serialVersionUID = 7591056383278695865L;

	public AppException(ApiErrorType apiErrorType, Object... args) {
		super(apiErrorType, args);
	}

	public AppException(ApiErrorType apiErrorType) {
		super(apiErrorType);
	}

	public AppException(Throwable cause, ApiErrorType apiErrorType, Object... args) {
		super(cause, apiErrorType, args);
	}

	public AppException(Throwable cause, ApiErrorType apiErrorType) {
		super(cause, apiErrorType);
	}

}
