package com.gymeet.be.payload;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SignUpPostRequest {

	@NotBlank
	@Email
	@JsonProperty(required = true)
	private String email;

	@NotBlank
	@JsonProperty(required = true)
	private String password;

	@NotBlank
	@JsonProperty(value = "first_name", required = true)
	private String firstName;

	@NotBlank
	@JsonProperty(required = true)
	private String surname;

	@Pattern(regexp = "\\+?([0-9]+[\\- ]?[0-9]+)*")
	@JsonProperty(value = "phone_number")
	private String phoneNumber;

	@NotNull
	@Past
	@JsonProperty(value = "birth_date", required = true)
	private Date birthDate;

	@NotNull
	@JsonProperty(required = true)
	private Boolean female;

	// TODO - de-comment and receive/infer location information (collect IP
	// information later on)
//    @JsonIgnore
//    private Date accountCreationTimestamp;
//
//    @NotBlank
//    @JsonProperty(value = "last_known_location", required = true)
//    private String lastKnownLocation;
//
//    @JsonIgnore
//    private Date lastKnownLocationTimestamp;
//
//    @JsonIgnore
//    private Date lastLoginTimestamp;
//
//    @NotBlank
//    @JsonProperty(value = "last_login_location", required = true)
//    private String lastLoginLocation;
//
//    @JsonIgnore
//    private Date lastActiveTimestamp;
//
//    {
//        Date now = new Date();
//        accountCreationTimestamp = now;
//        lastKnownLocationTimestamp = now;
//        lastLoginTimestamp = now;
//        lastActiveTimestamp = now;
//    }

}
