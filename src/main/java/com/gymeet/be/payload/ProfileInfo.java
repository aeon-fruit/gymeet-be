package com.gymeet.be.payload;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude = { "thumbnailPicture", "profilePicture" })
public class ProfileInfo {

	@NotBlank
	private String username;

	@NotBlank
	@JsonProperty(value = "first_name")
	private String firstName;

	@NotBlank
	private String surname;

	@JsonProperty(value = "thumbnail_picture")
	private String thumbnailPicture;

	@JsonProperty(value = "profile_picture")
	private String profilePicture;

	@JsonProperty(value = "relation_info")
	private ProfileRelationInfo relationInfo;

	public ProfileInfo() {
	}

	public ProfileInfo(@NotBlank String username, @NotBlank String firstName, @NotBlank String surname,
			String thumbnailPicture, String profilePicture) {
		this.username = username;
		this.firstName = firstName;
		this.surname = surname;
		this.thumbnailPicture = thumbnailPicture;
		this.profilePicture = profilePicture;
	}

}
