package com.gymeet.be.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class VenueInfo {

	@NotNull
	private Long id;

	@NotBlank
	private String name;

	@JsonProperty(value = "medium_picture")
	private String mediumPicture;

	@JsonProperty(value = "thumbnail_picture")
	private String thumbnailPicture;

	public VenueInfo(@NotNull Long id, @NotBlank String name, String mediumPicture, String thumbnailPicture) {
		this.id = id;
		this.name = name;
		this.mediumPicture = mediumPicture;
		this.thumbnailPicture = thumbnailPicture;
	}

}
