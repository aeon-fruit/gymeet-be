package com.gymeet.be.payload;

import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class VenuePostRequest {

	@NotBlank
	private String name;

	@NotNull
	private Double latitude;

	@NotNull
	private Double longitude;

	@NotBlank
	private String address;

	private String description;

	@NotNull
	private Set<Long> activities;

	public VenuePostRequest(@NotBlank String name, @NotNull Double latitude, @NotNull Double longitude,
			@NotBlank String address, String description, @NotNull Set<Long> activities) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
		this.description = description;
		this.activities = activities;
	}

}
