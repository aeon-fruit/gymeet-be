package com.gymeet.be.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ActivityInfo {

	@NotNull
	private Long id;

	@NotBlank
	private String name;

	@NotBlank
	private String icon;

	public ActivityInfo(@NotNull Long id, @NotBlank String name, @NotBlank String icon) {
		this.id = id;
		this.name = name;
		this.icon = icon;
	}

}
