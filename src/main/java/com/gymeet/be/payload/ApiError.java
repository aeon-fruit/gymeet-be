package com.gymeet.be.payload;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ApiError {

	private Integer code;
	private HttpStatus status;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	private String message;
	private List<ApiSubError> details;
	@JsonProperty("debug_message")
	private String debugMessage;
	private String uri;

	private ApiError() {
		timestamp = LocalDateTime.now();
	}

	public ApiError(HttpStatus status) {
		this();
		this.status = status;
	}

	public ApiError(HttpStatus status, Integer code, String message, Throwable ex) {
		this();
		this.status = status;
		this.code = code;
		this.message = message;
		this.debugMessage = ex.getLocalizedMessage();
	}

	public void addSubError(ApiSubError apiSubError) {
		if (details == null) {
			details = new ArrayList<>();
		}
		details.add(apiSubError);
	}

}
