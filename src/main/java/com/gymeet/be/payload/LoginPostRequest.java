package com.gymeet.be.payload;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class LoginPostRequest {

	@NotBlank
	@JsonProperty(required = true)
	private String login;

	@NotBlank
	@JsonProperty(required = true)
	private String password;

	// TODO - de-comment and receive/infer location information (collect IP
	// information later on)
//    @NotBlank
//    @JsonProperty(value = "last_known_location", required = true)
//    private String lastKnownLocation;
//
//    @JsonIgnore
//    private Date lastKnownLocationTimestamp;
//
//    @JsonIgnore
//    private Date lastLoginTimestamp;
//
//    @NotBlank
//    @JsonProperty(value = "last_login_location", required = true)
//    private String lastLoginLocation;
//
//    @JsonIgnore
//    private Date lastActiveTimestamp;
//
//    {
//        Date now = new Date();
//        lastKnownLocationTimestamp = now;
//        lastLoginTimestamp = now;
//        lastActiveTimestamp = now;
//    }

}
