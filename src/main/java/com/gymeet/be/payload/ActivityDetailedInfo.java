package com.gymeet.be.payload;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class ActivityDetailedInfo {

	private Long id;

	@NotBlank
	private String name;

	@NotBlank
	private String icon;

	private String description;

	public ActivityDetailedInfo(Long id, @NotBlank String name, @NotBlank String icon, String description) {
		this.id = id;
		this.name = name;
		this.icon = icon;
		this.description = description;
	}

}
