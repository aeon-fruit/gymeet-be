package com.gymeet.be.payload;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class ApiValidationError extends ApiSubError {

	private String object;
	private String field;
	private Object rejectedValue;
	private String message;

	public ApiValidationError(String object, String message) {
		this.object = object;
		this.message = message;
	}

	public static void addValidationErrors(ApiError parent, List<FieldError> fieldErrors) {
		fieldErrors.forEach(fieldError -> addValidationError(parent, fieldError));
	}

	public static void addValidationError(ApiError parent, List<ObjectError> globalErrors) {
		globalErrors.forEach(globalError -> addValidationError(parent, globalError));
	}

	public static void addValidationErrors(ApiError parent, Set<ConstraintViolation<?>> constraintViolations) {
		constraintViolations.forEach(constraintViolation -> addValidationError(parent, constraintViolation));
	}

	private static void addValidationError(ApiError parent, FieldError fieldError) {
		addValidationError(parent, fieldError.getObjectName(), fieldError.getField(), fieldError.getRejectedValue(),
				fieldError.getDefaultMessage());
	}

	private static void addValidationError(ApiError parent, ObjectError objectError) {
		addValidationError(parent, objectError.getObjectName(), objectError.getDefaultMessage());
	}

	private static void addValidationError(ApiError parent, String object, String field, Object rejectedValue,
			String message) {
		parent.addSubError(new ApiValidationError(object, field, rejectedValue, message));
	}

	private static void addValidationError(ApiError parent, String object, String message) {
		parent.addSubError(new ApiValidationError(object, message));
	}

	private static void addValidationError(ApiError parent, ConstraintViolation<?> cv) {
		addValidationError(parent, cv.getRootBeanClass().getSimpleName(),
				((PathImpl) cv.getPropertyPath()).getLeafNode().asString(), cv.getInvalidValue(), cv.getMessage());
	}

}
