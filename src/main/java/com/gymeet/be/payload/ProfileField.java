package com.gymeet.be.payload;

import lombok.Data;

@Data
public class ProfileField {

	private Object value;

	public ProfileField(Object value) {
		this.value = value;
	}

}
