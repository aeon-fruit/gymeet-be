package com.gymeet.be.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserIdentityAvailability {

	@JsonProperty(required = true)
	private Boolean available;

	public UserIdentityAvailability(Boolean available) {
		this.available = available;
	}

}
