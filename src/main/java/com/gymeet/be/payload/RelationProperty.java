package com.gymeet.be.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RelationProperty {

	@JsonProperty(required = true)
	private Boolean haveRelation;

	public RelationProperty(Boolean haveRelation) {
		this.haveRelation = haveRelation;
	}

}
