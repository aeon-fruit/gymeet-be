package com.gymeet.be.payload;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProfileDetailedInfo extends ProfileInfo {

	@NotBlank
	private String email;

	@JsonProperty(value = "phone_number")
	private String phoneNumber;

	@NotNull
	private Date birthDate;

	@NotNull
	private Boolean female;

	@JsonProperty(value = "home_country")
	private String homeCountry;

	private String country;

	private String city;

	public ProfileDetailedInfo(@NotBlank String username, @NotBlank String firstName, @NotBlank String surname,
			String thumbnailPicture, String profilePicture, @NotBlank String email, String phoneNumber,
			@NotNull Date birthDate, @NotNull Boolean female, String homeCountry, String country, String city) {
		super(username, firstName, surname, thumbnailPicture, profilePicture);
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.birthDate = birthDate;
		this.female = female;
		this.homeCountry = homeCountry;
		this.country = country;
		this.city = city;
	}

}
