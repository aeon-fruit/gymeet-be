package com.gymeet.be.payload;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class JwtAuthenticationResponse {

	@NotBlank
	@JsonProperty(value = "access_token", required = true)
	private String accessToken;

	@NotBlank
	@JsonProperty(value = "token_type", required = true)
	private String tokenType = "Bearer";

	public JwtAuthenticationResponse(String accessToken) {
		this.accessToken = accessToken;
	}

}
