package com.gymeet.be.payload;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProfileRelationInfo {

	private Boolean mate;
	private Boolean follower;
	private Boolean followed;
	private Boolean requester;
	private Boolean responder;
	private Boolean blocked;

	public ProfileRelationInfo() {
		this.mate = false;
		this.follower = false;
		this.followed = false;
		this.requester = false;
		this.responder = false;
		this.blocked = false;
	}

	public ProfileRelationInfo(Boolean mate, Boolean follower, Boolean followed, Boolean requester, Boolean responder,
			Boolean blocked) {
		this.mate = mate;
		this.follower = follower;
		this.followed = followed;
		this.requester = requester;
		this.responder = responder;
		this.blocked = blocked;
	}

	public void setMate(@NotNull Boolean mate) {
		this.mate = mate;
		if (mate) {
			requester = false;
			responder = false;
			blocked = false;
		}
	}

	public void setFollower(@NotNull Boolean follower) {
		this.follower = follower;
		if (follower) {
			blocked = false;
		}
	}

	public void setFollowed(@NotNull Boolean followed) {
		this.followed = followed;
		if (followed) {
			blocked = false;
		}
	}

	public void setRequester(@NotNull Boolean requester) {
		this.requester = requester;
		if (requester) {
			mate = false;
			responder = false;
			blocked = false;
		}
	}

	public void setResponder(@NotNull Boolean responder) {
		this.responder = responder;
		if (responder) {
			mate = false;
			requester = false;
			blocked = false;
		}
	}

	public void setBlocked(@NotNull Boolean blocked) {
		this.blocked = blocked;
		if (blocked) {
			mate = false;
			follower = false;
			followed = false;
			requester = false;
			responder = false;
		}
	}

}
