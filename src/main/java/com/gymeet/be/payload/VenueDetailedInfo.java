package com.gymeet.be.payload;

import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class VenueDetailedInfo {

	private Long id;

	@NotBlank
	private String name;

	@NotNull
	private Double latitude;

	@NotNull
	private Double longitude;

	@NotBlank
	private String address;

	@JsonProperty(value = "large_picture")
	private String largePicture;

	@JsonProperty(value = "medium_picture")
	private String mediumPicture;

	@JsonProperty(value = "thumbnail_picture")
	private String thumbnailPicture;

	private String description;

	@NotNull
	private Set<ActivityInfo> activityInfoSet;

	public VenueDetailedInfo(Long id, @NotBlank String name, @NotNull Double latitude, @NotNull Double longitude,
			@NotBlank String address, String largePicture, String mediumPicture, String thumbnailPicture,
			String description, @NotNull Set<ActivityInfo> activityInfoSet) {
		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
		this.largePicture = largePicture;
		this.mediumPicture = mediumPicture;
		this.thumbnailPicture = thumbnailPicture;
		this.description = description;
		this.activityInfoSet = activityInfoSet;
	}

}
