package com.gymeet.be.payload;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class ActivityPostRequest {

	@NotBlank
	private String name;

	private String description;

	public ActivityPostRequest(@NotBlank String name, String description) {
		this.name = name;
		this.description = description;
	}

}
