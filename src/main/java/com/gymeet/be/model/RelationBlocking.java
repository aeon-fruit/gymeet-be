package com.gymeet.be.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_relation_blocking", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "blocking_olympesk_user_id_ref", "blocked_olympesk_user_id_ref" }) })
public class RelationBlocking {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "blocking_olympesk_user_id_ref", nullable = false)
	private User blocking;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "blocked_olympesk_user_id_ref", nullable = false)
	private User blocked;

	public RelationBlocking(@NotNull User blocking, @NotNull User blocked) {
		this.blocking = blocking;
		this.blocked = blocked;
	}

}
