package com.gymeet.be.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_venue")
public class Venue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Column(name = "name", nullable = false, unique = true, length = 60)
	private String name;

	@NotNull
	@Column(name = "latitude", nullable = false)
	private Double latitude;

	@NotNull
	@Column(name = "longitude", nullable = false)
	private Double longitude;

	@NotBlank
	@Column(name = "address", nullable = false)
	private String address;

	@Column(name = "large_picture")
	private String largePicture;

	@Column(name = "medium_picture")
	private String mediumPicture;

	@Column(name = "thumbnail_picture")
	private String thumbnailPicture;

	@Column(name = "description", length = 3000)
	private String description;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "olympesk_activities_venues", joinColumns = @JoinColumn(name = "olympesk_venue_id_ref", nullable = false), inverseJoinColumns = @JoinColumn(name = "olympesk_activity_id_ref", nullable = false))
	private Set<Activity> activities = new HashSet<>();

	// TODO - add working hours
	// TODO - add photos (page w/ comments, etc.)

	public Venue() {
	}

	public Venue(@NotBlank String name, @NotNull Double latitude, @NotNull Double longitude, @NotBlank String address,
			String largePicture, String mediumPicture, String thumbnailPicture, String description) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
		this.largePicture = largePicture;
		this.mediumPicture = mediumPicture;
		this.thumbnailPicture = thumbnailPicture;
		this.description = description;
	}

	public void addActivity(Activity activity) {
		activities.add(activity);
		activity.getVenues().add(this);
	}

	public void removeActivity(Activity activity) {
		activities.remove(activity);
		activity.getVenues().remove(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Venue))
			return false;
		return id != null && id.equals(((Venue) o).id) && name != null && name.equals(((Venue) o).name)
				&& latitude != null && latitude.equals(((Venue) o).latitude) && longitude != null
				&& longitude.equals(((Venue) o).longitude);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, latitude, longitude);
	}

}
