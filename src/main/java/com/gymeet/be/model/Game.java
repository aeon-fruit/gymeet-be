package com.gymeet.be.model;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_game")
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Past
	@Column(name = "creation_timestamp", nullable = false)
	private Calendar creationTimestamp;

	@NotBlank
	@Column(name = "title", nullable = false, length = 128)
	private String title;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "planner_olympesk_user_id_ref", nullable = false)
	private User planner;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "olympesk_games_owners", joinColumns = @JoinColumn(name = "olympesk_game_id_ref", nullable = false), inverseJoinColumns = @JoinColumn(name = "olympesk_user_id_ref", nullable = false))
	private Set<User> owners = new HashSet<>();

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "olympesk_activity_id_ref", nullable = false)
	private Activity activity;

	@Column(name = "description", length = 3000)
	private String description;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "olympesk_level_id_ref", nullable = false)
	private GameLevel level;

	@OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
	private Set<GameVenue> gameVenues = new HashSet<>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "olympesk_games_players", joinColumns = @JoinColumn(name = "olympesk_game_id_ref", nullable = false), inverseJoinColumns = @JoinColumn(name = "olympesk_user_id_ref", nullable = false))
	private Set<User> players = new HashSet<>();

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "olympesk_state_id_ref", nullable = false)
	private GameState state;

	@Column(name = "large_picture")
	private String largePicture;

	@Column(name = "medium_picture")
	private String mediumPicture;

	@Column(name = "thumbnail_picture")
	private String thumbnailPicture;

	public Game() {
	}

	public Game(@NotNull @Past Calendar creationTimestamp, @NotBlank String title, @NotNull User planner,
			Set<User> owners, @NotNull Activity activity, String description, @NotNull GameLevel level,
			Set<GameVenue> gameVenues, Set<User> players, @NotNull GameState state, String largePicture,
			String mediumPicture, String thumbnailPicture) {
		this.creationTimestamp = creationTimestamp;
		this.title = title;
		this.planner = planner;
		this.owners = owners;
		this.activity = activity;
		this.description = description;
		this.level = level;
		this.gameVenues = gameVenues;
		this.players = players;
		this.state = state;
		this.largePicture = largePicture;
		this.mediumPicture = mediumPicture;
		this.thumbnailPicture = thumbnailPicture;
	}

	public void addGameVenue(GameVenue gameVenue) {
		gameVenues.add(gameVenue);
		gameVenue.setGame(this);
	}

	public void removeGameVenue(GameVenue gameVenue) {
		gameVenues.remove(gameVenue);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Game))
			return false;
		return id != null && id.equals(((Game) o).id) && planner != null && planner.equals(((Game) o).planner)
				&& creationTimestamp != null && creationTimestamp.equals(((Game) o).creationTimestamp);
	}

	@Override
	public int hashCode() {
		return Objects.hash(planner, creationTimestamp);
	}

}
