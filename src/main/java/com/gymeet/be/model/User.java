package com.gymeet.be.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Email
	@Column(name = "email", nullable = false, unique = true, length = 60)
	private String email;

	@NotBlank
	@Column(name = "password", nullable = false, length = 500)
	private String password;

	@NotBlank
	@Column(name = "first_name", nullable = false, length = 40)
	private String firstName;

	@NotBlank
	@Column(name = "surname", nullable = false, length = 25)
	private String surname;

	@Column(name = "large_picture")
	private String largePicture;

	@Column(name = "profile_picture")
	private String profilePicture;

	@Column(name = "thumbnail_picture")
	private String thumbnailPicture;

	@Pattern(regexp = "\\+?([0-9]+[\\- ]?[0-9]+)*")
	@Column(name = "phone_number", unique = true, length = 25)
	private String phoneNumber;

	// URI suffix
	@NotBlank
	@Pattern(regexp = "[a-zA-Z0-9\\-_.]+")
	@Column(name = "username", nullable = false, unique = true, length = 70)
	private String username;

	@NotNull
	@Past
	// @Temporal(TemporalType.DATE)
	@Column(name = "birth_date", nullable = false)
	private Date birthDate;

	@NotNull
	@Column(name = "female", nullable = false)
	private Boolean female;

	@Column(name = "home_country")
	private String homeCountry;

	@Column(name = "country")
	private String country;

	@Column(name = "city")
	private String city;

	// TODO - de-comment and receive/infer location information (collect IP
	// information later on)
//    @NotNull
//    @PastOrPresent
//    @Column(name = "account_creation_timestamp", nullable = false)
//    private Date accountCreationTimestamp;
//
//    @NotBlank
//    @Column(name = "last_known_location", nullable = false)
//    private String lastKnownLocation;
//
//    @NotNull
//    @PastOrPresent
//    @Column(name = "last_known_location_timestamp", nullable = false)
//    private Date lastKnownLocationTimestamp;
//
//    @NotNull
//    @PastOrPresent
//    @Column(name = "last_login_timestamp", nullable = false)
//    private Date lastLoginTimestamp;
//
//    @NotBlank
//    @Column(name = "last_login_location", nullable = false)
//    private String lastLoginLocation;
//
//    @NotNull
//    @PastOrPresent
//    @Column(name = "last_active_timestamp", nullable = false)
//    private Date lastActiveTimestamp;

	// TODO - add account/features (de)activation

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "olympesk_users_roles", joinColumns = @JoinColumn(name = "olympesk_user_id_ref", nullable = false), inverseJoinColumns = @JoinColumn(name = "olympesk_role_id_ref", nullable = false))
	private Set<Role> roles = new HashSet<>();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "olympesk_users_activities", joinColumns = @JoinColumn(name = "olympesk_user_id_ref", nullable = false), inverseJoinColumns = @JoinColumn(name = "olympesk_activity_id_ref", nullable = false))
	private Set<Activity> activities = new HashSet<>();

	public User() {
	}

	public User(@NotBlank @Email String email, @NotBlank String password, @NotBlank String firstName,
			@NotBlank String surname, @Pattern(regexp = "\\+?([0-9]+[\\- ]?[0-9]+)*") String phoneNumber,
			@NotNull @Past Date birthDate, @NotNull Boolean female) {
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.surname = surname;
		this.phoneNumber = phoneNumber;
		this.birthDate = birthDate;
		this.female = female;
		// TODO - generate username for URI
		this.username = firstName + "." + surname;
	}

}
