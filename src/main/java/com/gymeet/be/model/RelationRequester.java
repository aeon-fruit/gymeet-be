package com.gymeet.be.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_relation_requester", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "requester_olympesk_user_id_ref", "requested_olympesk_user_id_ref" }) })
public class RelationRequester {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requester_olympesk_user_id_ref", nullable = false)
	private User requester;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requested_olympesk_user_id_ref", nullable = false)
	private User requested;

	public RelationRequester(@NotNull User requester, @NotNull User requested) {
		this.requester = requester;
		this.requested = requested;
	}

}
