package com.gymeet.be.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_activity")
public class Activity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Column(name = "name", nullable = false, unique = true, length = 60)
	private String name;

	@NotBlank
	@Column(name = "icon", nullable = false)
	private String icon;

	@Column(name = "description", length = 3000)
	private String description;

	@ManyToMany(mappedBy = "activities")
	private Set<Venue> venues = new HashSet<>();

	public Activity() {
	}

	public Activity(@NotBlank String name, @NotBlank String icon, String description) {
		this.name = name;
		this.icon = icon;
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Activity tag = (Activity) o;
		return Objects.equals(name, tag.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

}
