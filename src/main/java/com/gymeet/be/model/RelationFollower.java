package com.gymeet.be.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_relation_follower", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "follower_olympesk_user_id_ref", "followed_olympesk_user_id_ref" }) })
public class RelationFollower {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "follower_olympesk_user_id_ref", nullable = false)
	private User follower;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "followed_olympesk_user_id_ref", nullable = false)
	private User followed;

	public RelationFollower(@NotNull User follower, @NotNull User followed) {
		this.follower = follower;
		this.followed = followed;
	}

}
