package com.gymeet.be.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_game_venue", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "olympesk_game_id_ref", "olympesk_venue_id_ref" }) })
public class GameVenue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "olympesk_game_id_ref", nullable = false)
	private Game game;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "olympesk_venue_id_ref", nullable = false)
	private Venue venue;

	@OneToMany(mappedBy = "gameVenue", cascade = CascadeType.ALL)
	private Set<GameVenueDatetime> gameVenueDatetimeSet = new HashSet<>();

	public GameVenue() {
	}

	public GameVenue(@NotNull Game game, @NotNull Venue venue, Set<GameVenueDatetime> gameVenueDatetimeSet) {
		this.game = game;
		this.venue = venue;
		this.gameVenueDatetimeSet = gameVenueDatetimeSet;
	}

	public void addGameVenueDatetime(GameVenueDatetime gameVenueDatetime) {
		gameVenueDatetimeSet.add(gameVenueDatetime);
		gameVenueDatetime.setGameVenue(this);
	}

	public void removeGameVenueDatetime(GameVenueDatetime gameVenueDatetime) {
		gameVenueDatetimeSet.remove(gameVenueDatetime);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		GameVenue tag = (GameVenue) o;
		return Objects.equals(game, tag.game) && Objects.equals(venue, tag.venue);
	}

	@Override
	public int hashCode() {
		return Objects.hash(game, venue);
	}

}
