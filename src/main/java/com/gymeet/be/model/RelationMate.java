package com.gymeet.be.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_relation_mate", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "first_olympesk_user_id_ref", "second_olympesk_user_id_ref" }) })
public class RelationMate {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "first_olympesk_user_id_ref", nullable = false)
	private User first;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "second_olympesk_user_id_ref", nullable = false)
	private User second;

	public RelationMate(@NotNull User first, @NotNull User second) {
		this.first = first;
		this.second = second;
	}

}
