package com.gymeet.be.model;

import java.util.Calendar;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name = "olympesk_game_datetime", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "olympesk_game_venue_id_ref", "game_datetime" }) })
public class GameVenueDatetime {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "olympesk_game_venue_id_ref", nullable = false)
	private GameVenue gameVenue;

	@NotNull
	@Future
	@Column(name = "game_datetime", nullable = false)
	private Calendar gameDatetime;

	public GameVenueDatetime() {
	}

	public GameVenueDatetime(@NotNull GameVenue gameVenue, @NotNull @Future Calendar gameDatetime) {
		this.gameVenue = gameVenue;
		this.gameDatetime = gameDatetime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof GameVenueDatetime))
			return false;
		return id != null && id.equals(((GameVenueDatetime) o).id) && gameVenue != null
				&& gameVenue.equals(((GameVenueDatetime) o).gameVenue) && gameDatetime != null
				&& gameDatetime.getTimeInMillis() == (((GameVenueDatetime) o).gameDatetime.getTimeInMillis());
	}

	@Override
	public int hashCode() {
		return Objects.hash(gameVenue, gameDatetime.getTimeInMillis());
	}

}
