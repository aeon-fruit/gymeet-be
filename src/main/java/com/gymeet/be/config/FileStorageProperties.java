package com.gymeet.be.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {

	private User user;
	private Venue venue;
	private Activity activity;

	@Data
	public static class User {
		private Picture picture;
	}

	@Data
	public static class Venue {
		private Picture picture;
	}

	@Data
	public static class Activity {
		private Icon icon;
	}

	@Data
	public static class Picture {
		private Original original;
		private Sized medium;
		private Sized thumbnail;

		@Data
		public static class Original {
			private String dir;
		}

		@Data
		public static class Sized {
			private String dir;
			private int height;
			private int width;
		}
	}

	@Data
	public static class Icon {
		private String dir;
		private int height;
		private int width;
	}

}
