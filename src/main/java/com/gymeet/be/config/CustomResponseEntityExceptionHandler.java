package com.gymeet.be.config;

import static com.gymeet.be.util.ApiErrorType.ACCESS_DENIED;
import static com.gymeet.be.util.ApiErrorType.IO_ERROR;

import java.io.IOException;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.gymeet.be.exception.AppException;
import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.ResourceUnauthorizedException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ApiError;
import com.gymeet.be.payload.ApiValidationError;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(AccessDeniedException.class)
	protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException e, WebRequest request) {
		return buildResponseEntity(
				new ApiError(HttpStatus.FORBIDDEN, ACCESS_DENIED.getCode(), ACCESS_DENIED.getMessage(), e), e, request);
	}

	@ExceptionHandler(IOException.class)
	protected ResponseEntity<Object> handleIOException(IOException e, WebRequest request) {
		return buildResponseEntity(
				new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, IO_ERROR.getCode(), IO_ERROR.getMessage(), e), e,
				request);
	}

	@ExceptionHandler(AppException.class)
	protected ResponseEntity<Object> handleAppException(AppException e, WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, e.getCode(), e.getMessage(), e), e,
				request);
	}

	@ExceptionHandler(BadRequestException.class)
	protected ResponseEntity<Object> handleBadRequestException(BadRequestException e, WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, e.getCode(), e.getMessage(), e), e, request);
	}

	@ExceptionHandler(ModelMappingException.class)
	protected ResponseEntity<Object> handleModelMappingException(ModelMappingException e, WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, e.getCode(), e.getMessage(), e), e,
				request);
	}

	@ExceptionHandler(ResourceBadRequestException.class)
	protected ResponseEntity<Object> handleResourceBadRequestException(ResourceBadRequestException e,
			WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, e.getCode(), e.getMessage(), e), e, request);
	}

	@ExceptionHandler(ResourceConflictException.class)
	protected ResponseEntity<Object> handleResourceConflictException(ResourceConflictException e, WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, e.getCode(), e.getMessage(), e), e, request);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	protected ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException e, WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, e.getCode(), e.getMessage(), e), e, request);
	}

	@ExceptionHandler(ResourceUnauthorizedException.class)
	protected ResponseEntity<Object> handleResourceUnauthorizedException(ResourceUnauthorizedException e,
			WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.UNAUTHORIZED, e.getCode(), e.getMessage(), e), e, request);
	}

	@ExceptionHandler(UnknownFileTypeException.class)
	protected ResponseEntity<Object> handleUnknownFileTypeException(UnknownFileTypeException e, WebRequest request) {
		return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, e.getCode(), e.getMessage(), e), e, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
		apiError.setMessage("Validation error");
		ApiValidationError.addValidationErrors(apiError, ex.getBindingResult().getFieldErrors());
		ApiValidationError.addValidationError(apiError, ex.getBindingResult().getGlobalErrors());
		return buildResponseEntity(apiError, ex, request);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		// TODO
		ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
		apiError.setMessage("Validation error");
		ApiValidationError.addValidationErrors(apiError, ex.getConstraintViolations());
		return buildResponseEntity(apiError, ex, request);
	}

	private ResponseEntity<Object> buildResponseEntity(ApiError apiError, Exception e, WebRequest request) {
		return handleExceptionInternal(e, apiError, new HttpHeaders(), apiError.getStatus(), request);
	}

}
