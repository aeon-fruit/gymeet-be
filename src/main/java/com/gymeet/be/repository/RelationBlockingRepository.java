package com.gymeet.be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gymeet.be.model.RelationBlocking;
import com.gymeet.be.model.User;

@Repository
public interface RelationBlockingRepository extends JpaRepository<RelationBlocking, Long> {

	List<RelationBlocking> findByBlocking(User blocking);

	RelationBlocking findByBlockingAndBlocked(User blocking, User blocked);

	Boolean existsByBlockingAndBlocked(User blocking, User blocked);

}
