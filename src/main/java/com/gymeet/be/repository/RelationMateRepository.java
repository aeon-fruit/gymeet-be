package com.gymeet.be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.model.RelationMate;
import com.gymeet.be.model.User;

@Repository
public interface RelationMateRepository extends JpaRepository<RelationMate, Long> {

	List<RelationMate> findByFirst(User user);

	List<RelationMate> findBySecond(User user);

	RelationMate findByFirstAndSecondOrSecondAndFirst(User user, User mate, User userMirror, User mateMirror);

	Boolean existsByFirstAndSecondOrSecondAndFirst(User user, User mate, User userMirror, User mateMirror);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM olympesk_relation_mate " + "WHERE first_olympesk_user_id_ref = :userId "
			+ "AND second_olympesk_user_id_ref = :mateId " + "OR first_olympesk_user_id_ref = :mateId "
			+ "AND second_olympesk_user_id_ref = :userId", nativeQuery = true)
	void severeConnection(@Param("userId") Long userId, @Param("mateId") Long mateId);

}
