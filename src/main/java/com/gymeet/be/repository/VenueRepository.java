package com.gymeet.be.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gymeet.be.model.Venue;

@Repository
public interface VenueRepository extends JpaRepository<Venue, Long> {

	List<Venue> findByNameContainingIgnoreCase(String partialName);

	Optional<Venue> findByNameIgnoreCase(String name);

}
