package com.gymeet.be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.model.RelationRequester;
import com.gymeet.be.model.User;

@Repository
public interface RelationRequesterRepository extends JpaRepository<RelationRequester, Long> {

	List<RelationRequester> findByRequester(User requester);

	List<RelationRequester> findByRequested(User requested);

	RelationRequester findByRequesterAndRequested(User requester, User requested);

	Boolean existsByRequesterAndRequested(User requester, User requested);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM olympesk_relation_requester " + "WHERE requester_olympesk_user_id_ref = :userId "
			+ "AND requested_olympesk_user_id_ref = :mateId " + "OR requester_olympesk_user_id_ref = :mateId "
			+ "AND requested_olympesk_user_id_ref = :userId", nativeQuery = true)
	void severeConnection(@Param("userId") Long userId, @Param("mateId") Long mateId);

}
