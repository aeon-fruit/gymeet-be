package com.gymeet.be.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gymeet.be.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByEmail(String email);

	Optional<User> findByPhoneNumber(String phoneNumber);

	Optional<User> findByEmailOrPhoneNumber(String email, String phoneNumber);

	Optional<User> findByUsername(String username);

	List<User> findByCityAndCountry(String city, String country);

	List<User> findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase(String partialFirstName,
			String partialSurname);

	List<User> findByHomeCountry(String homeCountry);

	Boolean existsByEmail(String email);

	Boolean existsByPhoneNumberNotNullAndPhoneNumber(String phoneNumber);

	Boolean existsByEmailOrPhoneNumberNotNullAndPhoneNumber(String email, String phoneNumber);

	Boolean existsByUsername(String uriSuffix);

}
