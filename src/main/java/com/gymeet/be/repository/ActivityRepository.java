package com.gymeet.be.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gymeet.be.model.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

	List<Activity> findByNameContainingIgnoreCase(String partialName);

	Optional<Activity> findByNameIgnoreCase(String name);

}
