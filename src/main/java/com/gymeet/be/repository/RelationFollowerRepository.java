package com.gymeet.be.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.model.RelationFollower;
import com.gymeet.be.model.User;

@Repository
public interface RelationFollowerRepository extends JpaRepository<RelationFollower, Long> {

	List<RelationFollower> findByFollower(User follower);

	List<RelationFollower> findByFollowed(User followed);

	RelationFollower findByFollowerAndFollowed(User follower, User followed);

	Boolean existsByFollowerAndFollowed(User follower, User followed);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM olympesk_relation_follower " + "WHERE follower_olympesk_user_id_ref = :userId "
			+ "AND followed_olympesk_user_id_ref = :mateId " + "OR follower_olympesk_user_id_ref = :mateId "
			+ "AND followed_olympesk_user_id_ref = :userId", nativeQuery = true)
	void severeConnection(@Param("userId") Long userId, @Param("mateId") Long mateId);

}
