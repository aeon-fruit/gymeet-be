package com.gymeet.be.controller;

import java.io.IOException;
import java.net.URI;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ActivityDetailedInfo;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ActivityPostRequest;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.service.ActivityService;

@RestController
@RequestMapping("/api/activities")
public class ActivityController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ActivityController.class);

	private final ActivityService activityService;
	private final ServletContext servletContext;

	@Autowired
	public ActivityController(final ActivityService activityService, final ServletContext servletContext) {
		this.activityService = activityService;
		this.servletContext = servletContext;
	}

	@GetMapping("/")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<ActivityInfo>> findAllActivities() throws ModelMappingException {
		return ResponseEntity.ok(activityService.findAllActivities());
	}

	@GetMapping("/search")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<ActivityInfo>> findActivityByPartialName(@RequestParam(value = "q") String name)
			throws ResourceNotFoundException, ModelMappingException {
		// TODO - more elaborate search
		return ResponseEntity.ok(activityService.findActivitiesByPartialName(name));
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ActivityDetailedInfo> findActivityById(@PathVariable(value = "id") Long id)
			throws ResourceNotFoundException, ModelMappingException {
		final ActivityDetailedInfo activityDetailedInfo = activityService.findActivityById(id);
		return ResponseEntity.ok(activityDetailedInfo);
	}

	@PostMapping("/")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ActivityDetailedInfo> addActivity(
			@RequestPart("data") @Valid ActivityPostRequest activityPostRequest,
			@RequestPart("file") MultipartFile icon)
			throws ResourceConflictException, ModelMappingException, UnknownFileTypeException, IOException {
		final ActivityDetailedInfo activityDetailedInfo = activityService.addActivity(activityPostRequest, icon);

		final URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/api/activities/" + activityDetailedInfo.getId()).build().toUri();

		return ResponseEntity.created(location).body(activityDetailedInfo);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ActivityDetailedInfo> updateActivityDetails(@PathVariable(value = "id") Long id,
			@RequestBody @Valid ActivityPostRequest activityPostRequest)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException {
		final ActivityDetailedInfo activityDetailedInfo = activityService.updateActivityDetails(id,
				activityPostRequest);
		return ResponseEntity.ok(activityDetailedInfo);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Void> removeActivityById(@PathVariable(value = "id") Long id)
			throws ResourceNotFoundException, IOException {
		activityService.removeActivityById(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{id}/icon")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Resource> getIcon(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		final Resource icon = activityService.getIcon(id);

		if (icon != null) {
			String contentType;
			try {
				contentType = servletContext.getMimeType(icon.getFile().getAbsolutePath());
			} catch (Exception e) {
				contentType = "application/octet-stream";
			}
			String headerValue = "attachment; filename=\"" + icon.getFilename() + "\"";
			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, headerValue).body(icon);
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@PutMapping("/{id}/icon")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Void> updateActivityIcon(@PathVariable(value = "id") Long id,
			@RequestParam("file") MultipartFile icon)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException {
		activityService.updateActivityIcon(id, icon);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{id}/venues")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<VenueDetailedInfo>> findVenuesByActivityId(@PathVariable(value = "id") Long id)
			throws ResourceNotFoundException, ModelMappingException {
		final Set<VenueDetailedInfo> venueDetailedInfoSet = activityService.findVenuesByActivityId(id);
		return ResponseEntity.ok(venueDetailedInfoSet);
	}

}