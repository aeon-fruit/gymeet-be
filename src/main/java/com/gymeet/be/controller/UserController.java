package com.gymeet.be.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.validation.constraints.NotBlank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriUtils;

import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ProfileDetailedInfo;
import com.gymeet.be.payload.ProfileField;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.UserIdentityAvailability;
import com.gymeet.be.security.CurrentUser;
import com.gymeet.be.security.UserDetailsImpl;
import com.gymeet.be.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	private final UserService userService;
	private final ServletContext servletContext;

	@Autowired
	public UserController(final UserService userService, final ServletContext servletContext) {
		this.userService = userService;
		this.servletContext = servletContext;
	}

	@GetMapping("/availability/email")
	public ResponseEntity<UserIdentityAvailability> checkEmailAvailability(
			@RequestParam(value = "em") @NotBlank String email) {
		return ResponseEntity.ok(userService.checkEmailAvailability(UriUtils.decode(email, StandardCharsets.UTF_8)));
	}

	@GetMapping("/availability/phone-number")
	public ResponseEntity<UserIdentityAvailability> checkPhoneNumberAvailability(
			@RequestParam(value = "pn") @NotBlank String phoneNumber) {
		return ResponseEntity
				.ok(userService.checkPhoneNumberAvailability(UriUtils.decode(phoneNumber, StandardCharsets.UTF_8)));
	}

	@GetMapping("/availability/username")
	public ResponseEntity<UserIdentityAvailability> checkUsernameAvailability(
			@RequestParam(value = "un") @NotBlank String username) {
		return ResponseEntity
				.ok(userService.checkUsernameAvailability(UriUtils.decode(username, StandardCharsets.UTF_8)));
	}

	@GetMapping("/me")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ProfileInfo> getCurrentUserMinimalProfile(@CurrentUser UserDetailsImpl currentUser)
			throws ResourceNotFoundException, ModelMappingException {
		final ProfileInfo profileInfo = userService.getCurrentUserMinimalProfile(currentUser);
		return ResponseEntity.ok(profileInfo);
	}

	@GetMapping("/{username:[a-zA-Z0-9\\-_.]+}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ProfileInfo> getUserMinimalProfile(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "username") @NotBlank String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException, ResourceBadRequestException {
		final ProfileInfo profileInfo = userService.getUserMinimalProfile(currentUser, username);
		return ResponseEntity.ok(profileInfo);
	}

	@GetMapping("/{username:[a-zA-Z0-9\\-_.]+}/about")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ProfileDetailedInfo> getUserDetailedProfile(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "username") @NotBlank String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException, ResourceBadRequestException {
		final ProfileDetailedInfo profileDetailedInfo = userService.getUserDetailedProfile(currentUser, username);
		return ResponseEntity.ok(profileDetailedInfo);
	}

	@PutMapping("/me/about")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ProfileField> updateProfileField(@CurrentUser UserDetailsImpl currentUser,
			@RequestParam(value = "field") @NotBlank String fieldName, @RequestBody ProfileField fieldValue)
			throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException {
		final ProfileField profileField = userService.updateProfileField(currentUser, fieldValue, fieldName);
		return ResponseEntity.ok(profileField);
	}

	@GetMapping("/{username:[a-zA-Z0-9\\-_.]+}/picture")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Resource> getPicture(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "username") @NotBlank String username, @RequestParam(value = "sz") int size)
			throws ResourceNotFoundException, BadRequestException {
		final Resource picture = userService.getPicture(currentUser, username, size);

		if (picture != null) {
			String contentType;
			try {
				contentType = servletContext.getMimeType(picture.getFile().getAbsolutePath());
			} catch (Exception e) {
				contentType = "application/octet-stream";
			}

			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + picture.getFilename() + "\"")
					.body(picture);
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@PutMapping("/me/picture")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Void> updatePicture(@CurrentUser UserDetailsImpl currentUser,
			@RequestParam("file") MultipartFile picture)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException {
		userService.updatePicture(currentUser, picture);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/me/picture")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Void> deletePicture(@CurrentUser UserDetailsImpl currentUser)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException {
		userService.updatePicture(currentUser, null);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{username:[a-zA-Z0-9\\-_.]+}/activities")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<ActivityInfo>> getUserActivities(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "username") @NotBlank String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException {
		final Set<ActivityInfo> activityInfoSet = userService.getUserActivities(currentUser, username);
		return ResponseEntity.ok(activityInfoSet);
	}

	@PostMapping("/me/activities/{activityId}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ActivityInfo> addActivity(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "activityId") Long activityId)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException {
		final ActivityInfo activityInfo = userService.addActivity(currentUser, activityId);
		return ResponseEntity.ok(activityInfo);
	}

	@DeleteMapping("/me/activities/{activityId}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Void> removeActivity(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "activityId") Long activityId) throws ResourceNotFoundException {
		userService.removeActivity(currentUser, activityId);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/search")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<ProfileInfo>> findByName(@CurrentUser UserDetailsImpl currentUser,
			@RequestParam(value = "q") @NotBlank String name)
			throws ModelMappingException, ResourceNotFoundException, BadRequestException, ResourceBadRequestException {
		// TODO - more elaborate search
		final Set<ProfileInfo> profileInfoSet = userService.findByName(currentUser, name);
		return ResponseEntity.ok(profileInfoSet);
	}

}