package com.gymeet.be.controller;

import java.net.URI;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gymeet.be.exception.AppException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.SignUpPostRequest;
import com.gymeet.be.service.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

	private final AuthService authService;

	@Autowired
	public AuthController(final AuthService authService) {
		this.authService = authService;
	}

	@PostMapping("/signin")
	public ResponseEntity<JwtAuthenticationResponse> authenticateUser(
			@RequestBody @Valid LoginPostRequest loginPostRequest) {
		final JwtAuthenticationResponse jwtAuthenticationResponse = authService.authenticateUser(loginPostRequest);
		return ResponseEntity.ok(jwtAuthenticationResponse);
	}

	@PostMapping("/signup")
	public ResponseEntity<ProfileInfo> registerUser(@RequestBody @Valid SignUpPostRequest signUpPostRequest)
			throws ResourceConflictException, ResourceBadRequestException, ModelMappingException, AppException {
		final ProfileInfo profileInfo = authService.registerUser(signUpPostRequest);

		final URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/me").build().toUri();

		return ResponseEntity.created(location).body(profileInfo);
	}

}
