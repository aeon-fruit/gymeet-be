package com.gymeet.be.controller;

import java.io.IOException;
import java.net.URI;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.payload.VenueInfo;
import com.gymeet.be.payload.VenuePostRequest;
import com.gymeet.be.service.VenueService;

@RestController
@RequestMapping("/api/venues")
public class VenueController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(VenueController.class);

	private final VenueService venueService;
	private final ServletContext servletContext;

	@Autowired
	public VenueController(final VenueService venueService, final ServletContext servletContext) {
		this.venueService = venueService;
		this.servletContext = servletContext;
	}

	@GetMapping("/search")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<VenueInfo>> findVenuesByPartialName(@RequestParam(value = "q") @NotBlank String name)
			throws ResourceNotFoundException, ModelMappingException {
		// TODO - more elaborate search
		return ResponseEntity.ok(venueService.findVenuesByPartialName(name));
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<VenueDetailedInfo> findVenueById(@PathVariable(value = "id") Long id)
			throws ResourceNotFoundException, ModelMappingException {
		final VenueDetailedInfo venueDetailedInfo = venueService.findVenueById(id);
		return ResponseEntity.ok(venueDetailedInfo);
	}

	@PostMapping("/")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<VenueDetailedInfo> addVenue(@RequestPart("data") @Valid VenuePostRequest venuePostRequest,
			@RequestPart("file") MultipartFile picture) throws ResourceConflictException, ResourceNotFoundException,
			ModelMappingException, UnknownFileTypeException, IOException {
		final VenueDetailedInfo venueDetailedInfo = venueService.addVenue(venuePostRequest, picture);

		final URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/api/venues/" + venueDetailedInfo.getId()).build().toUri();

		return ResponseEntity.created(location).body(venueDetailedInfo);
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<VenueDetailedInfo> updateVenueDetails(@PathVariable(value = "id") Long id,
			@RequestBody @Valid VenuePostRequest venuePostRequest)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException {
		final VenueDetailedInfo venueDetailedInfo = venueService.updateVenueDetails(id, venuePostRequest);
		return ResponseEntity.ok(venueDetailedInfo);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Void> removeVenueById(@PathVariable(value = "id") Long id)
			throws ResourceNotFoundException, IOException {
		venueService.removeVenueById(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{id}/picture")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Resource> getPicture(@PathVariable(value = "id") Long id,
			@RequestParam(value = "sz") int size) throws ResourceNotFoundException {
		final Resource picture = venueService.getPicture(id, size);

		if (picture != null) {
			String contentType = "application/octet-stream";
			try {
				contentType = servletContext.getMimeType(picture.getFile().getAbsolutePath());
			} catch (Exception ignored) {
			}

			return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + picture.getFilename() + "\"")
					.body(picture);
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@PutMapping("/{id}/picture")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Void> updateVenuePicture(@PathVariable(value = "id") Long id,
			@RequestParam("file") MultipartFile picture)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException {
		venueService.updateVenuePicture(id, picture);
		return ResponseEntity.noContent().build();
	}

}