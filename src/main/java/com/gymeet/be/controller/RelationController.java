package com.gymeet.be.controller;

import java.util.Set;

import javax.validation.constraints.NotBlank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.ResourceUnauthorizedException;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.ProfileRelationInfo;
import com.gymeet.be.security.CurrentUser;
import com.gymeet.be.security.UserDetailsImpl;
import com.gymeet.be.service.RelationService;

@RestController
@RequestMapping("/api/users")
public class RelationController {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(RelationController.class);

	private final RelationService relationService;

	@Autowired
	public RelationController(final RelationService relationService) {
		this.relationService = relationService;
	}

	@GetMapping("/me/relations")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<ProfileInfo>> getCurrentUserRelationList(@CurrentUser UserDetailsImpl currentUser,
			@RequestParam(value = "rt") @NotBlank String relationType)
			throws ResourceUnauthorizedException, ResourceNotFoundException, ModelMappingException {
		final Set<ProfileInfo> relationList = relationService.getCurrentUserRelationList(currentUser, relationType);
		return ResponseEntity.ok(relationList);
	}

	@GetMapping("/{username:[a-zA-Z0-9\\-_.]+}/relations")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Set<ProfileInfo>> getRelationList(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "username") @NotBlank String username,
			@RequestParam(value = "rt") @NotBlank String relationType) throws BadRequestException,
			ModelMappingException, ResourceNotFoundException, ResourceUnauthorizedException {
		final Set<ProfileInfo> relationList = relationService.getRelationList(currentUser, username, relationType);
		return ResponseEntity.ok(relationList);
	}

	@PostMapping("/me/relations/{username:[a-zA-Z0-9\\-_.]+}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ProfileRelationInfo> addRelation(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "username") @NotBlank String username,
			@RequestParam(value = "rt") @NotBlank String relationType)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceConflictException, ResourceBadRequestException {
		ProfileRelationInfo relationInfo = relationService.addRelation(currentUser, username, relationType);
		return ResponseEntity.ok(relationInfo);
	}

	@DeleteMapping("/me/relations/{username:[a-zA-Z0-9\\-_.]+}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<ProfileRelationInfo> removeRelation(@CurrentUser UserDetailsImpl currentUser,
			@PathVariable(value = "username") @NotBlank String username,
			@RequestParam(value = "rt") @NotBlank String relationType) throws ResourceUnauthorizedException,
			BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		ProfileRelationInfo relationInfo = relationService.removeRelation(currentUser, username, relationType);
		return ResponseEntity.ok(relationInfo);
	}

}