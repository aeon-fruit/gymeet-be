package com.gymeet.be.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.model.User;
import com.gymeet.be.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	final private UserRepository userRepository;

	@Autowired
	public UserDetailsServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String emailOrPhoneNumber) throws UsernameNotFoundException {
		// Let people login with either email or phone number
		final User user = userRepository.findByEmailOrPhoneNumber(emailOrPhoneNumber, emailOrPhoneNumber)
				.orElseThrow(() -> new UsernameNotFoundException(
						"User not found with email or phone number : " + emailOrPhoneNumber));

		return UserDetailsImpl.create(user);
	}

	// This method is used by JWTAuthenticationFilter
	@Transactional
	public UserDetails loadUserById(Long id) {
		final User user = userRepository.findById(id)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + id));

		return UserDetailsImpl.create(user);
	}

}
