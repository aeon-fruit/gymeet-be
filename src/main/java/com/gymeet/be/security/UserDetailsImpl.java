package com.gymeet.be.security;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gymeet.be.model.User;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(exclude = "password")
public class UserDetailsImpl implements UserDetails {

	private static final long serialVersionUID = 5127198797001292105L;

	@NotNull
	private Long id;

	@NotBlank
	private String firstName;

	@NotBlank
	private String surname;

	@NotBlank
	private String username;

	// email or phone number
	@NotBlank
	@JsonIgnore
	private String login;

	@NotBlank
	@JsonIgnore
	private String password;

	private Collection<? extends GrantedAuthority> authorities;

	private UserDetailsImpl(Long id, String firstName, String surname, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.firstName = firstName;
		this.surname = surname;
		this.username = username;
		this.login = email;
		this.password = password;
		this.authorities = authorities;
	}

	static UserDetailsImpl create(User user) {
		final List<GrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());

		return new UserDetailsImpl(user.getId(), user.getFirstName(), user.getSurname(), user.getUsername(),
				user.getEmail(), user.getPassword(), authorities);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl other = (UserDetailsImpl) o;
		return Objects.equals(id, other.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

}
