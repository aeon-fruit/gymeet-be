package com.gymeet.be.service;

import java.io.IOException;
import java.util.Set;

import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ProfileDetailedInfo;
import com.gymeet.be.payload.ProfileField;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.UserIdentityAvailability;
import com.gymeet.be.security.UserDetailsImpl;

public interface UserService {

	@Transactional
	UserIdentityAvailability checkEmailAvailability(String email);

	@Transactional
	UserIdentityAvailability checkPhoneNumberAvailability(String phoneNumber);

	@Transactional
	UserIdentityAvailability checkUsernameAvailability(String username);

	@Transactional
	ProfileInfo getCurrentUserMinimalProfile(UserDetailsImpl currentUser)
			throws ResourceNotFoundException, ModelMappingException;

	@Transactional
	ProfileInfo getUserMinimalProfile(UserDetailsImpl currentUser, String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException, ResourceBadRequestException;

	@Transactional
	ProfileDetailedInfo getUserDetailedProfile(UserDetailsImpl currentUser, String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException, ResourceBadRequestException;

	@Transactional
	ProfileField updateProfileField(UserDetailsImpl currentUser, ProfileField rawValue, String fieldName)
			throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException;

	@Transactional
	Resource getPicture(UserDetailsImpl currentUser, String username, int size)
			throws ResourceNotFoundException, BadRequestException;

	@Transactional
	void updatePicture(UserDetailsImpl currentUser, MultipartFile picture)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException;

	@Transactional
	Set<ActivityInfo> getUserActivities(UserDetailsImpl currentUser, String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException;

	@Transactional
	ActivityInfo addActivity(UserDetailsImpl currentUser, Long activityId)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException;

	@Transactional
	void removeActivity(UserDetailsImpl currentUser, Long activityId) throws ResourceNotFoundException;

	@Transactional
	Set<ProfileInfo> findByName(UserDetailsImpl currentUser, String name)
			throws ModelMappingException, ResourceNotFoundException, BadRequestException, ResourceBadRequestException;

}
