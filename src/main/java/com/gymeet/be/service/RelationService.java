package com.gymeet.be.service;

import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.ResourceUnauthorizedException;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.ProfileRelationInfo;
import com.gymeet.be.payload.RelationProperty;
import com.gymeet.be.security.UserDetailsImpl;
import com.gymeet.be.util.RelationType;

public interface RelationService {

	@Transactional
	Set<ProfileInfo> getCurrentUserRelationList(UserDetailsImpl currentUser, String relationType)
			throws ResourceNotFoundException, ModelMappingException, ResourceUnauthorizedException;

	@Transactional
	Set<ProfileInfo> getRelationList(UserDetailsImpl currentUser, String username, String relationType)
			throws BadRequestException, ResourceNotFoundException, ModelMappingException, ResourceUnauthorizedException;

	@Transactional
	ProfileRelationInfo getRelationInfo(UserDetailsImpl currentUser, String username, RelationType... relationTypes)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException;

	@Transactional
	RelationProperty isBlocked(UserDetailsImpl currentUser, String blockingUsername)
			throws BadRequestException, ResourceNotFoundException;

	@Transactional
	ProfileRelationInfo addRelation(UserDetailsImpl currentUser, String username, String relationType)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceConflictException, ResourceBadRequestException;

	@Transactional
	ProfileRelationInfo removeRelation(UserDetailsImpl currentUser, String username, String relationType)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceBadRequestException;

}
