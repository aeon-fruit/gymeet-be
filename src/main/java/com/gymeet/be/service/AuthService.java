package com.gymeet.be.service;

import javax.validation.Valid;

import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.exception.AppException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.SignUpPostRequest;

public interface AuthService {

	@Transactional
	JwtAuthenticationResponse authenticateUser(@Valid LoginPostRequest loginPostRequest);

	@Transactional
	ProfileInfo registerUser(@Valid SignUpPostRequest signUpPostRequest)
			throws ResourceConflictException, ResourceBadRequestException, ModelMappingException, AppException;

}
