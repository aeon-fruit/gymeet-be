package com.gymeet.be.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gymeet.be.config.FileStorageProperties;
import com.gymeet.be.exception.FileStorageException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.model.Activity;
import com.gymeet.be.model.Venue;
import com.gymeet.be.payload.ActivityDetailedInfo;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ActivityPostRequest;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.repository.ActivityRepository;
import com.gymeet.be.service.ActivityService;
import com.gymeet.be.util.DtoUtils;
import com.gymeet.be.util.ImageStorageConfig;
import com.gymeet.be.util.ImageStorageUtils;

@Service
public class ActivityServiceImpl implements ActivityService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ActivityServiceImpl.class);

	private final ActivityRepository activityRepository;
	private final Path iconLocation;
	private final int width;
	private final int height;

	@Autowired
	public ActivityServiceImpl(final ActivityRepository activityRepository,
			final FileStorageProperties fileStorageProperties) {
		this.activityRepository = activityRepository;

		String fileIconLocationDir = fileStorageProperties.getActivity().getIcon().getDir();

		width = fileStorageProperties.getActivity().getIcon().getWidth();
		height = fileStorageProperties.getActivity().getIcon().getHeight();

		this.iconLocation = Paths.get(fileIconLocationDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.iconLocation);
		} catch (Exception e) {
			throw new FileStorageException(
					"Could not create the directory where the uploaded original files will be stored.", e);
		}
	}

	@Override
	@Transactional
	public Set<ActivityInfo> findAllActivities() throws ModelMappingException {
		final List<Activity> activities = activityRepository.findAll();
		final Set<ActivityInfo> activityInfoSet = new HashSet<>();
		for (Activity activity : activities) {
			activityInfoSet.add(DtoUtils.entityToDto(activity, ActivityInfo.class));
		}
		return activityInfoSet;
	}

	@Override
	@Transactional
	public Set<ActivityInfo> findActivitiesByPartialName(String partialName)
			throws ResourceNotFoundException, ModelMappingException {
		final List<Activity> activities = activityRepository.findByNameContainingIgnoreCase(partialName);
		if (activities.isEmpty()) {
			throw new ResourceNotFoundException("Activity", "partial name", partialName);
		}
		final Set<ActivityInfo> activityInfoSet = new HashSet<>();
		for (Activity activity : activities) {
			activityInfoSet.add(DtoUtils.entityToDto(activity, ActivityInfo.class));
		}
		return activityInfoSet;
	}

	@Override
	@Transactional
	public ActivityDetailedInfo findActivityById(Long id) throws ResourceNotFoundException, ModelMappingException {
		final Activity activity = activityRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Activity", "id", id));
		return DtoUtils.entityToDto(activity, ActivityDetailedInfo.class);
	}

	@Override
	@Transactional
	public ActivityDetailedInfo addActivity(ActivityPostRequest activityPostRequest, MultipartFile icon)
			throws ResourceConflictException, ModelMappingException, UnknownFileTypeException, IOException {
		if (activityRepository.findByNameIgnoreCase(activityPostRequest.getName()).orElse(null) != null) {
			throw new ResourceConflictException("Activity", "name", activityPostRequest.getName());
		}
		final Activity activity = DtoUtils.dtoToEntity(activityPostRequest, Activity.class);
		String savedPicture = null;
		if (icon != null) {
			savedPicture = ImageStorageUtils.storePicture(icon,
					new ImageStorageConfig(this.iconLocation, width, height));
		}
		activity.setIcon(savedPicture);
		activityRepository.saveAndFlush(activity);

		return DtoUtils.entityToDto(activity, ActivityDetailedInfo.class);
	}

	@Override
	@Transactional
	public ActivityDetailedInfo updateActivityDetails(Long id, ActivityPostRequest activityPostRequest)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException {
		final Activity activity = activityRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Activity", "id", id));
		Activity duplicate = activityRepository.findByNameIgnoreCase(activityPostRequest.getName()).orElse(null);
		if ((duplicate != null) && !duplicate.getId().equals(id)) {
			throw new ResourceConflictException("Activity", "name", activityPostRequest.getName());
		}
		Activity activityUpdater = DtoUtils.dtoToEntity(activityPostRequest, Activity.class);

		activity.setName(activityUpdater.getName());
		activity.setDescription(activityUpdater.getDescription());
		activityRepository.saveAndFlush(activity);

		return DtoUtils.entityToDto(activity, ActivityDetailedInfo.class);
	}

	@Override
	@Transactional
	public void removeActivityById(Long id) throws ResourceNotFoundException, IOException {
		final Activity activity = activityRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Activity", "id", id));
		String icon = activity.getIcon();
		activityRepository.delete(activity);
		ImageStorageUtils.deletePicture(icon, this.iconLocation);
	}

	@Override
	@Transactional
	public Resource getIcon(Long id) throws ResourceNotFoundException {
		final Activity activity = activityRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Activity", "id", id));

		String filename = activity.getIcon();
		try {
			if (filename == null) {
				// TODO - return default picture
				return null;
			}
			Path filePath = iconLocation.resolve(filename).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new ResourceNotFoundException("Activity icon", "filename", filename);
			}
		} catch (MalformedURLException e) {
			throw new ResourceNotFoundException("Activity icon", "path", filename);
		}
	}

	@Override
	@Transactional
	public void updateActivityIcon(Long id, MultipartFile icon)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException {
		final Activity activity = activityRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Activity", "id", id));

		String savedPicture = null;
		if (icon != null) {
			savedPicture = ImageStorageUtils.storePicture(icon,
					new ImageStorageConfig(this.iconLocation, width, height));
		}

		try {
			ImageStorageUtils.deletePicture(activity.getIcon(), this.iconLocation);
		} finally {
			activity.setIcon(savedPicture);
			activityRepository.saveAndFlush(activity);
		}
	}

	@Override
	@Transactional
	public Set<VenueDetailedInfo> findVenuesByActivityId(Long id)
			throws ResourceNotFoundException, ModelMappingException {
		final Activity activity = activityRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Activity", "id", id));

		final Set<Venue> venues = activity.getVenues();
		final Set<VenueDetailedInfo> venueDetailedInfoSet = new HashSet<>();
		for (Venue venue : venues) {
			VenueDetailedInfo venueDetailedInfo = DtoUtils.entityToDto(venue, VenueDetailedInfo.class);
			if (venueDetailedInfo != null) {
				final Set<Activity> activities = venue.getActivities();
				final Set<ActivityInfo> activityInfoHashSet = new HashSet<>();
				for (Activity venueActivity : activities) {
					activityInfoHashSet.add(DtoUtils.entityToDto(venueActivity, ActivityInfo.class));
				}
				venueDetailedInfo.setActivityInfoSet(activityInfoHashSet);
				venueDetailedInfoSet.add(venueDetailedInfo);
			}
		}
		return venueDetailedInfoSet;
	}

}
