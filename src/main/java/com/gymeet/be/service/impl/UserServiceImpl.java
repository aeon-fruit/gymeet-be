package com.gymeet.be.service.impl;

import static com.gymeet.be.util.ProfileFieldName.BIRTH_DATE;
import static com.gymeet.be.util.ProfileFieldName.CITY;
import static com.gymeet.be.util.ProfileFieldName.COUNTRY;
import static com.gymeet.be.util.ProfileFieldName.EMAIL;
import static com.gymeet.be.util.ProfileFieldName.FIRST_NAME;
import static com.gymeet.be.util.ProfileFieldName.GENDER;
import static com.gymeet.be.util.ProfileFieldName.HOME_COUNTRY;
import static com.gymeet.be.util.ProfileFieldName.PASSWORD;
import static com.gymeet.be.util.ProfileFieldName.PHONE_NUMBER;
import static com.gymeet.be.util.ProfileFieldName.SURNAME;
import static com.gymeet.be.util.ProfileFieldName.USERNAME;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gymeet.be.config.FileStorageProperties;
import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.FileStorageException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.model.Activity;
import com.gymeet.be.model.User;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ProfileDetailedInfo;
import com.gymeet.be.payload.ProfileField;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.UserIdentityAvailability;
import com.gymeet.be.repository.ActivityRepository;
import com.gymeet.be.repository.UserRepository;
import com.gymeet.be.security.UserDetailsImpl;
import com.gymeet.be.service.RelationService;
import com.gymeet.be.service.UserService;
import com.gymeet.be.util.DtoUtils;
import com.gymeet.be.util.ImageStorageConfig;
import com.gymeet.be.util.ImageStorageUtils;
import com.gymeet.be.util.ProfileFieldName;
import com.gymeet.be.util.RelationType;

@Service
public class UserServiceImpl implements UserService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	private final UserRepository userRepository;
	private final ActivityRepository activityRepository;
	private final RelationService relationService;
	private final PasswordEncoder passwordEncoder;
	private final Path pictureOriginalLocation;
	private final Path pictureMediumLocation;
	private final Path pictureThumbnailLocation;
	private final int mediumWidth;
	private final int mediumHeight;
	private final int thumbnailWidth;
	private final int thumbnailHeight;

	@Autowired
	public UserServiceImpl(final UserRepository userRepository, final ActivityRepository activityRepository,
			final RelationService relationService, final PasswordEncoder passwordEncoder,
			final FileStorageProperties fileStorageProperties) {
		this.userRepository = userRepository;
		this.activityRepository = activityRepository;
		this.relationService = relationService;
		this.passwordEncoder = passwordEncoder;

		String filePictureOriginalDir = fileStorageProperties.getUser().getPicture().getOriginal().getDir();
		String filePictureMediumDir = fileStorageProperties.getUser().getPicture().getMedium().getDir();
		String filePictureThumbnailDir = fileStorageProperties.getUser().getPicture().getThumbnail().getDir();

		mediumWidth = fileStorageProperties.getUser().getPicture().getMedium().getWidth();
		mediumHeight = fileStorageProperties.getUser().getPicture().getMedium().getHeight();
		thumbnailWidth = fileStorageProperties.getUser().getPicture().getThumbnail().getWidth();
		thumbnailHeight = fileStorageProperties.getUser().getPicture().getThumbnail().getHeight();

		this.pictureOriginalLocation = Paths.get(filePictureOriginalDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.pictureOriginalLocation);
		} catch (Exception e) {
			throw new FileStorageException(
					"Could not create the directory where the uploaded original files will be stored.", e);
		}

		this.pictureMediumLocation = Paths.get(filePictureMediumDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.pictureMediumLocation);
		} catch (Exception e) {
			throw new FileStorageException(
					"Could not create the directory where the uploaded medium-size files will be stored.", e);
		}

		this.pictureThumbnailLocation = Paths.get(filePictureThumbnailDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.pictureThumbnailLocation);
		} catch (Exception e) {
			throw new FileStorageException(
					"Could not create the directory where the uploaded thumbnail-size files will be stored.", e);
		}
	}

	@Override
	@Transactional
	public UserIdentityAvailability checkEmailAvailability(String email) {
		final Boolean isAvailable = !userRepository.existsByEmail(email);
		return new UserIdentityAvailability(isAvailable);
	}

	@Override
	@Transactional
	public UserIdentityAvailability checkPhoneNumberAvailability(String phoneNumber) {
		final Boolean isAvailable = !userRepository.existsByPhoneNumberNotNullAndPhoneNumber(phoneNumber);
		return new UserIdentityAvailability(isAvailable);
	}

	@Override
	@Transactional
	public UserIdentityAvailability checkUsernameAvailability(String username) {
		final Boolean isAvailable = !userRepository.existsByUsername(username);
		return new UserIdentityAvailability(isAvailable);
	}

	@Override
	@Transactional
	public ProfileInfo getCurrentUserMinimalProfile(UserDetailsImpl currentUser)
			throws ResourceNotFoundException, ModelMappingException {
		final User user = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		return DtoUtils.entityToDto(user, ProfileInfo.class);
	}

	@Override
	@Transactional
	public ProfileInfo getUserMinimalProfile(UserDetailsImpl currentUser, String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException, ResourceBadRequestException {
		final User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		boolean self = currentUser.getUsername().equals(username);

		if (!currentUser.getUsername().equals(username)
				&& relationService.isBlocked(currentUser, username).getHaveRelation()) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		ProfileInfo profileInfo = DtoUtils.entityToDto(user, ProfileInfo.class);
		if (!self) {
			profileInfo.setRelationInfo(relationService.getRelationInfo(currentUser, username, RelationType.values()));
		}
		return profileInfo;
	}

	@Override
	@Transactional
	public ProfileDetailedInfo getUserDetailedProfile(UserDetailsImpl currentUser, String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException, ResourceBadRequestException {
		final User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		boolean self = currentUser.getUsername().equals(username);

		if (!self && relationService.isBlocked(currentUser, username).getHaveRelation()) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		ProfileDetailedInfo profileDetailedInfo = DtoUtils.entityToDto(user, ProfileDetailedInfo.class);
		if (!self) {
			profileDetailedInfo
					.setRelationInfo(relationService.getRelationInfo(currentUser, username, RelationType.values()));
		}
		return profileDetailedInfo;
	}

	@Override
	@Transactional
	public ProfileField updateProfileField(UserDetailsImpl currentUser, ProfileField rawValue, String fieldName)
			throws ResourceNotFoundException, ResourceConflictException, ResourceBadRequestException {
		if (rawValue == null) {
			throw new ResourceBadRequestException("Profile field", "value", null);
		}

		User user = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		// TODO make an exception for PASSWORD
		ProfileFieldName updatableFields[] = { EMAIL, PASSWORD, FIRST_NAME, SURNAME, PHONE_NUMBER, USERNAME,
				HOME_COUNTRY, COUNTRY, CITY, BIRTH_DATE, GENDER };
		ProfileFieldName profileFieldName = null;
		for (ProfileFieldName updatable : updatableFields) {
			if (updatable.toString().equalsIgnoreCase(fieldName)) {
				profileFieldName = updatable;
				break;
			}
		}
		if (profileFieldName == null) {
			throw new ResourceNotFoundException("Profile field", "name", fieldName);
		}

		switch (profileFieldName) {
		case EMAIL:
		case FIRST_NAME:
		case SURNAME:
		case PHONE_NUMBER:
		case USERNAME:
		case HOME_COUNTRY:
		case COUNTRY:
		case CITY:
		case PASSWORD:
			String strValue = null;
			if (rawValue.getValue() != null) {
				strValue = rawValue.getValue().toString();
			}
			return update(user, profileFieldName, strValue);
		case BIRTH_DATE:
			Date dateValue = null;
			if (rawValue.getValue() != null) {
				try {
					dateValue = (Date) rawValue.getValue();
				} catch (ClassCastException e) {
					throw new ResourceBadRequestException(e, "User", "birth date", rawValue.getValue());
				}
			}
			return updateBirthDate(user, dateValue);
		case GENDER:
			Boolean booleanValue = null;
			if (rawValue.getValue() != null) {
				try {
					booleanValue = (Boolean) rawValue.getValue();
				} catch (ClassCastException e) {
					throw new ResourceBadRequestException(e, "User", "gender", rawValue.getValue());
				}
			}
			return updateGender(user, booleanValue);
		}

		throw new ResourceNotFoundException("Profile field", "name", fieldName);
	}

	@Override
	@Transactional
	public Resource getPicture(UserDetailsImpl currentUser, String username, int size)
			throws ResourceNotFoundException, BadRequestException {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		if (!currentUser.getUsername().equals(username)
				&& relationService.isBlocked(currentUser, username).getHaveRelation()) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		Path location;
		String filename;
		switch (size) {
		case 0:
			location = pictureOriginalLocation;
			filename = user.getLargePicture();
			break;
		case 1:
			location = pictureMediumLocation;
			filename = user.getProfilePicture();
			break;
		case 2:
			location = pictureThumbnailLocation;
			filename = user.getThumbnailPicture();
			break;
		default:
			throw new ResourceNotFoundException("User picture", "size", size);
		}
		try {
			if (filename == null) {
				// TODO - return default picture
				return null;
			}
			Path filePath = location.resolve(filename).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new ResourceNotFoundException("User picture", "filename", filename);
			}
		} catch (MalformedURLException e) {
			throw new ResourceNotFoundException("User picture", "path", filename);
		}
	}

	@Override
	@Transactional
	public void updatePicture(UserDetailsImpl currentUser, MultipartFile picture)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException {
		User user = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		String savedPicture = null;
		if (picture != null) {
			savedPicture = ImageStorageUtils.storePicture(picture,
					new ImageStorageConfig(this.pictureOriginalLocation, 0, 0),
					new ImageStorageConfig(this.pictureMediumLocation, mediumWidth, mediumHeight),
					new ImageStorageConfig(this.pictureThumbnailLocation, thumbnailWidth, thumbnailHeight));
		}
		ImageStorageUtils.deletePicture(user.getLargePicture(), this.pictureOriginalLocation);
		ImageStorageUtils.deletePicture(user.getProfilePicture(), this.pictureMediumLocation);
		ImageStorageUtils.deletePicture(user.getThumbnailPicture(), this.pictureThumbnailLocation);
		user.setLargePicture(savedPicture);
		user.setProfilePicture(savedPicture);
		user.setThumbnailPicture(savedPicture);
		userRepository.saveAndFlush(user);
	}

	@Override
	@Transactional
	public Set<ActivityInfo> getUserActivities(UserDetailsImpl currentUser, String username)
			throws ResourceNotFoundException, ModelMappingException, BadRequestException {
		final User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		if (!currentUser.getUsername().equals(username)
				&& relationService.isBlocked(currentUser, username).getHaveRelation()) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		final Set<ActivityInfo> activityInfoSet = new HashSet<>();
		final Set<Activity> activities = user.getActivities();
		for (Activity activity : activities) {
			activityInfoSet.add(DtoUtils.entityToDto(activity, ActivityInfo.class));
		}
		return activityInfoSet;
	}

	@Override
	@Transactional
	public ActivityInfo addActivity(UserDetailsImpl currentUser, Long activityId)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException {
		User user = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));
		Activity activity = getActivityById(activityId);

		final Set<Activity> activities = user.getActivities();
		if (activities.contains(activity)) {
			throw new ResourceConflictException("User activity", "name", activity.getName());
		}
		activities.add(activity);
		userRepository.saveAndFlush(user);
		return DtoUtils.entityToDto(activity, ActivityInfo.class);
	}

	@Override
	@Transactional
	public void removeActivity(UserDetailsImpl currentUser, Long activityId) throws ResourceNotFoundException {
		User user = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));
		Activity activity = getActivityById(activityId);

		final Set<Activity> activities = user.getActivities();
		if (!activities.contains(activity)) {
			throw new ResourceNotFoundException("User activity", "name", activity.getName());
		}
		activities.remove(activity);
		userRepository.saveAndFlush(user);
	}

	@Override
	@Transactional
	public Set<ProfileInfo> findByName(UserDetailsImpl currentUser, String name)
			throws ModelMappingException, ResourceNotFoundException, BadRequestException, ResourceBadRequestException {
		final Set<ProfileInfo> profileInfos = new HashSet<>();
		final String trimmedName = name.trim();
		if (trimmedName.isEmpty()) {
			return profileInfos;
		}

		final String currentUserName = currentUser.getUsername();
		final String[] tokens = name.split("\\s+");

		List<User> users;
		ProfileInfo profileInfo;
		for (String token : tokens) {
			users = userRepository.findByFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCase(token, token);
			for (User user : users) {
				if (!user.getUsername().equals(currentUserName)
						&& !relationService.isBlocked(currentUser, user.getUsername()).getHaveRelation()) {
					profileInfo = DtoUtils.entityToDto(user, ProfileInfo.class);
					profileInfo.setRelationInfo(
							relationService.getRelationInfo(currentUser, user.getUsername(), RelationType.values()));
					profileInfos.add(profileInfo);
				}
			}
		}

		if (profileInfos.isEmpty()) {
			throw new ResourceNotFoundException("User", "partial name", name);
		}

		return profileInfos;
	}

	private Activity getActivityById(@NotNull Long activityId) throws ResourceNotFoundException {
		return activityRepository.findById(activityId)
				.orElseThrow(() -> new ResourceNotFoundException("Activity", "id", activityId));
	}

	private ProfileField update(User user, ProfileFieldName fieldName, String rawValue)
			throws ResourceConflictException, ResourceNotFoundException, ResourceBadRequestException {
		String value = rawValue;
		if (value != null) {
			value = value.trim();
			if (value.isEmpty()) {
				value = null;
			}
		}

		switch (fieldName) {
		case EMAIL:
			if (userRepository.existsByEmail(value)) {
				throw new ResourceConflictException("User", "email", rawValue);
			}
			user.setEmail(value);
			break;
		case PASSWORD:
			if (value == null) {
				throw new ResourceBadRequestException("User", "password", rawValue);
			}
			user.setPassword(passwordEncoder.encode(value));
			// For confidentiality
			value = null;
			break;
		case FIRST_NAME:
			user.setFirstName(value);
			break;
		case SURNAME:
			user.setSurname(value);
			break;
		case PHONE_NUMBER:
			if (userRepository.existsByPhoneNumberNotNullAndPhoneNumber(value)) {
				throw new ResourceConflictException("User", "phone number", rawValue);
			}
			user.setPhoneNumber(value);
			break;
		case USERNAME:
			if (userRepository.existsByUsername(value)) {
				throw new ResourceConflictException("User", "username", rawValue);
			}
			user.setUsername(value);
			break;
		case HOME_COUNTRY:
			user.setHomeCountry(value);
			break;
		case COUNTRY:
			user.setCountry(value);
			break;
		case CITY:
			user.setCity(value);
			break;
		default:
			throw new ResourceNotFoundException("Profile field", "name", fieldName);
		}

		userRepository.saveAndFlush(user);

		return new ProfileField(value);
	}

	private ProfileField updateBirthDate(User user, @NotNull @Past Date birthDate) {
		user.setBirthDate(birthDate);
		userRepository.saveAndFlush(user);
		return new ProfileField(birthDate);
	}

	private ProfileField updateGender(User user, @NotNull Boolean female) {
		user.setFemale(female);
		userRepository.saveAndFlush(user);
		return new ProfileField(female);
	}

}
