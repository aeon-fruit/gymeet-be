package com.gymeet.be.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gymeet.be.config.FileStorageProperties;
import com.gymeet.be.exception.FileStorageException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.model.Activity;
import com.gymeet.be.model.Venue;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.payload.VenueInfo;
import com.gymeet.be.payload.VenuePostRequest;
import com.gymeet.be.repository.ActivityRepository;
import com.gymeet.be.repository.VenueRepository;
import com.gymeet.be.service.VenueService;
import com.gymeet.be.util.DtoUtils;
import com.gymeet.be.util.ImageStorageConfig;
import com.gymeet.be.util.ImageStorageUtils;

@Service
public class VenueServiceImpl implements VenueService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(VenueServiceImpl.class);

	private final VenueRepository venueRepository;
	private final ActivityRepository activityRepository;
	private final Path pictureOriginalLocation;
	private final Path pictureMediumLocation;
	private final Path pictureThumbnailLocation;
	private final int mediumWidth;
	private final int mediumHeight;
	private final int thumbnailWidth;
	private final int thumbnailHeight;

	@Autowired
	public VenueServiceImpl(final VenueRepository venueRepository, final ActivityRepository activityRepository,
			final FileStorageProperties fileStorageProperties) {
		this.venueRepository = venueRepository;
		this.activityRepository = activityRepository;

		String filePictureOriginalDir = fileStorageProperties.getVenue().getPicture().getOriginal().getDir();
		String filePictureMediumDir = fileStorageProperties.getVenue().getPicture().getMedium().getDir();
		String filePictureThumbnailDir = fileStorageProperties.getVenue().getPicture().getThumbnail().getDir();

		mediumWidth = fileStorageProperties.getVenue().getPicture().getMedium().getWidth();
		mediumHeight = fileStorageProperties.getVenue().getPicture().getMedium().getHeight();
		thumbnailWidth = fileStorageProperties.getVenue().getPicture().getThumbnail().getWidth();
		thumbnailHeight = fileStorageProperties.getVenue().getPicture().getThumbnail().getHeight();

		this.pictureOriginalLocation = Paths.get(filePictureOriginalDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.pictureOriginalLocation);
		} catch (Exception e) {
			throw new FileStorageException(
					"Could not create the directory where the uploaded original files will be stored.", e);
		}

		this.pictureMediumLocation = Paths.get(filePictureMediumDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.pictureMediumLocation);
		} catch (Exception e) {
			throw new FileStorageException(
					"Could not create the directory where the uploaded medium-size files will be stored.", e);
		}

		this.pictureThumbnailLocation = Paths.get(filePictureThumbnailDir).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.pictureThumbnailLocation);
		} catch (Exception e) {
			throw new FileStorageException(
					"Could not create the directory where the uploaded thumbnail-size files will be stored.", e);
		}
	}

	@Override
	@Transactional
	public Set<VenueInfo> findVenuesByPartialName(String partialName)
			throws ResourceNotFoundException, ModelMappingException {
		final List<Venue> venues = venueRepository.findByNameContainingIgnoreCase(partialName);
		if (venues.isEmpty()) {
			throw new ResourceNotFoundException("Venue", "partial name", partialName);
		}
		final Set<VenueInfo> venueInfoHashSet = new HashSet<>();
		for (Venue venue : venues) {
			venueInfoHashSet.add(DtoUtils.entityToDto(venue, VenueInfo.class));
		}
		return venueInfoHashSet;
	}

	@Override
	@Transactional
	public VenueDetailedInfo findVenueById(Long id) throws ResourceNotFoundException, ModelMappingException {
		final Venue venue = venueRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Venue", "id", id));
		return entityToDto(venue);
	}

	@Override
	@Transactional
	public VenueDetailedInfo addVenue(VenuePostRequest venuePostRequest, MultipartFile picture)
			throws ResourceConflictException, ResourceNotFoundException, ModelMappingException,
			UnknownFileTypeException, IOException {
		if (venueRepository.findByNameIgnoreCase(venuePostRequest.getName()).orElse(null) != null) {
			throw new ResourceConflictException("Venue", "name", venuePostRequest.getName());
		}
		Venue venue = DtoUtils.dtoToEntity(venuePostRequest, Venue.class);

		String savedPicture = null;
		if (picture != null) {
			savedPicture = ImageStorageUtils.storePicture(picture,
					new ImageStorageConfig(this.pictureOriginalLocation, 0, 0),
					new ImageStorageConfig(this.pictureMediumLocation, mediumWidth, mediumHeight),
					new ImageStorageConfig(this.pictureThumbnailLocation, thumbnailWidth, thumbnailHeight));
		}
		ImageStorageUtils.deletePicture(venue.getLargePicture(), this.pictureOriginalLocation);
		ImageStorageUtils.deletePicture(venue.getMediumPicture(), this.pictureMediumLocation);
		ImageStorageUtils.deletePicture(venue.getThumbnailPicture(), this.pictureThumbnailLocation);
		venue.setLargePicture(savedPicture);
		venue.setMediumPicture(savedPicture);
		venue.setThumbnailPicture(savedPicture);
		venue.setActivities(extractActivities(venuePostRequest.getActivities()));
		venue = venueRepository.saveAndFlush(venue);

		return entityToDto(venue);
	}

	@Override
	@Transactional
	public VenueDetailedInfo updateVenueDetails(Long id, VenuePostRequest venuePostRequest)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException {
		Venue venue = venueRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Venue", "id", id));
		final Venue duplicate = venueRepository.findByNameIgnoreCase(venuePostRequest.getName()).orElse(null);
		if ((duplicate != null) && !duplicate.getId().equals(id)) {
			throw new ResourceConflictException("Venue", "name", venuePostRequest.getName());
		}
		final Venue venueUpdater = DtoUtils.dtoToEntity(venuePostRequest, Venue.class);

		venue.setActivities(extractActivities(venuePostRequest.getActivities()));
		venue.setName(venueUpdater.getName());
		venue.setAddress(venueUpdater.getAddress());
		venue.setDescription(venueUpdater.getDescription());
		venue.setLatitude(venueUpdater.getLatitude());
		venue.setLongitude(venueUpdater.getLongitude());
		venue = venueRepository.saveAndFlush(venue);

		return entityToDto(venue);
	}

	@Override
	@Transactional
	public void removeVenueById(Long id) throws ResourceNotFoundException, IOException {
		final Venue venue = venueRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Venue", "id", id));
		String largePicture = venue.getLargePicture();
		String mediumPicture = venue.getMediumPicture();
		String thumbnailPicture = venue.getThumbnailPicture();
		venueRepository.delete(venue);
		ImageStorageUtils.deletePicture(largePicture, this.pictureOriginalLocation);
		ImageStorageUtils.deletePicture(mediumPicture, this.pictureMediumLocation);
		ImageStorageUtils.deletePicture(thumbnailPicture, this.pictureThumbnailLocation);
	}

	@Override
	@Transactional
	public Resource getPicture(Long id, int size) throws ResourceNotFoundException {
		final Venue venue = venueRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Venue", "id", id));

		Path location;
		String filename;
		switch (size) {
		case 0:
			location = pictureOriginalLocation;
			filename = venue.getLargePicture();
			break;
		case 1:
			location = pictureMediumLocation;
			filename = venue.getMediumPicture();
			break;
		case 2:
			location = pictureThumbnailLocation;
			filename = venue.getThumbnailPicture();
			break;
		default:
			throw new ResourceNotFoundException("Venue picture", "size", size);
		}
		try {
			if (filename == null) {
				// TODO - return default picture
				return null;
			}
			Path filePath = location.resolve(filename).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new ResourceNotFoundException("Venue picture", "filename", filename);
			}
		} catch (MalformedURLException e) {
			throw new ResourceNotFoundException("Venue picture", "path", filename);
		}
	}

	@Override
	@Transactional
	public void updateVenuePicture(Long id, MultipartFile picture)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException {
		final Venue venue = venueRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Venue", "id", id));

		String savedPicture = null;
		if (picture != null) {
			savedPicture = ImageStorageUtils.storePicture(picture,
					new ImageStorageConfig(this.pictureOriginalLocation, 0, 0),
					new ImageStorageConfig(this.pictureMediumLocation, mediumWidth, mediumHeight),
					new ImageStorageConfig(this.pictureThumbnailLocation, thumbnailWidth, thumbnailHeight));
		}
		ImageStorageUtils.deletePicture(venue.getLargePicture(), this.pictureOriginalLocation);
		ImageStorageUtils.deletePicture(venue.getMediumPicture(), this.pictureMediumLocation);
		ImageStorageUtils.deletePicture(venue.getThumbnailPicture(), this.pictureThumbnailLocation);
		venue.setLargePicture(savedPicture);
		venue.setMediumPicture(savedPicture);
		venue.setThumbnailPicture(savedPicture);
		venueRepository.saveAndFlush(venue);
	}

	private VenueDetailedInfo entityToDto(Venue venue) throws ModelMappingException {
		VenueDetailedInfo venueDetailedInfo = DtoUtils.entityToDto(venue, VenueDetailedInfo.class);
		Set<ActivityInfo> activityInfoSet = new HashSet<>();
		Set<Activity> activities = venue.getActivities();
		for (Activity activity : activities) {
			activityInfoSet.add(DtoUtils.entityToDto(activity, ActivityInfo.class));
		}
		venueDetailedInfo.setActivityInfoSet(activityInfoSet);
		return venueDetailedInfo;
	}

	private Set<Activity> extractActivities(Set<Long> activityIdSet) throws ResourceNotFoundException {
		Set<Activity> activitySet = new HashSet<>();
		Activity activity;
		if (activityIdSet != null) {
			for (Long activityId : activityIdSet) {
				activity = activityRepository.findById(activityId).orElse(null);
				if (activity == null) {
					throw new ResourceNotFoundException("Activity", "id", activityId);
				}
				activitySet.add(activity);
			}
		}
		return activitySet;
	}

}
