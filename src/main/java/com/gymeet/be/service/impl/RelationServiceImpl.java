package com.gymeet.be.service.impl;

import static com.gymeet.be.util.ApiErrorType.CANNOT_PROBE_SELF;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.exception.BadRequestException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.ResourceUnauthorizedException;
import com.gymeet.be.model.RelationBlocking;
import com.gymeet.be.model.RelationFollower;
import com.gymeet.be.model.RelationMate;
import com.gymeet.be.model.RelationRequester;
import com.gymeet.be.model.User;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.ProfileRelationInfo;
import com.gymeet.be.payload.RelationProperty;
import com.gymeet.be.repository.RelationBlockingRepository;
import com.gymeet.be.repository.RelationFollowerRepository;
import com.gymeet.be.repository.RelationMateRepository;
import com.gymeet.be.repository.RelationRequesterRepository;
import com.gymeet.be.repository.UserRepository;
import com.gymeet.be.security.UserDetailsImpl;
import com.gymeet.be.service.RelationService;
import com.gymeet.be.util.DtoUtils;
import com.gymeet.be.util.RelationType;

@Service
public class RelationServiceImpl implements RelationService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(RelationServiceImpl.class);

	private final UserRepository userRepository;
	private final RelationMateRepository relationMateRepository;
	private final RelationFollowerRepository relationFollowerRepository;
	private final RelationRequesterRepository relationRequesterRepository;
	private final RelationBlockingRepository relationBlockingRepository;

	@Autowired
	public RelationServiceImpl(final UserRepository userRepository, final RelationMateRepository relationMateRepository,
			final RelationFollowerRepository relationFollowerRepository,
			final RelationRequesterRepository relationRequesterRepository,
			final RelationBlockingRepository relationBlockingRepository) {
		this.userRepository = userRepository;
		this.relationMateRepository = relationMateRepository;
		this.relationFollowerRepository = relationFollowerRepository;
		this.relationRequesterRepository = relationRequesterRepository;
		this.relationBlockingRepository = relationBlockingRepository;
	}

	@Override
	@Transactional
	public Set<ProfileInfo> getCurrentUserRelationList(UserDetailsImpl currentUser, String relationType)
			throws ResourceNotFoundException, ModelMappingException, ResourceUnauthorizedException {
		final User user = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		return getRelationList(user, relationType);
	}

	@Override
	@Transactional
	public Set<ProfileInfo> getRelationList(UserDetailsImpl currentUser, String username, String relationType)
			throws BadRequestException, ResourceNotFoundException, ModelMappingException,
			ResourceUnauthorizedException {
		if (currentUser.getUsername().equals(username)) {
			throw new BadRequestException(CANNOT_PROBE_SELF, "relation list");
		}

		final User viewer = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		final User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		// if blocked -> not found
		if (relationBlockingRepository.existsByBlockingAndBlocked(user, viewer)) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		return getRelationList(viewer, user, relationType);
	}

	@Override
	@Transactional
	public ProfileRelationInfo getRelationInfo(UserDetailsImpl currentUser, String username,
			RelationType... relationTypes)
			throws BadRequestException, ResourceNotFoundException, ResourceBadRequestException {
		if (currentUser.getUsername().equals(username)) {
			throw new BadRequestException(CANNOT_PROBE_SELF, "relation info");
		}

		final User viewer = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		final User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		// if blocked -> not found
		if (relationBlockingRepository.existsByBlockingAndBlocked(user, viewer)) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		return getRelationInfo(viewer, user, relationTypes);
	}

	@Override
	@Transactional
	public RelationProperty isBlocked(UserDetailsImpl currentUser, String blockingUsername)
			throws BadRequestException, ResourceNotFoundException {
		if (currentUser.getUsername().equals(blockingUsername)) {
			throw new BadRequestException(CANNOT_PROBE_SELF, "blocked info");
		}

		final User user = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		final User blocking = userRepository.findByUsername(blockingUsername)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", blockingUsername));

		return new RelationProperty(relationBlockingRepository.existsByBlockingAndBlocked(blocking, user));
	}

	@Override
	@Transactional
	public ProfileRelationInfo addRelation(UserDetailsImpl currentUser, String username, String relationType)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceConflictException, ResourceBadRequestException {
		if (currentUser.getUsername().equals(username)) {
			throw new BadRequestException(CANNOT_PROBE_SELF, "add relation");
		}

		final User active = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		final User passive = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		// if blocked -> not found
		if (relationBlockingRepository.existsByBlockingAndBlocked(passive, active)) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		RelationType relation = parseRelationType(relationType);

		switch (relation) {
		case FOLLOWER:
			follow(active, passive);
			break;
		case REQUESTER:
			sendRequest(active, passive);
			break;
		case RESPONDER:
			acceptRequest(active, passive);
			break;
		case BLOCKING:
			block(active, passive);
			break;
		case MATE:
		case FOLLOWED:
			throw new ResourceUnauthorizedException("Relation", "type", relationType);
		default:
			throw new ResourceNotFoundException("Relation", "type", relationType);
		}

		return getRelationInfo(active, passive, RelationType.values());
	}

	@Override
	@Transactional
	public ProfileRelationInfo removeRelation(UserDetailsImpl currentUser, String username, String relationType)
			throws BadRequestException, ResourceNotFoundException, ResourceUnauthorizedException,
			ResourceBadRequestException {
		if (currentUser.getUsername().equals(username)) {
			throw new BadRequestException(CANNOT_PROBE_SELF, "remove relation");
		}

		final User active = userRepository.findByUsername(currentUser.getUsername())
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));

		final User passive = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

		// if blocked -> not found
		if (relationBlockingRepository.existsByBlockingAndBlocked(passive, active)) {
			throw new ResourceNotFoundException("User", "username", username);
		}

		RelationType relation = parseRelationType(relationType);

		switch (relation) {
		case FOLLOWER:
			unFollow(active, passive);
			break;
		case REQUESTER:
			abortRequest(active, passive);
			break;
		case RESPONDER:
			declineRequest(active, passive);
			break;
		case BLOCKING:
			unBlock(active, passive);
			break;
		case MATE:
			deleteMate(active, passive);
			break;
		case FOLLOWED:
			throw new ResourceUnauthorizedException("Relation", "type", relationType);
		default:
			throw new ResourceNotFoundException("Relation", "type", relationType);
		}

		return getRelationInfo(active, passive, RelationType.values());
	}

	private Set<ProfileInfo> getRelationList(User user, String relationType)
			throws ResourceNotFoundException, ModelMappingException, ResourceUnauthorizedException {
		return getRelationList(user, user, relationType);
	}

	private Set<ProfileInfo> getRelationList(User viewer, User user, String relationType)
			throws ResourceNotFoundException, ModelMappingException, ResourceUnauthorizedException {
		RelationType relation = parseRelationType(relationType);

		switch (relation) {
		case MATE:
			return getMateList(user);
		case FOLLOWER:
			return getFollowerList(user);
		case FOLLOWED:
			return getFollowedList(user);
		case REQUESTER:
			if (!user.getUsername().equals(viewer.getUsername())) {
				throw new ResourceUnauthorizedException("Relation list", "user", user.getUsername());
			}
			return getRequestPendingList(viewer);
		case RESPONDER:
			if (!user.getUsername().equals(viewer.getUsername())) {
				throw new ResourceUnauthorizedException("Relation list", "user", user.getUsername());
			}
			return getResponsePendingList(viewer);
		case BLOCKING:
			if (!user.getUsername().equals(viewer.getUsername())) {
				throw new ResourceUnauthorizedException("Relation list", "user", user.getUsername());
			}
			return getBlockedList(viewer);
		}

		throw new ResourceNotFoundException("Relation", "type", relationType);
	}

	private Set<ProfileInfo> getMateList(User user) throws ModelMappingException {
		Set<ProfileInfo> contactList = new HashSet<>();

		List<RelationMate> relationMates = relationMateRepository.findByFirst(user);
		for (RelationMate relationMate : relationMates) {
			contactList.add(DtoUtils.entityToDto(relationMate.getSecond(), ProfileInfo.class));
		}

		relationMates = relationMateRepository.findBySecond(user);
		for (RelationMate relationMate : relationMates) {
			contactList.add(DtoUtils.entityToDto(relationMate.getFirst(), ProfileInfo.class));
		}

		return contactList;
	}

	private Set<ProfileInfo> getFollowerList(User user) throws ModelMappingException {
		Set<ProfileInfo> contactList = new HashSet<>();

		List<RelationFollower> relationFollowers = relationFollowerRepository.findByFollowed(user);
		for (RelationFollower relationFollower : relationFollowers) {
			contactList.add(DtoUtils.entityToDto(relationFollower.getFollower(), ProfileInfo.class));
		}

		return contactList;
	}

	private Set<ProfileInfo> getFollowedList(User user) throws ModelMappingException {
		Set<ProfileInfo> contactList = new HashSet<>();

		List<RelationFollower> relationFollowedList = relationFollowerRepository.findByFollower(user);
		for (RelationFollower relationFollowed : relationFollowedList) {
			contactList.add(DtoUtils.entityToDto(relationFollowed.getFollowed(), ProfileInfo.class));
		}

		return contactList;
	}

	private Set<ProfileInfo> getRequestPendingList(User user) throws ModelMappingException {
		Set<ProfileInfo> contactList = new HashSet<>();

		List<RelationRequester> relationRequesters = relationRequesterRepository.findByRequester(user);
		for (RelationRequester relationRequester : relationRequesters) {
			contactList.add(DtoUtils.entityToDto(relationRequester.getRequested(), ProfileInfo.class));
		}

		return contactList;
	}

	private Set<ProfileInfo> getResponsePendingList(User user) throws ModelMappingException {
		Set<ProfileInfo> contactList = new HashSet<>();

		List<RelationRequester> relationRequestedList = relationRequesterRepository.findByRequested(user);
		for (RelationRequester relationRequested : relationRequestedList) {
			contactList.add(DtoUtils.entityToDto(relationRequested.getRequester(), ProfileInfo.class));
		}

		return contactList;
	}

	private Set<ProfileInfo> getBlockedList(User user) throws ModelMappingException {
		Set<ProfileInfo> contactList = new HashSet<>();

		List<RelationBlocking> relationBlockingList = relationBlockingRepository.findByBlocking(user);
		for (RelationBlocking relationBlocking : relationBlockingList) {
			contactList.add(DtoUtils.entityToDto(relationBlocking.getBlocked(), ProfileInfo.class));
		}

		return contactList;
	}

	private ProfileRelationInfo getRelationInfo(User viewer, User user, RelationType[] relationTypes)
			throws ResourceNotFoundException, ResourceBadRequestException {
		if (relationTypes == null) {
			throw new ResourceBadRequestException("Relation types", "value", null);
		}
		if (relationTypes.length == 0) {
			throw new ResourceBadRequestException("Relation types", "size", 0);
		}
		ProfileRelationInfo relationInfo = new ProfileRelationInfo();
		for (RelationType relation : relationTypes) {
			if (relation == null) {
				throw new ResourceNotFoundException("Relation", "type", null);
			}
			switch (relation) {
			case MATE:
				relationInfo.setMate(
						relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user, viewer, user, viewer));
				break;
			case FOLLOWER:
				relationInfo.setFollower(relationFollowerRepository.existsByFollowerAndFollowed(user, viewer));
				break;
			case FOLLOWED:
				relationInfo.setFollowed(relationFollowerRepository.existsByFollowerAndFollowed(viewer, user));
				break;
			case REQUESTER:
				relationInfo.setRequester(relationRequesterRepository.existsByRequesterAndRequested(user, viewer));
				break;
			case RESPONDER:
				relationInfo.setResponder(relationRequesterRepository.existsByRequesterAndRequested(viewer, user));
				break;
			case BLOCKING:
				relationInfo.setBlocked(relationBlockingRepository.existsByBlockingAndBlocked(viewer, user));
				break;
			default:
				throw new ResourceNotFoundException("Relation", "type", relation);
			}
		}

		return relationInfo;
	}

	private RelationType parseRelationType(String relationType) throws ResourceNotFoundException {
		RelationType relation = null;
		for (RelationType rt : RelationType.values()) {
			if (rt.toString().equalsIgnoreCase(relationType)) {
				relation = rt;
				break;
			}
		}
		if (relation == null) {
			throw new ResourceNotFoundException("Relation", "type", relationType);
		}
		return relation;
	}

	private void follow(User user, User followed) throws ResourceConflictException {
		// not follower
		if (relationFollowerRepository.existsByFollowerAndFollowed(user, followed)) {
			throw new ResourceConflictException(user.getUsername() + " -> " + followed.getUsername(), "relation",
					"follower");
		}

		// no problem -> follow
		relationFollowerRepository.saveAndFlush(new RelationFollower(user, followed));
		// auto - if blocking -> delete blocking entry
		RelationBlocking relationBlocking = relationBlockingRepository.findByBlockingAndBlocked(user, followed);
		if (relationBlocking != null) {
			relationBlockingRepository.delete(relationBlocking);
		}
		// TODO - create notification
	}

	private void sendRequest(User user, User requested) throws ResourceConflictException {
		// not mate
		if (relationMateRepository.existsByFirstAndSecondOrSecondAndFirst(user, requested, user, requested)) {
			throw new ResourceConflictException(user.getUsername() + " -> " + requested.getUsername(), "relation",
					"mate");
		}
		// no request pending
		if (relationRequesterRepository.existsByRequesterAndRequested(user, requested)) {
			throw new ResourceConflictException(user.getUsername() + " -> " + requested.getUsername(), "relation",
					"request pending");
		}
		// no response pending
		if (relationRequesterRepository.existsByRequesterAndRequested(requested, user)) {
			throw new ResourceConflictException(user.getUsername() + " -> " + requested.getUsername(), "relation",
					"response pending");
		}

		// no problem -> add the request
		relationRequesterRepository.saveAndFlush(new RelationRequester(user, requested));
		// auto - if blocking -> delete blocking entry
		RelationBlocking relationBlocking = relationBlockingRepository.findByBlockingAndBlocked(user, requested);
		if (relationBlocking != null) {
			relationBlockingRepository.delete(relationBlocking);
		}
		// auto - if not follower -> follow
		if (!relationFollowerRepository.existsByFollowerAndFollowed(user, requested)) {
			relationFollowerRepository.saveAndFlush(new RelationFollower(user, requested));
		}
		// TODO - create notification
	}

	private void acceptRequest(User user, User requester) throws ResourceNotFoundException {
		RelationRequester request = relationRequesterRepository.findByRequesterAndRequested(requester, user);
		if (request == null) {
			throw new ResourceNotFoundException(user.getUsername() + " -> " + requester.getUsername(), "relation",
					"response pending");
		}
		relationMateRepository.saveAndFlush(new RelationMate(requester, user));
		relationRequesterRepository.delete(request);
		// auto - if not follower -> follow
		if (!relationFollowerRepository.existsByFollowerAndFollowed(user, requester)) {
			relationFollowerRepository.saveAndFlush(new RelationFollower(user, requester));
		}
		// TODO - create notification
	}

	private void block(User user, User blocked) throws ResourceConflictException {
		// not blocking
		if (relationBlockingRepository.existsByBlockingAndBlocked(user, blocked)) {
			throw new ResourceConflictException(user.getUsername() + " -> " + blocked.getUsername(), "relation",
					"blocking");
		}

		// no problem -> block
		relationBlockingRepository.saveAndFlush(new RelationBlocking(user, blocked));
		// auto - if mate -> delete
		relationMateRepository.severeConnection(user.getId(), blocked.getId());
		// auto - if follower or followed -> bilateral un-follow
		relationFollowerRepository.severeConnection(user.getId(), blocked.getId());
		// auto - if request sent/received -> bilateral abort
		relationRequesterRepository.severeConnection(user.getId(), blocked.getId());
	}

	private void unFollow(User user, User followed) throws ResourceNotFoundException {
		RelationFollower relationFollower = relationFollowerRepository.findByFollowerAndFollowed(user, followed);
		if (relationFollower == null) {
			throw new ResourceNotFoundException(user.getUsername() + " -> " + followed.getUsername(), "relation",
					"follower");
		}
		relationFollowerRepository.delete(relationFollower);
	}

	private void abortRequest(User user, User requested) throws ResourceNotFoundException {
		RelationRequester request = relationRequesterRepository.findByRequesterAndRequested(user, requested);
		if (request == null) {
			throw new ResourceNotFoundException(user.getUsername() + " -> " + requested.getUsername(), "relation",
					"request pending");
		}
		relationRequesterRepository.delete(request);
	}

	private void declineRequest(User user, User requester) throws ResourceNotFoundException {
		RelationRequester request = relationRequesterRepository.findByRequesterAndRequested(requester, user);
		if (request == null) {
			throw new ResourceNotFoundException(user.getUsername() + " -> " + requester.getUsername(), "relation",
					"response pending");
		}
		relationRequesterRepository.delete(request);
	}

	private void unBlock(User user, User blocked) throws ResourceNotFoundException {
		RelationBlocking relationBlocking = relationBlockingRepository.findByBlockingAndBlocked(user, blocked);
		if (relationBlocking == null) {
			throw new ResourceNotFoundException(user.getUsername() + " -> " + blocked.getUsername(), "relation",
					"blocking");
		}
		relationBlockingRepository.delete(relationBlocking);
	}

	private void deleteMate(User user, User mate) throws ResourceNotFoundException {
		RelationMate relationMate = relationMateRepository.findByFirstAndSecondOrSecondAndFirst(user, mate, user, mate);
		if (relationMate == null) {
			throw new ResourceNotFoundException(user.getUsername() + " -> " + mate.getUsername(), "relation", "mate");
		}
		relationMateRepository.delete(relationMate);
		// auto - if follower or followed -> bilateral un-follow
		relationFollowerRepository.severeConnection(user.getId(), mate.getId());
	}

}
