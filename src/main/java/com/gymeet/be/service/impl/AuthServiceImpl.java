package com.gymeet.be.service.impl;

import static com.gymeet.be.util.ApiErrorType.USER_ROLE_NOT_SET;

import java.util.Collections;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gymeet.be.config.JwtTokenProvider;
import com.gymeet.be.exception.AppException;
import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceBadRequestException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.model.Role;
import com.gymeet.be.model.User;
import com.gymeet.be.payload.JwtAuthenticationResponse;
import com.gymeet.be.payload.LoginPostRequest;
import com.gymeet.be.payload.ProfileInfo;
import com.gymeet.be.payload.SignUpPostRequest;
import com.gymeet.be.repository.RoleRepository;
import com.gymeet.be.repository.UserRepository;
import com.gymeet.be.service.AuthService;
import com.gymeet.be.util.DtoUtils;
import com.gymeet.be.util.RoleName;

@Service
public class AuthServiceImpl implements AuthService {

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);

	private final UserRepository userRepository;
	private final RoleRepository roleRepository;
	private final AuthenticationManager authenticationManager;
	private final PasswordEncoder passwordEncoder;
	private final JwtTokenProvider tokenProvider;

	@Autowired
	public AuthServiceImpl(final UserRepository userRepository, final RoleRepository roleRepository,
			final AuthenticationManager authenticationManager, final PasswordEncoder passwordEncoder,
			final JwtTokenProvider tokenProvider) {
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
		this.authenticationManager = authenticationManager;
		this.passwordEncoder = passwordEncoder;
		this.tokenProvider = tokenProvider;
	}

	@Override
	@Transactional
	public JwtAuthenticationResponse authenticateUser(@Valid LoginPostRequest loginPostRequest) {
		final Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginPostRequest.getLogin(), loginPostRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		final String jwt = tokenProvider.generateToken(authentication);
		return new JwtAuthenticationResponse(jwt);
	}

	@Override
	@Transactional
	public ProfileInfo registerUser(@Valid SignUpPostRequest signUpPostRequest)
			throws ResourceConflictException, ResourceBadRequestException, ModelMappingException, AppException {
		if ((signUpPostRequest.getPassword() == null) || signUpPostRequest.getPassword().isEmpty()) {
			throw new ResourceBadRequestException("Sign up", "password", signUpPostRequest.getPassword());
		}

		if (userRepository.existsByEmail(signUpPostRequest.getEmail())) {
			throw new ResourceConflictException("User", "email", signUpPostRequest.getEmail());
		}

		if ((signUpPostRequest.getPhoneNumber() != null)
				&& userRepository.existsByPhoneNumberNotNullAndPhoneNumber(signUpPostRequest.getPhoneNumber())) {
			throw new ResourceConflictException("User", "phone number", signUpPostRequest.getPhoneNumber());
		}

		// Creating user's account
		final User user = new User(signUpPostRequest.getEmail(), signUpPostRequest.getPassword(),
				signUpPostRequest.getFirstName(), signUpPostRequest.getSurname(), signUpPostRequest.getPhoneNumber(),
				signUpPostRequest.getBirthDate(), signUpPostRequest.getFemale());

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		final Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new AppException(USER_ROLE_NOT_SET));

		user.setRoles(Collections.singleton(userRole));

		final User createdUser = userRepository.saveAndFlush(user);

		return DtoUtils.entityToDto(createdUser, ProfileInfo.class);
	}

}
