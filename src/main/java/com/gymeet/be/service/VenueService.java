package com.gymeet.be.service;

import java.io.IOException;
import java.util.Set;

import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.VenueDetailedInfo;
import com.gymeet.be.payload.VenueInfo;
import com.gymeet.be.payload.VenuePostRequest;

public interface VenueService {

	@Transactional
	Set<VenueInfo> findVenuesByPartialName(String partialName) throws ResourceNotFoundException, ModelMappingException;

	// TODO - vicinity search

	@Transactional
	VenueDetailedInfo findVenueById(Long id) throws ResourceNotFoundException, ModelMappingException;

	@Transactional
	VenueDetailedInfo addVenue(VenuePostRequest venuePostRequest, MultipartFile picture)
			throws ResourceConflictException, ResourceNotFoundException, ModelMappingException,
			UnknownFileTypeException, IOException;

	@Transactional
	VenueDetailedInfo updateVenueDetails(Long id, VenuePostRequest venuePostRequest)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException;

	@Transactional
	void removeVenueById(Long id) throws ResourceNotFoundException, IOException;

	@Transactional
	Resource getPicture(Long id, int size) throws ResourceNotFoundException;

	@Transactional
	void updateVenuePicture(Long id, MultipartFile picture)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException;

}
