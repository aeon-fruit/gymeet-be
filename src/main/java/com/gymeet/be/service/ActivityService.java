package com.gymeet.be.service;

import java.io.IOException;
import java.util.Set;

import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gymeet.be.exception.ModelMappingException;
import com.gymeet.be.exception.ResourceConflictException;
import com.gymeet.be.exception.ResourceNotFoundException;
import com.gymeet.be.exception.UnknownFileTypeException;
import com.gymeet.be.payload.ActivityDetailedInfo;
import com.gymeet.be.payload.ActivityInfo;
import com.gymeet.be.payload.ActivityPostRequest;
import com.gymeet.be.payload.VenueDetailedInfo;

public interface ActivityService {

	@Transactional
	Set<ActivityInfo> findAllActivities() throws ModelMappingException;

	@Transactional
	Set<ActivityInfo> findActivitiesByPartialName(String partialName)
			throws ResourceNotFoundException, ModelMappingException;

	@Transactional
	ActivityDetailedInfo findActivityById(Long id) throws ResourceNotFoundException, ModelMappingException;

	@Transactional
	ActivityDetailedInfo addActivity(ActivityPostRequest activityPostRequest, MultipartFile icon)
			throws ResourceConflictException, ModelMappingException, UnknownFileTypeException, IOException;

	@Transactional
	ActivityDetailedInfo updateActivityDetails(Long id, ActivityPostRequest activityPostRequest)
			throws ResourceNotFoundException, ResourceConflictException, ModelMappingException;

	@Transactional
	void removeActivityById(Long id) throws ResourceNotFoundException, IOException;

	@Transactional
	Resource getIcon(Long id) throws ResourceNotFoundException;

	@Transactional
	void updateActivityIcon(Long id, MultipartFile icon)
			throws ResourceNotFoundException, UnknownFileTypeException, IOException;

	@Transactional
	Set<VenueDetailedInfo> findVenuesByActivityId(Long id) throws ResourceNotFoundException, ModelMappingException;

}
